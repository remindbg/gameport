<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




/*
 * Front End Routes
 */
Route::get('/search/','SearchController@index')->name('search.index');

Route::get('/contact/','PageController@contact')->name('contact.index');
Route::get('/usloviq/','PageController@usloviq')->name('usloviq.index');
Route::get('/gdpr/','PageController@gdpr')->name('gdpr.index');

Route::get('/ads/gradove/','RegionController@index')->name('region.index');
Route::get('/ads/gradove/{id}/{slug}','RegionController@single')->name('region.single');
Route::get('/','HomeController@index')->name('home');
Route::get('/ads','AdController@index')->name('ad.index');

Route::get('/ads/kategorii','CategoryController@index')->name('category.index');
Route::get('/ads/kategorii/{id}/{slug}', 'CategoryController@single')->name('category.single');
Route::get('/ads/tip/{id}/{slug}', 'AdTypeController@index')->name('adtype.single');

Route::get('users', 'UserController@index')->name('users.index');
Route::get('users/ads/{id}', 'UserController@publishedads')->name('users.publishedads');

Route::get('users/{id}', 'UserController@single')->name('users.single');
Route::get('ratings/user/{id}', 'RatingController@single')->name('ratings.single');

Route::get('/ads/{id}/{catslug}/{uid}','AdController@single')->name('ad.single');




//blog

Route::get('articles', 'BlogArticleController@index')->name('articles.index');
Route::get('articles/read/{article_id}/{category_slug}/{article_slug}/', 'BlogArticleController@single')->name('blogarticles.single');

Route::get('articles/category/{category_id}/{category-slug}', 'BlogCategoryController@single')->name('blogcategories.single');


//end blog


Route::group(['middleware' => 'auth'], function() {
    //Auth routes for the front end
    Route::post('dashboard/edit', 'DashboardController@storeprofile')->name('store.profile');
    Route::get('ads/create', 'AdController@create')->name('ad.create');
    Route::post('ads/store', 'AdController@store')->name('ad.store');
    Route::get('ads/edit/{id}', 'AdController@edit')->name('ad.edit')->middleware('isYourAd');
    Route::get('ads/editimages/{id}', 'AdController@editimages')->name('ad.editimages')->middleware('isYourAd');
    Route::post('ads/update/{id}', 'AdController@update')->name('ad.update')->middleware('isYourAd');
    Route::post('ads/deleteimage/{id}', 'AdController@deleteimage')->name('adimage.delete')->middleware('isYourAd');;
    Route::post('ads/updateimage/{id}', 'AdController@updateimage')->name('adimage.update')->middleware('isYourAd');;


    Route::post('favoriteads/{ad_id}', 'FavoriteAdsController@store')->name('favorite.store');
    Route::post('deletefavoriteads/{id}', 'FavoriteAdsController@destroy')->name('favorite.destroy');

    Route::get('dashboard/favorites', 'FavoriteAdsController@index')->name('dashboard.favorites');
    Route::get('report/{id}', 'ReportController@create')->name('report.create');
    Route::post('report/{id}', 'ReportController@store')->name('report.store');
//    Route::get('uploadimage', 'AdController@mainimage')->name('ad.mainimage');
    Route::get('offer/{ad_id}', 'OfferController@create')->name('offer.create');
    Route::post('offer/{ad_id}', 'OfferController@store')->name('offer.store');
    Route::get('offers/', 'OfferController@index')->name('offer.index');
    Route::get('offer/read/{id}', 'OfferController@single')->name('offer.single');
    Route::get('dashboard/myads', 'DashboardController@myads')->name('dashboard.myads');
    Route::get('messages', 'MessageController@index')->name('messages.index');
    Route::get('messages/read/{id}', 'MessageController@single')->name('messages.single');
    Route::post('messages/insert/{id}', 'MessageController@insert')->name('messages.insert');
    Route::post('messages/compose/{adid}/{sender_id}/{receiver?}', 'MessageController@create')->name('messages.create');
    Route::get('messages/create/{adid}/{sender_id}/{accept?}', 'MessageController@compose')->name('messages.compose');
    Route::post('offerreject/{id}', 'OfferController@reject')->name('offer.reject');
    Route::post('acceptoffer/{id}', 'OfferController@accept')->name('offer.accept');
    Route::get('dashboard/changepassword', 'UserController@changepassword')->name('user.changepassword');
    Route::get('dashboard/editprofile', 'DashboardController@editprofile')->name('dashboard.editprofile');
    Route::post('dashboard/changepassword', 'UserController@updatepassword')->name('dashboard.updatepassword');
    Route::get('offer/activeoffers/{id}', 'OfferController@activesingle')->name('offer.activesingle');
    Route::post('offer/activeoffers/{id}', 'OfferController@finishoffer')->name('offer.finishoffer');

    Route::post('/ratings/{id}/','RatingController@store')->name('rating.store');






});
Auth::routes(['verify' => true]);






Auth::routes();

Route::get('/blog', function () {
    return view('blog.index');
});

Route::get('logout', 'Auth\LoginController@logout');


//Auth::routes(['verify' => true]);

//Route::get('/home', 'HomeController@index')->name('home');

/*
 * admin routes
 */



Route::namespace('Admin')->prefix('panel')->name('admin.')->middleware('admin')->group(function () {

    Route::resource('region','RegionController');
    Route::resource('adtype','AdTypeController');
    Route::resource('ad','AdController');
    Route::resource('adcondition','AdConditionController');
    Route::resource('addelivery','AdDeliveryController');
    Route::resource('ad','AdController');
    Route::resource('category','CategoryController');
    Route::resource('settings','SettingsController');
    Route::resource('messages','MessageController');
    Route::resource('reports','ReportController');
    Route::resource('blogarticles','BlogArticleController');
    Route::resource('blogcategories','BlogCategoryController');
    Route::resource('statictexts','StaticTextController');
    Route::resource('pages','PagesController');
    Route::resource('users','UserController');
    Route::resource('offers','OfferController');
    Route::resource('ratings','RatingController');
    Route::get('/panel', function () {
        return view('admin.layouts.app');
    })->name('panel.welcome');



});

