@extends('layouts.appwithsearch')
@section('title','Обяви и оферти по Области')


@section('content')

    <section class="content">
        <section class="block">
            <div class="container">
                <div class="float-right d-xs-none thumbnail-toggle">
                    <a href="#" class="change-class active" data-change-from-class="list" data-change-to-class="grid" data-parent-class="items">
                        <i class="fa fa-th"></i>
                    </a>
                    <a href="#" class="change-class" data-change-from-class="grid" data-change-to-class="list" data-parent-class="items">
                        <i class="fa fa-th-list"></i>
                    </a>
                </div>
                <!--============ Section Title===================================================================-->
                <div class="section-title clearfix">
                    <div class="float-left float-xs-none">
                      <h2>Разгледайте добавените обяви по области</h2>

                    </div>

                </div>
                <!--============ Items ==========================================================================-->
                <div class="items grid compact grid-xl-4-items grid-lg-4-items grid-md-3-items">

                    @forelse($regions as $region)

                        <a href="gradove/{{$region->id}}/{{$region->slug}}" class="item call-to-action">
                            <div class="wrapper region">
                                <div class="title">
                                    <i class="fa fa-city"></i>
                                    {{$region->name}}
                                    <p class="">
                                        @if($region->ads->count() == 0)
                                            Няма Обяви
                                        @elseif($region->ads->count() == 1)
                                            {{$region->ads->count()}} Обява
                                        @else
                                            {{$region->ads->count()}} Обяви

                                        @endif
                                    </p>
                                </div>

                            </div>
                        </a>

                        @empty
                        <h3>Няма Добавени Области</h3>
                    @endforelse

                </div>
                <!--============ End Items ======================================================================-->
                <div class="page-pagination">
                    <nav aria-label="Pagination">
                        {{$regions->links()}}
                    </nav>
                </div>
                <!--end page-pagination-->
            </div>
            <!--end container-->
        </section>
        <!--end block-->
    </section>
    <!--end content-->



@endsection
