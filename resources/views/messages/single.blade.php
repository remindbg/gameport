@extends('layouts.app')

@section('title','Съобщение')

@section('content')

    <div class="row">
        <div class="col-md-3">
            @include('components.leftsidebar')
        </div>
        <!--end col-md-3-->
        <div class="col-md-9">
            <h3>Преглед на Съобщение</h3>
                @csrf
                {{ csrf_field() }}
                {{--                @method('post')--}}
                <div class="row">

                    <!--end col-md-3-->
                    <div class="col-md-12">
                        <!--============ Section Title===========================================================-->

                        <!--end section-title-->
                        <div id="messaging__chat-window" class="messaging__box">
                            <div class="messaging__header">
                                <div class="float-left flex-row-reverse messaging__person">
                                    <h5 class="font-weight-bold">От : {{$conversation->sender->username}}</h5>
                                    <figure class="mr-4 messaging__image-person" data-background-image=
                                    "{{asset('/uploads/'.$conversation->sender->id . '/'.$conversation->sender->avatar)}}"></figure>
                                </div>
                                <div class="float-right messaging__person">
                                    <h5 class="mr-4">До:  ( {{$conversation->receiver->username}} )</h5>
                                    <figure id="messaging__user" class="messaging__image-person" data-background-image="
{{asset('/uploads/'.$conversation->receiver->id . '/'.$conversation->receiver->avatar)}}"></figure>
                                </div>
                            </div>
                            <div class="messaging__content" data-background-color="rgba(255,255,0,.05)">
                                <div class="messaging__main-chat">

                                    @forelse ($conversation->messages->sortBy('created_at') as $message)


                                        @if($message->user->id ==  Auth::user()->id)
                                            <div class="messaging__main-chat__bubble user">
                                                <p>
                                                   {{$message->message}}
                                                    <small>{{$message->created_at->diffForHumans()}}</small>
                                                    @if ($message->is_seen)

                                                        <span class="badge badge-success">Прочетено</span>
                                                        @else
                                                        <span class="badge badge-warning">Не е видяно</span>
                                                    @endif

                                                </p>
                                            </div>

                                            @else
                                            <div class="messaging__main-chat__bubble">
                                                <p>
                                                   {{$message->message}}


                                                    <small>{{$message->created_at->diffForHumans()}}</small>
                                                    @if ($message->is_seen)

                                                        <span class="badge badge-success">Прочетено</span>
                                                    @else
                                                        <span class="badge badge-warning">Не е видяно</span>
                                                    @endif
                                                </p>
                                            </div>
                                        @endif





                                    @empty
                                        Няма Съобщения все още!

                                    @endforelse

                                </div>
                            </div>
                            <div class="messaging__footer">
                                <form class="form" method="POST" action="{{route('messages.insert',$conversation->id)}}">
                                    @csrf
                                    {{ csrf_field() }}
                                    <div class="input-group">
                                        <input type="text" name="message" class="form-control mr-4" placeholder="Вашето Съобщение">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="submit">Изпрати <i class="fa fa-send ml-3"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--end col-md-9-->
                </div>

                <!--end col-md-3-->


        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('assets/plugins/typeahead.js')}}"></script>
    <script>

    </script>


@endsection










