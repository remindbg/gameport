@extends('layouts.app')

@section('title','Редакция на Профил')

@section('content')

    <div class="row">
        <div class="col-md-3">
            @include('components.leftsidebar')
        </div>
        <!--end col-md-3-->
        <div class="col-md-9">
            <h3>Съобщения от други потребители</h3>
            <form class="form" method="POST" action="{{route('store.profile')}}" enctype="multipart/form-data">
                @csrf
                {{ csrf_field() }}
                {{--                @method('post')--}}
                <div class="row">
                    <div class="col-md-12">
                        <section class="content">
                            <section class="block">
                                <div class="col-md-12">
                                    <h2>Съобщения</h2>
                                    <ul class="nav nav-pills" id="myTab-pills" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="one-tab-pills" data-toggle="tab"
                                               href="#received" role="tab" aria-controls="one-pills"
                                               aria-expanded="true">Стартирани от Други</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="two-tab-pills" data-toggle="tab" href="#sent"
                                               role="tab" aria-controls="two-pills">Стартирани от Теб</a>
                                        </li>

                                    </ul>
                                    <div class="tab-content" id="myTabContent-pills">
                                        <div class="tab-pane fade show active" id="received" role="tabpanel" aria-labelledby="one-tab-pills">
                                            <div id="messaging__chat-list" class="messaging__box">
                                                <div class="messaging__content">
                                                    <ul class="messaging__persons-list">
                                                        @forelse($receivedconvos as $message)
                                                            {{--                                                {{dd($message->conversation->sender)}}--}}
                                                            <a href="{{route('messages.single',$message->id)}}">

                                                            <li>
                                                                    <div class="messaging__person">
                                                                        <figure class="messaging__image-item"
                                                                                @if ($message->sender->avatar)
                                                                                data-background-image="{{asset('/uploads/'.$message->sender->id . '/'.$message->sender->avatar)}}"></figure>

                                                                    @else
                                                                            data-background-image="{{asset('avatar.jpg')
                                                                            }}"></figure>


                                                                        @endif
                                                                        <figure class="content">
                                                                            <h5>{{$message->sender->username}}</h5>
                                                                            @if (!$message->is_seen)
                                                                                <span class="badge
                                                                                badge-success">Ново 4</span>
                                                                            @endif
{{--                                                                            Относно Обява: {{$message->ad->title}}--}}

                                                                            <small>{{$message->created_at->diffForHumans()}}</small>
                                                                        </figure>

                                                                    </div>
                                                                </li>
                                                            </a>

                                                        @empty
                                                           Нямате стартирали дискусии от други хора

                                                        @endforelse



                                                    </ul>
                                                    <!--end messaging__persons-list-->
                                                </div>
                                                <!--messaging__content-->
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="sent" role="tabpanel" aria-labelledby="two-tab-pills">
                                            <div id="messaging__chat-list" class="messaging__box">

                                                <div class="messaging__content">
                                                    <ul class="messaging__persons-list">
                                                        @forelse($sentconvos as $message)
                                                            {{--                                                {{dd($message->conversation->sender)}}--}}
                                                            <a href="{{route('messages.single',$message->id)}}">
                                                                <li>
                                                                    <div class="messaging__person">
                                                                        <figure class="messaging__image-item"
                                                                                data-background-image="{{asset('/uploads/'.$message->receiver->id . '/'.$message->receiver->avatar)}}"></figure>
                                                                        <figure class="content">
                                                                            <h5>До: {{$message->receiver->username}}</h5>
                                                                            @if (!$message->is_seen)
                                                                                <span class="badge
                                                                                badge-success">Ново</span>
                                                                            @endif
{{--                                                                            Относно Обява: {{$message->ad->title}}--}}

                                                                            <p>{{$message->messages->last()->message}}</p>
                                                                            <small>{{$message->created_at->diffForHumans()}}</small>
                                                                        </figure>

                                                                    </div>
                                                                    <!--messaging__person-->
                                                                </li>
                                                            </a>

                                                        @empty
                                                            Нямате Изпратени Съобщения Все Още

                                                        @endforelse



                                                    </ul>
                                                    <!--end messaging__persons-list-->
                                                </div>
                                                <!--messaging__content-->
                                            </div>

                                        </div>

                                    </div>
                                </div>


                                <!--end section-title-->

                            </section>
                            <!--end block-->
                        </section>
                    </div>

                    <!--end col-md-8-->

                </div>
                <!--end col-md-3-->

            </form>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('assets/plugins/typeahead.js')}}"></script>
    <script>

    </script>


@endsection










