@extends('layouts.appwithsearch')
@section('title','Докладване на обява')


@section('content')


    <!--*********************************************************************************************************-->
    <section class="content">
        <section class="block">
            <div class="container">
                <form class="form form-submit" method="POST" enctype="multipart/form-data" action="{{route('report.store',$ad->id)}}">
                    @csrf

                    <section>
                        <h3>Докладване на Обява:</h3>
                        <div class="box">
                            <p>{{$ad->title}}</p>
                            <p class="muted">От потребител: {{$ad->user->username}}</p>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <section>

                                    <div class="form-group">
                                        <label for="details" class="col-form-label">Информация относно Доклада:</label>
                                        <textarea name="body" id="details" class="form-control" rows="4"></textarea>
                                    </div>
                                    <!--end form-group-->
                                </section>
                                <!--end form-group-->
                            </div>
                            <!--end col-md-8-->

                        </div>
                        <div class="row">


                        </div>

                    <section class="clearfix">
                        <div class="form-group">
                            <button type="submit" onclick="return confirm('Сигурни ли сте?')"
                                    class="btn btn-primary large icon btn-block">Изпращане на доклада
                                <i class="fa fa-chevron-right"></i>
                            </button>
                        </div>
                    </section>
                </form>
                <!--end form-submit-->
            </div>
            <!--end container-->
        </section>
        <!--end block-->
    </section>
    <!--end content-->



@endsection
