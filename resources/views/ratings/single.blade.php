@extends('layouts.app')
@section('title','Рейтинг на Потребител : ' . $user->username)

@section('content')
    <section class="content">
        <section class="block">
            <div class="container">
                <div class="row flex-column-reverse flex-md-row">
                    <div class="col-md-12">

                        <hr>
                        <a href="{{route('users.single',$user->id)}}"class="btn btn-info btn-block"><i class="fa
                            fa-arrow-circle-left fa-fw
"></i>Назад Към
                            Профила</a>
                        <section>

                            <h2>Всички Отзиви за потребител {{$user->username}}</h2>
                            <div class="comments">
                                @forelse($userrating->sortByDesc('created_at') as $rating)

                                    <div class="comment">
                                        <div class="author">
                                            <a href="{{route('users.single',$rating->ratingauthor->id)}})}}"
                                               class="author-image">
                                                <div class="background-image">
                                                    <img src="{{asset('/uploads/'.$rating->ratingauthor->id . '/'
                                                    .$rating->ratingauthor->avatar)}}" alt="">
                                                </div>
                                            </a>
                                            <div class="author-description {{$rating->is_negative ? 'text-danger' :
                                            ''}}">
                                                <h3>{{$rating->is_negative ? 'Негативен' : 'Положителен'}}</h3>
                                                <div class="meta">
                                                    <div class="rating" data-rating="{{$rating->rating_number}}"></div>


                                                    <span>{{$rating->updated_at->diffForHumans()}}</span>
                                                    <h5><a href="#">{{$rating->ratingauthor->username}}</a></h5>
                                                </div>
                                                <!--end meta-->
                                                <p>
                                                    {{$rating->body}}
                                                </p>
                                            </div>
                                            <!--end author-description-->
                                        </div>
                                        <!--end author-->
                                    </div>

                                @empty
                                    <h3>Няма добавени отзиви</h3>
                            @endforelse

                            <!--end comment-->

                            </div>
                            <!--end comment-->

                        </section>
                    </div>
                    <!--end col-md-9-->

                    <!--end col-md-3-->
                </div>
            </div>
            <!--end container-->
        </section>
        <!--end block-->
    </section>
    <!--end content-->

@stop
