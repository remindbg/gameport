@extends('layouts.homelayout')
@section('title'){{$settings->title ?? 'Няма Заглавие'}}
@endsection

@section('meta')
    <meta name="description" content="{{$settings->description ?? 'Няма Описание'}}">

@endsection

@section('content')
    <section class="block">
        <div class="container">
          @include('components.search')
            <hr>
            <h2>Категории</h2>
            <ul class="categories-list clearfix">

                @forelse($categories as $category)
                    @if($category->parent_id == 0)
                        <li>
                            <i class="category-icon">
                                <i class="{{$category->icon}}"></i>
                            </i>
                            <h3><a href="{{route('category.single',['id' => $category->id, 'slug' => $category->slug])}}">{{$category->name}}</a></h3>

                            <div class="sub-categories">
                                @foreach($category->children as $parent)
                                    <a href="{{route('category.single',['id' => $parent->id, 'slug' => $parent->slug])}}">{{$parent->name}}</a>
                                @endforeach
                            </div>
                        </li>
                    @endif
                @empty
                    <h2>Няма Категории</h2>
                @endforelse
            </ul>
            <a href="ads/kategorii"><button type="button" class="btn btn-info icon float-right width-100">Виж Всички
                    Категории<i class="fa fa-chevron-right"></i></button></a>
            <!--end categories-list-->
        </div>
        <!--end container-->
    </section>
    <!--============ Section Title===================================================================-->

    <div class="float-right d-xs-none thumbnail-toggle">
        <a href="#" class="change-class active" data-change-from-class="list" data-change-to-class="grid" data-parent-class="items">
            <i class="fa fa-th"></i>
        </a>
        <a href="#" class="change-class" data-change-from-class="grid" data-change-to-class="list" data-parent-class="items">
            <i class="fa fa-th-list"></i>
        </a>
    </div>

    <h2>Последни Обяви</h2>

    <!--============ Items ==========================================================================-->
    <div class="items grid compact grid-xl-4-items grid-lg-3-items grid-md-2-items" id="items">
        @php
            $randomad = rand(1,$ads->count())
        @endphp
        @forelse ($ads as $ad)
            @if ($loop->iteration == $randomad)

                <a href="/ads/create" class="item call-to-action">
                    <div class="wrapper">
                        <div class="title">
                            <i class="fa fa-plus-square-o"></i>
                            Добави Обява
                        </div>
                    </div>
                </a>
                @include('components.adsingle')
            @else
                @include('components.adsingle')
            @endif
        @empty
            Няма намерени Обяви
        @endforelse

            <a href="/ads"><button type="button" class="btn btn-info icon float-right width-100">Виж Всички Обяви<i
                            class="fa fa-chevron-right"></i></button></a>

    </div>


    <div class="page-pagination">
        <nav aria-label="Pagination">

              {{$ads->links()}}

        </nav>
    </div>
    <!--end page-pagination-->

    <!--============ End Items ======================================================================-->

    <!--end page-pagination-->
@stop('content')
