<div class="col-md-4">
    <!--============ Side Bar ===============================================================-->
    <aside class="sidebar">
        <section>
            <h2>Категории</h2>
            <ul class="sidebar-list list-unstyled">
                @forelse($bcategories as $category)
                    <li>
                        <a href="{{route('blogcategories.single',['category_id',$category->id,'category_slug' =>
                        $category->slug])
                        }}">{{$category->name}}<span>
                                        @if ($category->articles)
                                    {{$category->articles->count()}}
                                @endif
                                    </span></a>
                    </li>
                @empty
                    <strong>Няма Добавени Категории</strong>

                @endforelse

            </ul>
        </section>

        <section>
            <h2>Последни Публикации</h2>
            <div class="sidebar-post">
                @forelse($latestarticles as $article)
                    <a href="{{route('blogarticles.single',
                            ['article_id' => $article->id,
                            'category_slug' => $article->category->slug,
                            'article_slug' => $article->slug
                            ])
                            }}" class="background-image">
                        <img src="{{$article->image}}">
                    </a>
                    <!--end background-image-->
                    <div class="description">
                        <h4>
                            <a href="blog-post.html">{{$article->title}}</a>
                        </h4>
                        <div class="meta">
                            <a href="{{route('blogarticles.single',
                            ['article_id' => $article->id,
                            'category_slug' => $article->category->slug,
                            'article_slug' => $article->slug
                            ])
                            }}">{{$article->user->username}}</a>
                            <figure>{{$article->created_at->diffForHumans()}}</figure>
                        </div>
                        <!--end meta-->
                    </div>
                    @empty

                    Няма

                    @endforelse


                <!--end description-->
            </div>
            <!--end sidebar-post-->


            <!--end sidebar-post-->

        </section>


    </aside>
    <!--============ End Side Bar ===========================================================-->
</div>