<form class="hero-form form" action="{{route('search.index')}}">
    <div class="container">
        <!--Main Form-->
        <div class="main-search-form">
            <div class="form-row">
                <div class="col-md-3 col-sm-3">
                    <div class="form-group">
                        <label for="what" class="col-form-label">Какво?</label>
                        <input name="keyword" value="{{ old('keyword') }}" type="text" class="form-control"
                               id="what"
                        placeholder="Какво?">
                    </div>
                    <!--end form-group-->
                </div>
                <!--end col-md-3-->
                <div class="col-md-3 col-sm-3">
                    <div class="form-group">
                        <label for="category" class="col-form-label">Категория?</label>
                        <select name="category_id" id="category" data-placeholder="Select Category">
                            <option value="0">Без Значение</option>
                           @forelse($categoriessearch as $category)

                                    <option value="{{$category->id}}"
                                        @if (old('$category_id'))
                                            {{old('category_id') == $category_id ?
                                    'selected' : ''}}
                                            @endif

                                        >{{$category->name}}</option>

                            @empty
                            @endforelse
                        </select>
                    </div>
                    <!--end form-group-->
                </div>
                <!--end col-md-3-->
                <div class="col-md-3 col-sm-3">
                    <div class="form-group">
                        <label for="region" class="col-form-label">Област?</label>
                        <select name="region_id" id="region" data-placeholder="Регион">
                            <option value="0">Без Значение</option>
                            @forelse($regionssearch as $region)


                                <option value="{{$region->id}}"
                              >{{$region->name}}</option>

                            @empty
                            @endforelse
                        </select>
                    </div>
                    <!--end form-group-->
                </div>
                <!--end col-md-3-->
                <div class="col-md-3 col-sm-3">
                    <button type="submit" class="btn btn-info width-100">Търсене</button>
                </div>
                <!--end col-md-3-->
            </div>
            <!--end form-row-->
        </div>
        <!--end main-search-form-->
        <!--Alternative Form-->
        <div class="alternative-search-form">
            <a href="#collapseAlternativeSearchForm"
               class="icon"
               data-toggle="collapse"
               aria-expanded="false"
               aria-controls="collapseAlternativeSearchForm">
                <i class="fa fa-plus"></i>Още</a>
            <div class="collapse" id="collapseAlternativeSearchForm">
                <div class="wrapper">
                    <div class="form-row">

                        <!--end col-xl-6-->
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div class="form-row">
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="type" class="col-form-label">Тип?</label>
                                        <select name="type_id" id="type" data-placeholder="Тип" class="small">
                                            <option value="0" class="small">Без Значение</option>
                                            @forelse($adtypes as $type)

                                                <option value="{{$type->id}}" >{{$type->name}}</option>

                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                    <!--end form-group-->
                                </div>
                                <div class="col-md-4 col-sm-6">

                                    <div class="form-group">
                                        <label for="min-price" class="col-form-label">Минимална Цена?</label>

                                        <input name="min_price" type="text" class="form-control small"
                                               id="min-price" placeholder="От" value="{{old('min_price')}}">
                                        <span class="input-group-addon small">.лв</span>
                                    </div>
                                    <!--end form-group-->
                                </div>
                                <!--end col-md-4-->
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="max-price" class="col-form-label">Максимална Цена?</label>

                                        <input name="max_price" type="text" class="form-control small"
                                               id="max-price" placeholder="До" value="{{old('max_price')}}">
                                        <span class="input-group-addon small">.лв</span>
                                    </div>
                                    <!--end form-group-->
                                </div>
                                <button type="submit" class="btn btn-info width-100">Търсене</button>

                                <!--end col-md-4-->

                                <!--end col-md-3-->
                            </div>
                            <!--end form-row-->
                        </div>
                        <!--end col-xl-6-->
                    </div>
                    <!--end row-->
                </div>
                <!--end wrapper-->
            </div>
            <!--end collapse-->
        </div>
        <!--end alternative-search-form-->
    </div>
    <!--end container-->
</form>
