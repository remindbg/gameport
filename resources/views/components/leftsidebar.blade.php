<nav class="nav flex-column side-nav">

    <a class="nav-link icon {{request()->routeIs('offer.index*') ? 'active' : '' }}" href="/offers/">
        <i class="fa fa-gift"></i>Оферти
    </a>
    <a class="nav-link icon {{request()->routeIs('messages.index*') ? 'active' : '' }}" href="/messages/">
        <i class="fa fa-comment fa-fw"></i>Съобщения
    </a>
    <a class="nav-link icon {{request()->routeIs('dashboard.editprofile*') ? 'active' : '' }}"
       href="/dashboard/editprofile">
        <i class="fa fa-user fa-fw"></i>Редакция на Профила
    </a>
    <a class="nav-link icon {{request()->routeIs('dashboard.myads*') ? 'active' : '' }}" href="/dashboard/myads">
        <i class="fa fa-heart fa-fw"></i>Моите Обяви
    </a>
    <a class="nav-link icon {{request()->routeIs('dashboard.favorites*') ? 'active' : '' }} " href="/dashboard/favorites">
        <i class="fa fa-star fa-fw"></i>Любими
    </a>
    <a class="nav-link icon {{request()->routeIs('user.changepassword*') ? 'active' : '' }}"
       href="{{route('user.changepassword')}}">
        <i class="fa fa-recycle fa-fw"></i>Смяна на Парола
    </a>

</nav>
