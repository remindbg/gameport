<div class="item">
{{--                <div class="ribbon-featured">{{$ad->created_at->diffforhumans()}}</div>--}}
<!--end ribbon-->
    <div class="wrapper">
        <div class="image">
            <h3>
                <a href="#" class="tag category">{{$ad->category->name}}</a>
                <a href="{{route('ad.single',[
                            'id' => $ad->id,
                            'catslug' => $ad->category->slug,
                            'uid' => $ad->uid
                            ])}}" class="title">{{$ad->title}}</a>
                <span class="tag">{{$ad->type->name}}</span>
            </h3>
            <a href="{{route('ad.single',[
                            'id' => $ad->id,
                            'catslug' => $ad->category->slug,
                            'uid' => $ad->uid
                            ])}}" class="image-wrapper background-image">
                @if(!$ad->images->isEmpty())
                    @foreach($ad->images as $image)
                        @if($image->is_first)
                            <img src="{!! asset($image->filepath) !!}" alt="">

                        @else
                            <img src="{!! asset($image->filepath) !!}" alt="">
                        @endif
                    @endforeach
                @else
                    <img src="{{asset('noimage-small.png')}}" alt="">
                @endif
            </a>
        </div>
        <!--end image-->
        <h4 class="location">
            <a href="#">{{$ad->region->name}}</a>
        </h4>
        <div class="price">{{$ad->price}}лв</div>
        <div class="meta">
            <figure>
                <i class="fa fa-calendar-o"></i>{{$ad->created_at->diffforhumans()}}
            </figure>
            <figure>

                <a href="#">
                    <i class="fa fa-user"></i>{{$ad->user->username}}
                </a>
            </figure>
        </div>
        <!--end meta-->
        <div class="description">
            <p>{{$ad->description}}</p>
        </div>
        <!--end description-->
        <a href="{{route('ad.single',[
                            'id' => $ad->id,
                            'catslug' => $ad->category->slug,
                            'uid' => $ad->uid
                            ])}}" class="detail text-caps underline">Детайли</a>
    </div>
</div>
