@extends('layouts.appwithsearch')
@section('title','Потребители')


@section('content')

    <section class="content">
        <section class="block">
            <div class="container">
                <!--============ Section Title===================================================================-->
                <div class="section-title clearfix">
                    <div class="float-left float-xs-none">
                      <h3>Потребители</h3>
                    </div>
                    <div class="float-right d-xs-none thumbnail-toggle">
                        <a href="#" class="change-class active" data-change-from-class="list" data-change-to-class="masonry" data-parent-class="authors">
                            <i class="fa fa-th"></i>
                        </a>
                        <a href="#" class="change-class" data-change-from-class="masonry" data-change-to-class="list" data-parent-class="authors">
                            <i class="fa fa-th-list"></i>
                        </a>
                    </div>
                </div>
                <!--============ Items ==========================================================================-->
                <div class="authors masonry items grid-xl-4-items grid-lg-4-items grid-md-4-items">
                    @forelse ($users as $user)
                        <div class="item author">
                            <div class="wrapper">
                                <div class="image">
                                    <h3>
                                        <a href="/users/{{$user->id}}" class="title">{{$user->username}}</a>
                                    </h3>
                                    <a href="/users/{{$user->id}}" class="image-wrapper background-image">
                                        @if ($user->avatar)
                                            <img src="{{asset('/uploads/'.$user->id . '/'.$user->avatar)}}" alt="">
                                            @else
                                            <img src="{{asset('avatar.jpg')}}" alt="">


                                        @endif
                                    </a>
                                </div>
                                <!--end image-->
                                <h4 class="location">
                                    <a href="#"> @if($user->region)
                                            {{$user->region->name}}
                                        @else
                                            Няма
                                        @endif</a>
                                </h4>
                                <div class="meta">

                                    <figure>
                                        <a href="{{route('users.publishedads',$user->id)}}">
                                        <i class="fa fa-user"></i><strong>Обяви : {{$user->ads->count()}}</strong>
                                        </a>
                                    </figure>
                                </div>
                                <!--end meta-->
                                <div class="description">
                                    <p>{{$user->about}}</p>
                                </div>
                                <!--end description-->
                                <div class="additional-info">
                                    <ul>
                                        <li>
                                            <figure>Имейл</figure>
                                            <aside>{{$user->hideemail()}}</aside>
                                        </li>
                                        <li>
                                            <figure>Телефон</figure>
                                            <aside>
                                                @if($user->phone)
                                                    {{$user->phone}}
                                                @else
                                                    Няма
                                                @endif

                                            </aside>
                                        </li>
                                    </ul>
                                </div>
                                <!--end addition-info-->
                                    <a href="/users/{{$user->id}}" class="detail text-caps underline text-center">Виж Целия Профил</a>

                            </div>
                        </div>
                    @empty
                    Няма Потребители
                    @endforelse

                    <!--end item-->





                </div>
                <!--============ End Items ======================================================================-->

                <!--end page-pagination-->
            </div>
            <!--end container-->
        </section>
        <!--end block-->
    </section>




@endsection









<!--end content-->
