@extends('layouts.app')
@section('title','Потребител с username: ' . $user->username)

@section('content')
    <section class="content">
        <section class="block">
            <div class="container">
                <div class="row flex-column-reverse flex-md-row">
                    <div class="col-md-12">
                        <section class="my-0">
                            <div class="author big">
                                <div class="author-image">
                                    <div class="background-image">
                                        <img src="{{asset('/uploads/'.$user->id . '/'.$user->avatar)}}" alt="">

                                    </div>

                                </div>
                                <!--end author-image-->
                                <div class="author-description">

                                    <div class="section-title">
                                        <h2>{{$user->username}}</h2>



                                        <h4 class="location">
                                            @if ($user->region)

                                                <a href="{{route('region.single',[
                                            'id' => $user->region->id,
                                            'slug' => $user->region->slug
                                            ])}}"></a>
                                            @else
                                                Няма Информация
                                            @endif

                                        </h4>
                                        <h4 class="">
                                            <span> <i class="fa fa-city"></i> Местоживеене:</span>
                                            @if ($user->city)

                                                <a href=#">
                                                    {{$user->city}}
                                                </a>
                                            @else
                                                Няма Информация
                                            @endif

                                        </h4>


                                    </div>
                                    <div class="additional-info">
                                        <ul>
                                            <li>
                                                <figure>Телефон</figure>
                                                @if ($user->phone)

                                                    <a href=#">
                                                        {{$user->phone}}
                                                    </a>
                                                @else
                                                    Няма Информация
                                                @endif
                                            </li>
                                            <li>
                                                <figure>Email</figure>
                                                <aside><a href="#">{{$user->hideemail()}}</a></aside>
                                            </li>

                                        </ul>
                                    </div>
                                    <hr><span class="text-muted">Потребителят има Оставени общо
                                        {{$user->receivedrating->where('is_approved',true)->count()}} отзива.

                                 <hr>


                                    <a href="{{route('ratings.single',$user->id)}}"><h4> Вижте Всички Отзиви за
                                            потребителя:</h4></a>


                                </div>
                                <div>
                                    <h3>Описание:</h3>
                                    <h4> adfadfasfasfa{{$user->about}}</h4>


                                </div>

                                <!--end author-description-->
                            </div>
                            <!--end author-->
                        </section>

                        <hr>

                        <section>
                            <a href="{{route('users.publishedads',$user->id)}}">    <h3>Вижте Всички
                                   </h3></a>
                            <h4> Последните  8 Публикувани Обяви</h4>

                            <!--============ Section Title===================================================================-->
                            <div class="section-title clearfix">

                                <div class="float-right float-xs-none d-xs-none thumbnail-toggle">
                                    <a href="#" class="change-class active" data-change-from-class="list compact" data-change-to-class="grid" data-parent-class="items">
                                        <i class="fa fa-th"></i>
                                    </a>
                                    <a href="#" class="change-class" data-change-from-class="grid" data-change-to-class="list compact" data-parent-class="items">
                                        <i class="fa fa-th-list"></i>
                                    </a>
                                </div>
                            </div>

                            <!--============ Items ==========================================================================-->
                            <div class="read-more" data-read-more-link-more="Show More" data-read-more-link-less="Show Less">
                                <div class="items grid compact grid-xl-4-items grid-lg-3-items grid-md-2-items">
                                    @php
                                        $randomad = rand(1,  $user->ads->count())
                                    @endphp
                                    @forelse ( $user->ads->sortByDesc('created_at')->slice(0,8) as $ad)
                                        @if ($loop->iteration == $randomad)

                                            <a href="/ads/create" class="item call-to-action">
                                                <div class="wrapper">
                                                    <div class="title">
                                                        <i class="fa fa-plus-square-o"></i>
                                                        Добави Обява
                                                    </div>
                                                </div>
                                            </a>
                                        @else
                                            @include('components.adsingle')
                                        @endif
                                    @empty
                                        <a href="#" class="item call-to-action">
                                            <div class="wrapper">
                                                <div class="title">
                                                    <i class="fa fa-plus-square-o"></i>
                                                  Няма Добавени Обяви
                                                </div>
                                            </div>
                                        </a>
                                    @endforelse


                                </div>

                            </div>
                            <!--end read-more-->
                        </section>

                        <section>
                            <section>
                                <h2>Оставете Рейтинг</h2>
                                <form class="form" action="{{route('rating.store',$user->id)}}" method="POST">
                                    @csrf
                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="rating" class="col-form-label">Рейтинг</label>
                                                <select name="is_negative" id="rating" data-placeholder="Изберете
                                                Рейтинг">
                                                    <option value="0" data-option-stars="1">Положителен</option>
                                                    <option value="1" data-option-stars="2">Негативен</option>

                                                </select>
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="rating" class="col-form-label">Рейтинг</label>
                                                <select name="rating_number" id="rating" data-placeholder="Рейтинг">
                                                    <option value="">Изберете Рейтинг от 1 до 5</option>
                                                    <option value="1" data-option-stars="1">1</option>
                                                    <option value="2" data-option-stars="2">2</option>
                                                    <option value="3" data-option-stars="3">3</option>
                                                    <option value="4" data-option-stars="4">4</option>
                                                    <option value="5" data-option-stars="5">5</option>
                                                </select>
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-4-->
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="review" class="col-form-label">Текст към рейтинга</label>
                                                <textarea name="body" id="review" class="form-control" rows="4"
                                                          placeholder="Good seller, I am satisfied."></textarea>
                                            </div>
                                            <!--end form-group-->
                                            <button class="btn btn-info">Изпрати Рейтинг</button>
                                        </div>

                                        <!--end col-md-12-->
                                    </div>
                                    <!--end row-->
                                </form>
                                <!--end form-->
                            </section>
                            <h2>Отзиви за потребителя</h2>
                            <div class="comments">
                                @forelse($userrating->sortByDesc('created_at') as $rating)

                                    <div class="comment">
                                        <div class="author">
                                            <a href="{{route('users.single',$rating->ratingauthor->id)}})}}"
                                               class="author-image">
                                                <div class="background-image">
                                                    <img src="{{asset('/uploads/'.$rating->ratingauthor->id . '/'
                                                    .$rating->ratingauthor->avatar)}}" alt="">
                                                </div>
                                            </a>
                                            <div class="author-description {{$rating->is_negative ? 'text-danger' :
                                            ''}}">
                                                <h3>{{$rating->is_negative ? 'Негативен' : 'Положителен'}}</h3>
                                                <div class="meta">
                                                    <div class="rating" data-rating="{{$rating->rating_number}}"></div>


                                                    <span>{{$rating->updated_at->diffForHumans()}}</span>
                                                    <h5><a href="#">{{$rating->ratingauthor->username}}</a></h5>
                                                </div>
                                                <!--end meta-->
                                                <p>
                                                   {{$rating->body}}
                                                </p>
                                            </div>
                                            <!--end author-description-->
                                        </div>
                                        <!--end author-->
                                    </div>
                                    @if ($loop->last)
                                        <div class="center">
                                            <a href="{{route('ratings.single',$user->id)}}" class="btn btn-primary
                                        btn-rounded
                                        btn-framed">Вижте Всички</a>
                                        </div>
                                    @endif
                                @empty
                                    <h3>Няма добавени отзиви</h3>
                                    @endforelse

                                    <!--end comment-->

                            </div>
                            <!--end comment-->

                        </section>
                    </div>
                    <!--end col-md-9-->

                    <!--end col-md-3-->
                </div>
            </div>
            <!--end container-->
        </section>
        <!--end block-->
    </section>
    <!--end content-->

@stop
