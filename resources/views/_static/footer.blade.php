<footer class="footer">
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <a href="#" class="brand">
                        <img src="assets/img/logo.png" alt="">
                    </a>
                    <p>
                        {{$footertext->footer_right}}
                    </p>
                </div>
                <!--end col-md-5-->
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <nav>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="/">Начало</a>
                                    </li>
                                    <li>
                                        <a href="{{route('contact.index')}}">Контакти</a>
                                    </li>
                                    <li>
                                        <a href="{{route('usloviq.index')}}">Условия</a>
                                    </li>

                                    <li>
                                        <a href="{{route('gdpr.index')}}">GDPR</a>
                                    </li>
                                    <li>
                                        <a href="{{route('ad.create')}}">Публикуване</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <nav>
                                <ul class="list-unstyled">
                                    @auth
                                        <li>
                                            <a href="{{route('dashboard.myads')}}">Моите Обяви</a>
                                        </li>
                                    @endauth


                                        @guest
                                            <li>
                                                <a href="{{route('login')}}">Вход</a>
                                            </li>
                                            <li>
                                                <a href="{{route('register')}}">Регистрация</a>
                                            </li>

                                        @endguest

                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--end col-md-3-->
                <div class="col-md-4">
                    <h2>Контакти</h2>
                    <address>

                        <figure>
                            {{$footertext->footer_right}}
                        </figure>
                        <br>

                        <a href="{{route('contact.index')}}" class="btn btn-info text-caps btn-framed">Контакти</a>
                    </address>
                </div>
                <!--end col-md-4-->
            </div>
            <!--end row-->
        </div>
        <div class="background">
            <div class="background-image original-size">
                <img src="{{asset('assets/img/hero-background-image-02.jpg')}}" alt="">
            </div>
            <!--end background-image-->
        </div>
        <!--end background-->
    </div>
</footer>



<!--end page-->

<script src="{{asset('assets/js/jquery-3.3.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBEDfNcQRmKQEyulDN8nGWjLYPm8s4YB58&libraries=places"></script>
<!--<script type="text/javascript" src="http://maps.google.com/maps/api/js"></script>-->
<script src="{{asset('assets/js/selectize.min.js')}}"></script>
<script src="{{asset('assets/js/masonry.pkgd.min.js')}}"></script>
<script src="{{asset('assets/js/icheck.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/js/custom.js')}}"></script>
<script defer src="https://use.fontawesome.com/releases/v5.9.0/js/all.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.9.0/js/v4-shims.js"></script>

@yield('scripts')

</body>
</html>
