
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="gameport">
    @yield('meta')
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Varela+Round" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/fonts/font-awesome.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/fonts/font-awesome2.css')}}" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.9.0/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.9.0/css/v4-shims.css">
    <link rel="stylesheet" href="{{asset('assets/css/selectize.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/user.css')}}">

    <title>@yield('title')</title>
    <style>

    </style>
    @yield('css')
</head>
<body>
<div class="page home-page">
    <!--*********************************************************************************************************-->
    <!--************ HERO ***************************************************************************************-->
    <!--*********************************************************************************************************-->
    <header class="hero">
        <div class="hero-wrapper">
            <!--============ Secondary Navigation ===============================================================-->
            <div class="secondary-navigation">
                <div class="container">
                    <ul class="left">

                    </ul>
                    <!--end left-->
                    <ul class="right">

                        @auth
                            @if(Auth::user()->is_admin)
                                <li>
                                    <a href="{{route('admin.panel.welcome')}}">
                                        <i class="fas fa-settings"></i>Admin Panel
                                    </a>
                                </li>
                            @endif
                            <li>
                                <a href="{{route('dashboard.editprofile')}}">
                                    <i class="fa fa-user-astronaut"></i>{{Auth::user()->username}}
                                </a>
                            </li>
                            <li class="">
                                <a href="/dashboard/myads">
                                    <i class="fa fa-heart"></i>Моите Обяви
                                </a>
                            </li>
                            <li class="">
                            <a href="{{route('messages.index')}}">
                                    <i class="fa fa-box"></i>Съобщения
                                </a>
                            </li>
                            <li class="">
                                <a href="{{route('logout')}}">
                                    <i class="fa fa-arrow-right"></i>Изход
                                </a>
                            </li>
                        @endauth

                        @guest
                                <li>
                                    <a href="{{route('login')}}">
                                        <i class="fa fa-sign-in"></i>Вход
                                    </a>
                                </li>

                                <li>
                                    <a href="{{route('register')}}">
                                        <i class="fa fa-pencil-square-o"></i>Регистрация
                                    </a>
                                </li>
                        @endguest



                    </ul>
                    <!--end right-->
                </div>
                <!--end container-->
            </div>
            <!--============ End Secondary Navigation ===========================================================-->
            <!--============ Main Navigation ====================================================================-->
            <div class="main-navigation">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light justify-content-between">

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbar">
                            <!--Main navigation list-->
                            <ul class="navbar-nav">
                                <li class="nav-item active">
                                    <a class="nav-link" href="/"><i class="fas fa-home fa-fw"></i>Начало</a>

                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="/search"><i class="fas fa-search fa-fw"></i>Търси</a>

                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" href="/ads/kategorii"><i class="fa fa-folder fa-fw"></i>Категории</a>


                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" href="{{route('region.index')}}"><i class="fa fa-fw fa-city
"></i> Градове</a>

                                </li>
                                <li class="nav-item has-child">
                                    <a class="nav-link" href="#"><i class="fa fa-kiss-wink-heart fa-fw"></i>Тип</a>
                                    <!-- 2nd level -->
                                    <ul class="child">
                                        @forelse($adtypes as $type)
                                            <li class="nav-item">
                                                <a href="/ads/tip/{{$type->id}}/{{$type->slug}}" class="nav-link">{{$type->name}}</a>
                                            </li>
                                        @empty

                                            @endforelse



                                    </ul>

                                </li>
                                <li class="nav-item has-child">
                                    <a class="nav-link" href="/users"><i class="fa fa-user fa-fw"></i>Профили</a>
                                    <!-- 2nd level -->
                                    <ul class="child">
                                        <li class="nav-item">
                                            <a href="/users" class="nav-link">Всички Профили</a>
                                        </li>

                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('articles.index')}}"><i class="fa fa-blog"></i> Блог</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/contact"><i class="fa fa-mail-bulk fa-fw"></i>Контакти</a>
                                </li>
                                <li class="nav-item">
                                    <a href="/ads/create" class="btn btn-success text-caps btn-rounded btn-framed btn-sm
"><i class="fa fa-mail-forward"></i>Публикувай</a>
                                </li>


                            </ul>
                            <!--Main navigation list-->
                        </div>
                        <!--end navbar-collapse-->
                    </nav>

                    <!--end navbar-->
                </div>
                <!--end container-->

            </div>
