@include('_static.header')
<div class="col-lg-7 mx-auto">
    @include('components.messages')

</div>
<!--============ End Main Navigation ================================================================-->
<!--============ Page Title =========================================================================-->
<div class="page-title">

    <div class="container">
        <h1 class="opacity-40 center">
            {!! $homepagetext->homepage_lead !!}

        </h1>
        <p class="p-5"> {{$homepagetext->homepage_normal}}</p>
    </div>
    <!--end container-->

</div>


<!--============ End Page Title =====================================================================-->
<!--============ Hero Form ==========================================================================-->
{{--@include('components.searchhome')--}}
<!--============ End Hero Form ======================================================================-->
<div class="background">
    <div class="background-image original-size">
        <img src="{{asset('assets/img/hero-background-image-02.jpg')}}" alt="">
    </div>
    <!--end background-image-->
</div>
<!--end background-->
</div>
<!--end hero-wrapper-->
</header>
<!--end hero-->

<!--*********************************************************************************************************-->
<!--************ CONTENT ************************************************************************************-->
<!--*********************************************************************************************************-->

<section class="content">
    <!--============ Categories =============================================================================-->

    <!--end block-->
    <!--============ End Categories =========================================================================-->
    <section class="block">
        <div class="container">
           @yield('content')
        </div>
        <!--end container-->
    </section>
    <!--end block-->
</section>
<!--end content-->
@include('_static.footer')