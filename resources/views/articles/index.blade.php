@extends('layouts.app')
@section('title','Блог')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8">


                @forelse($articles as $article)
                    <article class="blog-post clearfix">
                        <div class="article-title">
                            <h2><a href="{{route('blogarticles.single',
                            ['article_id' => $article->id,
                            'category_slug' => $article->category->slug,
                            'article_slug' => $article->slug
                            ])
                            }}">{{$article->title}}</a></h2>
                            <div class="tags framed">
                                <a href="#" class="tag">{{$article->category->name}}</a>
                            </div>
                        </div>
                        <a href="">
                            <img src="{{$article->image}}" alt="" style="width:750px; height: 400px;">
                        </a>

                        <div class="meta">
                            <figure>
                                <a href="#" class="icon">
                                    <i class="fa fa-user"></i>
                                    Автор: {{$article->user->username}}
                                </a>
                            </figure>
                            <figure>
                                <i class="fa fa-calendar-o"></i>
                                {{$article->created_at->diffForHumans()}}
                            </figure>
                        </div>
                        <div class="blog-post-content">
                            <p>
                                {!! str_limit(strip_tags($article->body),250) !!}

                            </p>
                            <a href="{{route('blogarticles.single',
                            ['article_id' => $article->id,
                            'category_slug' => $article->category->slug,
                            'article_slug' => $article->slug
                            ])
                            }}" class="btn btn-primary btn-framed detail">Прочети</a>
                        </div>
                    </article>
                @empty
                    <h4>Няма Добавени Статии</h4>
                @endforelse

            </div>
            <!--end col-md-8-->

           @include('components.blogsidebar')
            <!--end col-md-3-->
        </div>
    </div>
    <!--end container-->

@stop