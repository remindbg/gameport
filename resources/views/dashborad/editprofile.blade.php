@extends('layouts.app')

@section('title','Редакция на Профил')

@section('content')

    <div class="row">
        <div class="col-md-3">
           @include('components.leftsidebar')
        </div>
        <!--end col-md-3-->
        <div class="col-md-9">
            <form class="form" method="POST" action="{{route('store.profile')}}" enctype="multipart/form-data">
                @csrf
            {{ csrf_field() }}
            {{--                @method('post')--}}
                <div class="row">
                    <div class="col-md-8">
                        <h2>Потребителски Настройки</h2>
                        <ul class="nav nav-pills" id="myTab-pills" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active show" id="obshtitab" data-toggle="tab" href="#obshti" role="tab" aria-controls="one-pills" aria-expanded="true" aria-selected="true"><i class="fa fa-globe fa-fw"></i>Общи</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="kontaktitab" data-toggle="tab" href="#kontakti" role="tab" aria-controls="two-pills" aria-selected="false"><i class="fa fa-mail-bulk fa-fw"></i>Контакти</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent-pills">
                            <div class="tab-pane fade show active" id="obshti" role="tabpanel" aria-labelledby="obshti">
                                <section>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="name" class="col-form-label required">Потребителско име</label>
                                                <input name="username" disabled type="text"
                                                       class="form-control" id="name" placeholder="username" value="{{$user->username}}" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="email" class="col-form-label required">Имейл Адрес</label>
                                                <input name="email" disabled type="text"
                                                       class="form-control" id="email" placeholder="email" value="{{$user->email}}" required>
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="gender" class="col-form-label">Пол</label>
                                                <select name="gender" id="gender" name="gender" data-placeholder="Пол">
                                                    <option value="Мъж" {{$user->gender  === 'Мъж' ?  'selected' : ''}}>Мъж</option>
                                                    <option value="Жена" {{$user->gender  === 'Жена' ?  'selected' : ''}}>Жена</option>
                                                    <option value="Друго" {{$user->gender  === 'Друго' ?  'selected' : ''}}>Друго</option>
                                                    <option value="0" {{$user->gender  === 0 ?  'selected' : ''}}>Не казвам</option>

                                                </select>
                                            </div>
                                        </div>
                                        <!--end form-group-->
                                        <div class="col-lg-8">

                                            <div class="form-group">
                                                <label for="name" class="col-form-label required">Вашите Имена</label>
                                                <input name="names" type="text" class="form-control" id="names" placeholder="" value="{{$user->names}}" >
                                            </div>
                                            <!--end form-group-->

                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="age" class="col-form-label">Години</label>
                                                <input name="age" type="number" class="form-control" id="age" placeholder="" min="11" max="90" value="{{$user->age}}" >

                                            </div>
                                        </div>

                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label for="oblast" class="col-form-label">Област</label>
                                                <select name="region_id" id="oblast" data-placeholder="">
                                                    <option value="0">Не казвам</option>

                                                @foreach($regions as $region)

                                                        <option value="{{$region->id}}"
                                                        {{$user->region_id == $region->id ? 'selected' : ''}}>
                                                            {{$region->name}}
                                                        </option>
                                                    @endforeach


                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label for="location" class="col-form-label ">Град/ Село * <span class="small">Не е задължително</span></label>
                                                <input name="city" type="text" class="form-control"
                                                       id="" placeholder="Местоживеене" value="{{$user->city}}">
                                            </div>
                                        </div>
                                    </div>



                                    <!--end col-md-8-->

                                    <!--end row-->

                                    <!--end form-group-->
                                    <div class="form-group">
                                        <label for="about" class="col-form-label">Информация за Вас</label>
                                        <textarea name="about" id="about" class="form-control" rows="4">{{$user->about}}</textarea>
                                    </div>
                                    <!--end form-group-->
                                </section>
                            </div>
                            <div class="tab-pane fade" id="kontakti" role="tabpanel" aria-labelledby="kontakti">
                                <section>
                                    <h2>Информация за Контакт</h2>
                                    <div class="form-group">
                                        <label for="phone" class="col-form-label">Телефон</label>
                                        <input name="phone" type="text" class="form-control" id="phone" name="phone" placeholder="" value="{{$user->phone}}">
                                    </div>




                                    <!--end form-group-->
                                </section>
                            </div>

                        </div>
                    </div>
                    <!--end col-md-8-->
                    <div class="col-md-4">
                        <hr>
                        <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save fa-fw"></i>Запази</button>
                        <hr>

                        <div class="profile-image">

                            <div class="image background-image">
                                <img src="{{asset('/uploads/'.$user->id . '/'.$user->avatar)}}" alt="">
                            </div>
                            <div class="single-file-input">
                                <input type="file" id="avatar" name="avatar">
                                <div class="btn btn-framed btn-primary small">Качете снимка</div>
                            </div>

                        </div>
                    </div>
        </div>
                    <!--end col-md-3-->

            </form>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('assets/plugins/typeahead.js')}}"></script>
    <script>

    </script>


@endsection










