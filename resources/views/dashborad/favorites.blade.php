@extends('layouts.app')

@section('title','Запазени Обяви')

@section('content')

    <div class="row">
        <div class="col-md-3">
            @include('components.leftsidebar')
        </div>
        <!--end col-md-3-->
        <div class="col-md-9">
            <!--============ Section Title===================================================================-->
            <div class="section-title clearfix">

                <div class="float-right d-xs-none thumbnail-toggle">
                    <a href="#" class="change-class" data-change-from-class="list" data-change-to-class="grid" data-parent-class="items">
                        <i class="fa fa-th"></i>
                    </a>
                    <a href="#" class="change-class active" data-change-from-class="grid" data-change-to-class="list" data-parent-class="items">
                        <i class="fa fa-th-list"></i>
                    </a>
                </div>
            </div>
            <!--============ Items ==========================================================================-->
                <div class="items grid grid-xl-3-items grid-lg-3-items grid-md-3-items">
                    @forelse ($ads as $ad )


                            <div class="item">
                                <div class="ribbon-featured">{{$ad->ads->created_at->diffforhumans()}}</div>
                                <!--end ribbon-->
                                <div class="wrapper">
                                    <div class="image">
                                        <h3>
                                            <a href="#" class="tag category">{{$ad->ads->category->name}}</a>
                                            <a href="{{route('ad.single',[
                            'id' => $ad->ads->id,
                            'catslug' => $ad->ads->category->slug,
                            'uid' => $ad->ads->uid
                            ])}}" class="title">{{$ad->ads->title}}</a>
                                            <span class="tag">{{$ad->ads->type->name}}</span>
                                        </h3>
                                        <a href="{{route('ad.single',[
                            'id' => $ad->ads->id,
                            'catslug' => $ad->ads->category->slug,
                            'uid' => $ad->ads->uid
                            ])}}" class="image-wrapper background-image">
                                            @if($ad->ads->images)
                                                @foreach($ad->ads->images as $image)
                                                    @if($image->is_first)
                                                        <img src="{!! asset($image->filepath) !!}" alt="">

                                                    @else
                                                        <img src="{{asset('assets/img/image-01.jpg')}}" alt="">
                                                    @endif

                                                @endforeach


                                            @else
                                                <img src="{{asset('assets/img/image-01.jpg')}}" alt="">

                                            @endif
                                        </a>
                                    </div>
                                    <!--end image-->
                                    <h4 class="location">
                                        <a href="#">{{$ad->ads->region->name}}</a>
                                    </h4>
                                    <div class="price">{{$ad->ads->price}}лв</div>
                                    <div class="meta">
                                        <figure>
                                            <i class="fa fa-calendar-o"></i>{{$ad->ads->created_at->diffforhumans()}}
                                        </figure>
                                        <figure>

                                            <a href="{{route('users.single',$ad->ads->user->id)}}">
                                                <i class="fa fa-user"></i>{{$ad->ads->user->username}}
                                            </a>
                                        </figure>
                                    </div>
                                    <!--end meta-->
                                    <div class="description">
                                        <p>{{$ad->ads->description}}</p>
                                    </div>
                                    <!--end description-->
                                </div>
                                <form action="{{route('favorite.destroy',$ad->id)}}" method="POST">
                                    {{csrf_field()}}
                                    <button class="btn btn-block btn-danger">Премахване от Любими</button>
                                </form>
                            </div>



                    @empty
                        <h3>Няма Добавени Обяви</h3>
                @endforelse
                <!--end item-->

                    <!--end item-->
                </div>


            <!--end items-->
        </div>
        <!--end col-md-9-->
    </div>
    <!--end row-->


@endsection


@section('scripts')



@endsection










