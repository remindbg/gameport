@extends('layouts.app')

@section('title','Всички Ваши Обяви')

@section('content')

    <div class="row">
        <div class="col-md-3">
            @include('components.leftsidebar')
        </div>
        <!--end col-md-3-->
        <div class="col-md-9">
            <form class="form" method="POST" action="" enctype="multipart/form-data">
                @csrf
                {{ csrf_field() }}
                {{--                @method('post')--}}
                <div class="row">
                    <div class="col-md-12">
                        <h2>Вашите Обяви</h2>

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Заглавие</th>
                                <th>Цена</th>
                                <th>Дата</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($user->ads->sortByDesc('created_at') as $ad)
                                <tr>
                                    <td><a href="{{route('ad.edit',$ad->id)}}">{{$ad->title}}</a></td>
                                    <td>{{$ad->price}}</td>
                                    <td>{{$ad->created_at->diffForHumans()}}</td>
                                </tr>
                            @empty
                                Нямате Активни Обяви
                            @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>

            </form>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('assets/plugins/typeahead.js')}}"></script>
    <script>

    </script>


@endsection










