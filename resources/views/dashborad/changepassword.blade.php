@extends('layouts.app')

@section('title','Смяна на парола')

@section('content')

    <div class="row">
        <div class="col-md-3">
            @include('components.leftsidebar')
        </div>
        <!--end col-md-3-->
        <div class="col-md-9">
            <form class="form" method="POST" action="{{route('dashboard.updatepassword')}}" enctype="multipart/form-data">
                @csrf
                {{ csrf_field() }}
                {{--                @method('post')--}}
                <div class="row">
                    <div class="col-md-8">
                        <h2>Смяна на Парола</h2>

                        <div class="tab-content" id="myTabContent-pills">
                            <div class="tab-pane fade show active" id="obshti" role="tabpanel" aria-labelledby="obshti">
                                <section>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="name" class="col-form-label required">Въведете Нова Парола
                                                </label>
                                                <input name="password"  type="password"
                                                       class="form-control" id="name"
                                                        required>
                                            </div>
                                            <div class="form-group">
                                                <label for="email" class="col-form-label required">Въведете
                                                    Паролата Отново</label>
                                                <input name="password-confirm"  type="password"
                                                       class="form-control"
                                                       id="password-confirm"
                                                        required>
                                            </div>
                                            <hr>

                                            <button type="submit" class="btn btn-info">Смяна на паролата</button>
                                        </div>



                                    </div>

                                    <!--end col-md-8-->

                                    <!--end row-->


                                    <!--end form-group-->
                                </section>
                            </div>


                        </div>
                    </div>

                </div>
                <!--end col-md-3-->

            </form>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('assets/plugins/typeahead.js')}}"></script>
    <script>

    </script>


@endsection










