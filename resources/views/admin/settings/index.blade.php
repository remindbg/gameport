@extends('admin.layouts.app')
@section('title','Настройки на Сайта')

@section('content')
    <form action="{{route('admin.settings.update',1)}}" method="POST" enctype="multipart/form-data">
        @csrf()
        @method('put')
        <div class="row">
            <div class="col-lg-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Заглавие на Сайта</h3>
                    </div>
                    <div class="card-body">
                        <p class="text-sm"></p>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" value="{{$settings->title}}" name="title">
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-6">

                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">SEO Информация</h3>
                    </div>
                    <div class="card-body">

                        <div class="form-group">
                            <p class="text-sm">SEO Описание - не повече от 160, но гугъл показва около 115 на мобилни</p>
                            <textarea class="form-control" rows="3" name="description">{{$settings->description}}</textarea>
                        </div>
                    </div>

                </div>
            </div>
            <button type="submit" class="btn btn-block btn-success">Запазване</button>
        </div>
    </form>

@stop


@section('scripts')


@endsection