@extends('admin.layouts.app')
@section('title','Статични Текстове')

@section('content')
    <form action="{{route('admin.statictexts.update',1)}}" method="POST" enctype="multipart/form-data">
        @csrf()
        @method('put')
        <div class="row">

            <div class="col-lg-6">

                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Lead (мигащ ) цвят на началната страница</h3>
                    </div>
                    <div class="card-body">
                        default изглежда така:
                        <code>

                            &lt;h1 class=&quot;opacity-40 center&quot;&gt;
                            &lt;a href=&quot;#&quot;&gt;Купи&lt;/a&gt;, &lt;a href=&quot;#&quot;&gt;Продай&lt;/a&gt; Или  &lt;a href=&quot;#&quot;&gt;Размени&lt;/a&gt;


                        </code>

                        <div class="form-group">

                            <textarea class="form-control" rows="3"
                                      name="homepage_lead">{{$statictext->homepage_lead}}</textarea>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-6">

                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Нормален Текст ( параграф ) на началната страница</h3>
                    </div>
                    <div class="card-body">

                        <div class="form-group">

                            <textarea class="form-control" rows="3"
                                      name="homepage_normal">{{$statictext->homepage_normal}}</textarea>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-6">

                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Footer от Ляво</h3>
                    </div>
                    <div class="card-body">

                        <div class="form-group">

                            <textarea class="form-control" rows="3"
                                      name="footer_left">{{$statictext->footer_left}}</textarea>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-6">

                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Footer текст от дясно</h3>
                    </div>
                    <div class="card-body">

                        <div class="form-group">

                            <textarea class="form-control" rows="3"
                                      name="footer_right">{{$statictext->footer_right}}</textarea>
                        </div>
                    </div>

                </div>
            </div>
            <button type="submit" class="btn btn-block btn-success">Запазване</button>
        </div>
    </form>

@stop


@section('scripts')


@endsection