@extends('admin.layouts.app')
@section('title','Потребители')

@section('content')
    <div class="card-body table-responsive p-0">
        <table class="table table-hover">
            <tbody><tr>
                <th>ID</th>
                <th>username</th>
                <th>Дата Регистрация</th>
                <th>Active??</th>
                <th>Обяви</th>
                <th>Опции</th>
            </tr>
            @forelse ($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->username}}</td>
                    <td>{{$user->created_at->diffForHumans()}}</td>
                    <td>{!! $user->is_active ? '<span class="tag tag-success">Да</span>' : '<span class="tag tag-danger">Не</span>' !!}</td>
                    <td>
                        @if($user->ads)
                            {{$user->ads->count()}}
                            @else
                        Няма
                            @endif

                    </td>
                    <td>
                        <a href="{{route('admin.users.edit', $user->id)}}"><button class="btn btn-primary
                        btn-sm">Редакция/Преглед</button></a>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-danger-{{$user->id}}">
                            Изтриване
                        </button>
                    </td>

                </tr>
                <hr>
                <div class="modal fade" id="modal-danger-{{$user->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Изтриване на Потребител?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.users.destroy',$user->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Изтриване</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Отказ</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            @empty
                <hr>
                Няма Потребители
            @endforelse
            </tbody>
        </table>
        {{ $users->links() }}
    </div>

    <!-- /.modal -->
@stop