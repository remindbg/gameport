@extends('admin.layouts.app')
@section('title','Потребител с username: ' . $user->username)

@section('content')
    <form action="{{route('admin.users.update',$user->id)}}" method="POST" enctype="multipart/form-data">
        @csrf('put')
        <div class="row">
            <div class="col-lg-8">
                @method('put')
                @if(!$user->email_verified_at)
                    <div class="callout callout-warning">
                        <h5>Информация: Имейл Адреса не е потвърден</h5>
                    </div>
                @else
                    <div class="callout callout-success">
                        <h5> Имейл Адреса  е потвърден</h5>
                    </div>
                @endif

                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Основна Информация</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3">
                                <p>Потребителско Име</p>
                                <div class="input-group mb-3">

                                    <div class="input-group-prepend">
                                        <span class="input-group-text">@</span>
                                    </div>
                                    <input type="text" class="form-control" value="{{$user->username}}" name="username">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <p>Имейл Адрес</p>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                    </div>
                                    <input type="email" class="form-control" value="{{$user->email}}" name="email">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <p>Имена</p>
                                <div class="input-group mb-3">

                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-user"></i></span>
                                    </div>
                                    <input type="text" class="form-control" value="{{$user->names}}" name="names">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <p>Област</p>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-globe"></i></span>
                                    </div>
                                    <select class="form-control" id="region" name="region_id">
                                       @foreach($regions as $region)
                                        <option value="{{$region->id}}"
                                                {{$region->id === $user->region_id ? 'selected' : ''}}>{{$region->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <p>Град/ Село</p>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-city"></i></span>
                                    </div>
                                    <input type="text" class="form-control" value="{{$user->city}}" name="city">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <p>Години</p>
                                <input type="number" class="form-control" value="{{$user->age}}" name="age">

                            </div>
                            <div class="col-lg-3">
                                <p>Пол</p>

                                    <select class="form-control" name="gender">
                                        <option value="Мъж" {{$user->gender  == 'Мъж' ?  'selected' : ''}}>Мъж</option>
                                        <option value="Жена" {{$user->gender  == 'Жена' ?  'selected' : ''}}>Жена</option>
                                        <option value="Друго" {{$user->gender  == 'Друго' ?  'selected' : ''}}>Друго</option>
                                        <option value="0" {{$user->gender  == 0 ?  'selected' : ''}}>Не казвам</option>
                                    </select>
                            </div>
                            <div class="col-lg-6">
                                <p>Телефон</p>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                    </div>
                                    <input type="text" class="form-control" value="{{$user->phone}}" name="phone">
                                </div>
                            </div>
                            <div class="col-lg-12">

                                <div class="form-group">
                                    <label>За Потребителя</label>
                                    <textarea class="form-control" rows="3" name="about">{{$user->about}}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <p>Аватар: </p>
                                <div class="form-group">
                                    <img src="" alt="">
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <p>Аватар: </p>
                                <div class="form-group">
                                    <input type="text" class="form-control" value="{{$user->avatar}}" name="avatar">
                                </div>
                            </div>
                        </div>

                    </div>
                    <button type="submit" class="btn btn-block btn-info">Редакция</button>
                </div>

            </div>
            <div class="col-lg-4">
                <div class="card card-danger">
                    <div class="card-header">
                        <h3 class="card-title">Статус</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Смяна на Статус</label>
                            <select class="form-control" name="is_active">
                                <option value="1" {!! $user->is_active == 1 ? 'selected' : '' !!}>Активен</option>
                                <option value="0" {!! $user->is_active == 0 ? 'selected' : '' !!}>Неактивен</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">IP Адрес</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <th>Последен Вход</th>
                                <th>IP Адрес</th>
                            </tr>
                            <tr>
                                <td>{{$user->last_login_at}} -  {{$user->last_login_at ? $user->last_login_at->diffForHumans() : ''}}</td>
                                <td>{{$user->last_login_ip ? $user->last_login_ip : 'няма информация'}}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <p>Админ?</p>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-users-cog"></i></span>
                            </div>
                            <select class="form-control text-danger" name="is_admin">
                                <option value="0" {{$user->is_admin == 0 ? 'selected':'' }} >Не</option>
                                <option value="1" {!!$user->is_admin == true ? 'selected' : ''!!}>Да</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@stop


@section('scripts')


@endsection