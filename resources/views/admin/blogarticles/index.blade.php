@extends('admin.layouts.app')
@section('title','Категории ( на Блога )')

@section('content')
    <div class="card-body table-responsive p-0">
        <a href="{{route('admin.blogarticles.create')}}">
            <button type="button" class="btn btn-info btn-sm">Създаване на Статия</button>
        </a>
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>Заглавие</th>
                <th>slug</th>
                <th>Категория</th>
                <th>Опции</th>
            </tr>
            @forelse($articles as $article)
                <tr>
                    <td>{{$article->id}}</td>
                    <td>{{$article->title}}</td>
                    <td>{{$article->slug}}</td>
                    <td>{{$article->category->name}}</td>
                    <td>
                        <a href="{{route('admin.blogarticles.edit', $article->id)}}"><button class="btn btn-primary btn-sm">Редакция/Преглед</button></a>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-danger-{{$article->id}}">
                            Изтриване
                        </button>
                    </td>
                </tr>
                <div class="modal fade" id="modal-danger-{{$article->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Изтриване на Статия?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.blogarticles.destroy',$article->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Изтриване</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Отказ</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            @empty
                <hr>
                Няма добавени Статии
            @endforelse
            </tbody>
        </table>
    </div>

    <!-- /.modal -->
@stop