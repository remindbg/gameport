@extends('admin.layouts.app')
@section('title','Редакция на Форумна Публикация')

@section('content')
    <form action="{{route('admin.blogarticles.update',$article->id)}}" method="POST" enctype="multipart/form-data">
        @csrf()
        @method('put')
        <div class="row">
            <div class="col-lg-7">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Заглавие</h3>
                    </div>
                    <div class="card-body">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" value="{{$article->title}}" name="title">
                        </div>
                    </div>

                </div>

            </div>
            <div class="col-lg-5">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">SEO Заглавие ( не е задължително )</h3>
                    </div>
                    <div class="card-body">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" value="{{$article->seo_title}}" name="seo_title">
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-7">

                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Текст</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <textarea class="form-control" rows="6" name="body" id="body">{{$article->body}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">SEO Описание</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <textarea class="form-control" rows="3" name="seo_description">{{$article->seo_description}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Допълнително</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">

                            <div class="col-lg-5">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title">Изображение</h3>
                                    </div>
                                    <div class="card-body">
                                        @if ($article->image)
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control"
                                                       value="{{$article->image}}"
                                                       name="image">
                                            </div>

                                            <img src="{{asset($article->image)}}" alt="" style="width: 600px;height: 400px;">
                                        @else
                                            <div class="form-group">
                                                <label for="exampleInputFile"></label>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file" name="image" class="custom-file-input" id="exampleInputFile">
                                                        <label class="custom-file-label" for="exampleInputFile">Избиране</label>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card-header">
                                    <h3 class="card-title">Категория</h3>
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="form-controlactive">Категория</label>
                                        <select class="form-control text-info" id="form-controlactive" name="category_id">
                                            @foreach ($categories as $category)
                                                <option value="{{$category->id}}"
                                                        {{$category->id == $article->category->id ? 'selected' : ''}}
                                                        >
                                                    {{$category->name}}</option>

                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="card-header">
                                    <h3 class="card-title">Активна?</h3>
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="form-controlactive">Направи статията активна?</label>
                                        <select class="form-control text-info" id="form-controlactive" name="is_active">
                                            <option value="1" {{$article->is_active ? 'selected' : ''}} >Да</option>
                                            <option value="0" {{$article->is_active ? '' : 'selected'}} >Не</option>
                                        </select>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-block btn-success">Редакция</button>
        </div>
    </form>

@stop

@section('scripts')
    <script src="{{asset('tinymce/tinymce.min.js')}}"></script>
    <script>tinymce.init({selector:'#body'});</script>

@endsection