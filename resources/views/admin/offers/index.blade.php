@extends('admin.layouts.app')
@section('title','Преглед на оферти')

@section('content')
    <div class="card-body table-responsive p-0">

        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>От Потребител</th>
                <th>Към Потребител</th>
                <th>Относно Обява</th>
                <th>Размяна за:</th>

                <th>Доплащане?</th>
                <th>Цена?</th>
                <th>Статус</th>
                <th>Опции</th>
            </tr>

            @forelse($offers as $offer)

                <tr>
                    <td>{{$offer->id}}</td>
                    <td>{{$offer->sender->username}}</td>
                    <td>{{$offer->receiver->username}}</td>
                    <td><a href="{{route('admin.ad.edit',$offer->ad->id)}}">{{str_limit($offer->ad->title,8)
                    }}</a></td>
                    <td><a href="{{route('admin.ad.edit',$offer->for_which_ad->id)}}">{{str_limit
                    ($offer->for_which_ad->title,8)}}</a></td>
                    <td>
                        @if ($offer->is_doplashtane)
                            Да
                            @else
                            Не
                        @endif

                    </td>
                    <td>
                        {{$offer->price}}
                    </td>
                    <td>
                        @if ($offer->is_accepted)
                            Приета
                            @elseif($offer->is_rejected)
                            Отказана
                            @else
                            Изчакване

                        @endif
                    </td>
                    <td>
                        <a href="{{route('admin.offers.edit', $offer->id)}}"><button class="btn btn-primary
                        btn-sm">Редакция/Преглед</button></a>
                        <button type="button" class="btn btn-danger btn-sm"
                                data-toggle="modal" data-target="#modal-danger-{{$offer->id}}">
                            Изтриване
                        </button>
                    </td>
                </tr>
                <hr>
                <div class="modal fade" id="modal-danger-{{$offer->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Изтриване на Оферта???</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.offers.destroy',$offer->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Изтриване</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Отказ</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            @empty
                <hr>
                Няма добавени Области
            @endforelse








            </tbody>
        </table>
    </div>

    <!-- /.modal -->
@stop
