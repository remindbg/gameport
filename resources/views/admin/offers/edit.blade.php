@extends('admin.layouts.app')
@section('title','Редакция на Оферта')

@section('content')
    <form action="{{route('admin.offers.update',$offer->id)}}" method="POST" enctype="multipart/form-data">
        @csrf()
        @method('put')
        <div class="row">

            <div class="col-lg-12">

                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Информация</h3>
                    </div>
                    <div class="card-body">

                        <div class="form-group">
                            <p class="text-sm">Текст към офертата изпратен от: <a href="{{route('admin.users.edit',
                            $offer->sender->id)}}">
                                    {{$offer->sender->username}}
                                </a></p>
                            <textarea class="form-control" rows="3" name="body">{{$offer->body}}</textarea>
                        </div>
                    </div>
                    <p>Потребителят е направил оферта със своята обява, която е : <a href="{{route('admin.ad.edit',
                    $offer->for_which_ad->id)}}">   {{$offer->for_which_ad->title}}</a>
                     </p>

                </div>
            </div>
            <button type="submit" class="btn btn-block btn-success">Редакция</button>
        </div>
    </form>

@stop


@section('scripts')


@endsection
