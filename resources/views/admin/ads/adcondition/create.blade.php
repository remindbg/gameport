@extends('admin.layouts.app')
@section('title','Създаване на ново Състояние')

@section('content')
    <form action="{{route('admin.adcondition.store')}}" method="POST" enctype="multipart/form-data">
        @csrf()
        @method('post')
        <div class="row">
            <div class="col-lg-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Състояние</h3>
                    </div>
                    <div class="card-body">
                        <p class="text-sm">Име( Ново / Употребявано и т.н.. )  ( slug-a се генерира автоматично, след това може да се редактира ако не е удачен</p>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" value="" name="name">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">

                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">SEO Информация</h3>
                    </div>
                    <div class="card-body">
                        <div class="col-lg-8">
                            <p class="text-sm">SEO Тайтъл (около 60 символа - може и ASCII икони/символи - визулизират се от гугъл в търсачката</p>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" value="" name="seo_title">
                            </div>
                        </div>
                        <div class="form-group">
                            <p class="text-sm">SEO Описание - не повече от 160, но гугъл показва около 115 на мобилни</p>
                            <textarea class="form-control" rows="3" name="seo_description"></textarea>
                        </div>
                    </div>

                </div>
            </div>
            <button type="submit" class="btn btn-block btn-success">Създаване</button>
        </div>
    </form>

@stop


@section('scripts')


@endsection