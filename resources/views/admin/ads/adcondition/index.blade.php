@extends('admin.layouts.app')
@section('title','Състояния на Обяви')

@section('content')
    <div class="card-body table-responsive p-0">
        <a href="{{route('admin.adcondition.create')}}">
            <button type="button" class="btn btn-info btn-sm">Създаване на Състояние</button>
        </a>
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>Име</th>
                <th>slug</th>
                <th>Брой Обяви</th>
                <th>Опции</th>
            </tr>
            @forelse($adconditions as $condition)
                <tr>
                    <td>{{$condition->id}}</td>
                    <td>{{$condition->name}}</td>
                    <td>{{$condition->slug}}</td>
                    <td>{{$condition->ads->count()}}</td>
                    <td>
                        <a href="{{route('admin.adcondition.edit', $condition->id)}}"><button class="btn btn-primary btn-sm">Редакция/Преглед</button></a>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-danger-{{$condition->id}}">
                            Изтриване
                        </button>
                    </td>
                </tr>
                <div class="modal fade" id="modal-danger-{{$condition->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Изтриване на Област?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.adcondition.destroy',$condition->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Изтриване</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Отказ</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            @empty
                <hr>
                Няма добавени Състояния
            @endforelse
            </tbody>
        </table>
    </div>

    <!-- /.modal -->
@stop
