@extends('admin.layouts.app')
@section('title','Създаване на нов Тип на доставка')

@section('content')
    <form action="{{route('admin.addelivery.store')}}" method="POST" enctype="multipart/form-data">
        @csrf()
        @method('post')
        <div class="row">
            <div class="col-lg-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Състояние</h3>
                    </div>
                    <div class="card-body">
                        <p class="text-sm">Име( Лично Предаване, Няма значение, Еконт само и т.н.. )
                        </p>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" value="" name="name">
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-block btn-success">Създаване</button>
        </div>
    </form>

@stop


@section('scripts')


@endsection