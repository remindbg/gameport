@extends('admin.layouts.app')
@section('title','Създаване на тип доставка')

@section('content')
    <div class="card-body table-responsive p-0">
        <a href="{{route('admin.addelivery.create')}}">
            <button type="button" class="btn btn-info btn-sm">Създаване на нов Тип Доставка</button>
        </a>
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>Име</th>
                <th>Опции</th>
            </tr>
            @forelse($addeliveries as $delivery)
                <tr>
                    <td>{{$delivery->id}}</td>
                    <td>{{$delivery->name}}</td>

                    <td>
                        <a href="{{route('admin.addelivery.edit', $delivery->id)}}"><button class="btn btn-primary btn-sm">Редакция/Преглед</button></a>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-danger-{{$delivery->id}}">
                            Изтриване
                        </button>
                    </td>
                </tr>
                <div class="modal fade" id="modal-danger-{{$delivery->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Изтриване на Област?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.addelivery.destroy',$delivery->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Изтриване</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Отказ</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            @empty
                <hr>
                Няма добавени Тип Доставка / Предавания
            @endforelse
            </tbody>
        </table>
    </div>

    <!-- /.modal -->
@stop