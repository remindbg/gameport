@extends('admin.layouts.app')
@section('title','Редакция на Тип Доставка')

@section('content')
    <form action="{{route('admin.addelivery.update',$addelivery->id)}}" method="POST" enctype="multipart/form-data">
        @csrf()
        @method('put')
        <div class="row">
            <div class="col-lg-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Име</h3>
                    </div>
                    <div class="card-body">
                        <p class="text-sm">Име</p>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" value="{{$addelivery->name}}" name="name">
                        </div>
                    </div>

                </div>
            </div>

            <button type="submit" class="btn btn-block btn-success">Редакция</button>
        </div>
    </form>

@stop


@section('scripts')


@endsection