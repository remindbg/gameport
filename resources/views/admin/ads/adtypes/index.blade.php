@extends('admin.layouts.app')
@section('title','Тип на Обяви')

@section('content')
    <div class="card-body table-responsive p-0">
        <a href="{{route('admin.adtype.create')}}">
            <button type="button" class="btn btn-info btn-sm">Създаване на Тип</button>
        </a>
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>Име</th>
                <th>slug</th>
                <th>Брой Обяви</th>
                <th>Опции</th>
            </tr>
            @forelse($adtypes as $type)
                <tr>
                    <td>{{$type->id}}</td>
                    <td>{{$type->name}}</td>
                    <td>{{$type->slug}}</td>
                    <td>TODO</td>
                    <td>
                        <a href="{{route('admin.adtype.edit', $type->id)}}"><button class="btn btn-primary btn-sm">Редакция/Преглед</button></a>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-danger-{{$type->id}}">
                            Изтриване
                        </button>
                    </td>
                </tr>
                <div class="modal fade" id="modal-danger-{{$type->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Изтриване на Тип?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.adtype.destroy',$type->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Изтриване</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Отказ</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            @empty
                <hr>
                Няма добавени Състояния
            @endforelse
            </tbody>
        </table>
    </div>

    <!-- /.modal -->
@stop