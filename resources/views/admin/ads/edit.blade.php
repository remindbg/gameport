@extends('admin.layouts.app')
@section('title','Редактиране на Обява')

@section('content')
    <form action="{{route('admin.ad.update',$ad->id)}}" method="POST" enctype="multipart/form-data">
        @csrf()
        @method('put')
        <div class="row">

            <div class="col-lg-5">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Заглавие</h3>
                    </div>
                    <div class="card-body">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" value="{{$ad->title}}" name="title">
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-7">

                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Описание на Обявата</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <textarea class="form-control" rows="7" name="description">{{$ad->description}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Цена</h3>
                    </div>
                    <div class="card-body">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" value="{{$ad->price}}" name="price">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Потребител</h3>
                    </div>
                    <div class="card-body">
                        <hr>
                        Обявата е качена от: <a href="{{route('admin.users.edit',$ad->user->id)
                        }}">{{$ad->user->username}}</a>
                    </div>

                </div>
            </div>
            <div class="col-lg-4">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Прегледа</h3>
                    </div>
                    <div class="card-body">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" value="{{$ad->views}}" name="views">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Допълнително</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="form-controlactive">Активна?</label>
                                    <select class="form-control text-info" id="form-controlactive" name="is_active">
                                        <option value="1" {{$ad->is_active ?'selected' : ''}}>Да</option>
                                        <option value="0" {{$ad->is_active ?'' : 'selected'}} >Не</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="form-controlactive">Категория?</label>
                                    <select class="form-control text-info" id="form-controlactive" name="category_id">
                                       @foreach($categories as $category)
                                        <option value="{{$category->id}}" {{$category->id
                                        == $ad->category->id ? 'selected' : ''}}>{{$category->name}}</option>
                                           @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="form-controlactive">Състояние?</label>
                                    <select class="form-control text-info" id="form-controlactive" name="adcondition_id">
                                        @forelse($conditions as $condition)
                                            <option value="{{$condition->id}}" >{{$condition->name}}</option>
                                        @empty
                                            Няма
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="form-controlactive">Област?</label>
                                    <select class="form-control text-danger" id="form-controlactive" name="region_id">
                                        @forelse($regions as $region)
                                            <option value="{{$region->id}}"
                                                {{$region->id == $ad->region->id ? 'selected' :
                                                ''}}>{{$region->name}}</option>
                                        @empty
                                            <option value="" >Няма Създаден Тип</option>
                                        @endforelse

                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="form-controlactive">Тип?</label>
                                    <select class="form-control text-danger" id="form-controlactive" name="adtype_id">
                                        @forelse($types as $type)
                                            <option value="{{$type->id}}"
                                                {{$type->id == $ad->type->id ? 'selected' :
                                                ''}}>{{$type->name}}</option>
                                        @empty
                                            <option value="" >Няма Създаден Тип</option>
                                        @endforelse

                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="form-controlactive">Доставка?</label>
                                    <select class="form-control text-info" id="form-controlactive"
                                            name="addelivery_id">
                                        @forelse($deliveries as $delivery)
                                            <option value="{{$delivery->id}}" {{$delivery->id
                                        == $ad->delivery->id ? 'selected' : ''}}>{{$delivery->name}}</option>
                                        @empty
                                            Няма
                                        @endforelse

                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="form-controlactive">Статус на Обявата?</label>
                                    <select class="form-control text-info" id="form-controlactive" name="adstatus">


                                            <option value="1" {{$ad->adstatus == 1 ? 'selected' : ''}} >Няма приети
                                                оферти</option>
                                            <option value="2"  {{$ad->adstatus == 2 ? 'selected' : ''}}>Изчакване на
                                                оферта(приета)</option>
                                            <option value="3"  {{$ad->adstatus == 3 ? 'selected' : ''}}>Приключила
                                                Оферта</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="form-controlactive">Приемат ли се оферти?</label>
                                    <select class="form-control text-info" id="form-controlactive" name="accept_offers">
                                        <option value="1" {{$ad->adstatus == 1 ? 'selected' : ''}} >Да</option>
                                        <option value="0"  {{$ad->adstatus == 0 ? 'selected' : ''}}>Не</option>
                                            Оферта</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card card-body">
                            Обявата има: 3 Снимки, които могат да се редактират от тук: todo button
                            <hr>
                            Обявата има 3 Оферти, 0 приети
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-block btn-success">Редакция</button>
        </div>
    </form>

@stop


@section('scripts')


@endsection
