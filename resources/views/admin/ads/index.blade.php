@extends('admin.layouts.app')
@section('title','Всички Обяви')

@section('content')
    <div class="card-body table-responsive p-0">
        <a href="{{route('admin.ad.create')}}">
            <button type="button" class="btn btn-info btn-sm">Добавяне на нова Обява</button>
        </a>
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>Заглавие</th>
                <th>Потребител</th>
                <th>Дата</th>
                <th>Активна?</th>
                <th>Изображения?</th>
                <th>Категория</th>

                <th>Опции</th>
            </tr>
            @forelse($ads as $ad)
                <tr>
                    <td>{{$ad->id}}</td>
                    <td>{{$ad->title}}</td>

                    <td>
                        @if($ad->user)
                            {{$ad->user->username}}
                        @else
                            Няма
                        @endif
                    </td>
                    <td>{{$ad->created_at->diffForHumans()}}</td>
                    <td>
                        @if($ad->is_active)
                            <button type="button" class="btn btn-success btn-sm">Да</button>
                            @else
                            <button type="button" class="btn btn-danger btn-sm">Не</button>
                        @endif
                    </td>
                    <td>
                        @if ($ad->images)
                            {{$ad->images->count()}}
                            @else
                            Няма
                        @endif
                    </td>
                    <td>
                        @if($ad->category)
                        {{$ad->category->name}}
                            @else
                        Няма
                            @endif
                    </td>

                    <td>
                        <a href="{{route('admin.ad.edit', $ad->id)}}"><button class="btn btn-primary btn-sm">Редакция/Преглед</button></a>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-danger-{{$ad->id}}">
                            Изтриване
                        </button>
                    </td>
                </tr>
                <div class="modal fade" id="modal-danger-{{$ad->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Изтриване на Обява?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.ad.destroy',$ad->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Изтриване</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Отказ</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            @empty
                <hr>
                Няма добавени Обяви Все Още
            @endforelse
            </tbody>
        </table>
    </div>

    <!-- /.modal -->
@stop
