<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">

            </div>
            <div class="info">
                <a href="#" class="d-block">Admin</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Начало
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="/panel/settings" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Настройки
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/panel/settings" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Настройки</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.statictexts.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Текстове</p>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="" class="nav-link">
                        <i class="nav-icon fas fa-pager"></i>
                        <p>
                            Текст Страници
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('admin.pages.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Текст Страници</p>
                            </a>
                        </li>

                    </ul>
                </li>


                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            Обяви
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">{{$adcount}}</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/panel/ad" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Всички Обяви</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="/panel/adtype" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Тип Обяви</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/panel/adcondition" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Състояния на Обявата</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/panel/addelivery" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Тип Доставки</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{route('admin.offers.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-gift"></i>
                        <p>
                             Оферти
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right"></span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('admin.offers.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Всички Оферти</p>
                            </a>
                        </li>




                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{route('admin.ratings.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-stop"></i>
                        <p>
                            Рейтинг
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right"></span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('admin.ratings.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Всички Рейтинги</p>
                            </a>
                        </li>




                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-folder"></i>
                        <p>
                            Категории
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">{{$catcount}}</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('admin.category.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Всички</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.category.create')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Нова Категория</p>
                            </a>
                        </li>



                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Потребители
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">{{$usercount}}</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/panel/users" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Всички</p>
                            </a>
                        </li>


                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-info"></i>
                        <p>
                            Доклади
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right"></span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('admin.reports.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Всички</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="/panel/region" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Области
                            <i class="right fas fa-angle-left"></i>
                            <span class="badge badge-info right">{{$regioncount}}</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/panel/region" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Всички Области</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/panel/region/create" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Нова Област</p>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="/panel/region" class="nav-link">
                        <i class="nav-icon fas fa-mail-bulk"></i>
                        <p>
                            Съобщения
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/panel/messages/" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Всички Съобщения</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            Блог
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">6</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('admin.blogarticles.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Всички Публикации</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.blogarticles.create')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Нова Блог Публикация</p>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{route('admin.blogcategories.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            Блог Категории
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">6</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('admin.blogcategories.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Всички Категории</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.blogcategories.create')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Нова Категория</p>
                            </a>
                        </li>

                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
