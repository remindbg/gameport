@extends('admin.layouts.app')
@section('title','Редактиране на Обява')

@section('content')

        @csrf()
        @method('post')
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <h3 class="profile-username text-center">Доклада е изпратен от :</h3>
                            <img class="profile-user-img img-fluid img-circle" src="{{asset('admin/dist/img/user4-128x128.jpg')
                        }}"
                                 alt="">
                        </div>

                        <h3 class="profile-username text-center">{{$report->user->username}}</h3>

                        <p class="text-muted text-center">{{$report->created_at->diffForHumans()}}</p>
                        <div class="row ">
                            <div class="col-lg-5">
                                <ul class="list-group list-group-unbordered text-center">
                                    <li class="list-group-item">
                                        <b class="text-danger">Към Потребител:</b>
                                        <a class="float-right">{{$report->ad->user->username}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Относно Обява:</b> <a class="float-right">543</a>
                                    </li>
                                    <li class="list-group-item">
                                        <p>{{$report->body}}</p>
                                    </li>

                                </ul>

                            </div>
                        </div>


                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <a href="/panel/reports"> <button type="" class="btn btn-block
            btn-success">Назад</button></a>


        </div>


@stop


@section('scripts')


@endsection
