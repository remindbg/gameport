@extends('admin.layouts.app')
@section('title','Създаване на новa Обява')

@section('content')
    <form action="{{route('admin.ad.store')}}" method="POST" enctype="multipart/form-data">
        @csrf()
        @method('post')
        <div class="row">
            <div class="col-lg-5">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Заглавие</h3>
                    </div>
                    <div class="card-body">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" value="" name="title">
                        </div>
                    </div>


                </div>
            </div>
            <div class="col-lg-7">

                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Описание на Обявата</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <textarea class="form-control" rows="3" name="description"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Допълнително</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="form-controlactive">Активна?</label>
                                    <select class="form-control text-info" id="form-controlactive" name="is_active">
                                        <option value="0">Не</option>
                                        <option value="1" >Да</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="form-controlactive">Състояние?</label>
                                    <select class="form-control text-info" id="form-controlactive" name="adcondition_id">
                                        @forelse($conditions as $condition)
                                            <option value="{{$condition->id}}" >{{$condition->name}}</option>
                                        @empty
                                            Няма
                                        @endforelse

                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="form-controlactive">Тип?</label>
                                    <select class="form-control text-danger" id="form-controlactive" name="adtype_id">
                                        @forelse($types as $type)
                                            <option value="{{$type->id}}" >{{$type->name}}</option>
                                        @empty
                                            <option value="" >Няма Създаден Тип</option>
                                        @endforelse

                                    </select>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-block btn-success">Създаване</button>
        </div>
    </form>

@stop


@section('scripts')


@endsection