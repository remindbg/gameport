@extends('admin.layouts.app')
@section('title','Всички Обяви')

@section('content')
    <div class="card-body table-responsive p-0">

        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>От Потребител</th>
                <th>Към Потребител</th>
                <th>Обява</th>
                <th>Дата</th>
                <th>Опции</th>
            </tr>
            @forelse($reports->sortByDesc('created_at') as $report)
                <tr>
                    <td>{{$report->id}}</td>
                    <td class="bg-info"><a href="{{route('admin.users.edit',$report->user->username)
                    }}">{{$report->user->username}}</a></td>
                    <td class="bg-red"><a href="{{route('admin.users.edit',$report->ad->user->id)
                    }}">{{$report->ad->user->username}}</a></td>
                    <td>{{$report->ad->title}}</td>
                    <td>{{$report->created_at->diffForHumans()}}</td>
                    <td>
                        <a href="{{route('admin.reports.edit', $report->id)}}">
                            <button class="btn btn-primary btn-sm">Преглед</button>
                        </a>
                        <button type="button" class="btn btn-danger btn-sm"
                                data-toggle="modal" data-target="#modal-danger-{{$report->id}}">
                            Изтриване
                        </button>
                    </td>
                </tr>
                <div class="modal fade" id="modal-danger-{{$report->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Изтриване на Рипорта?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.reports.destroy',$report->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Изтриване</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Отказ</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            @empty
                <hr>
                Няма добавени Рипорти Все Още
            @endforelse
            </tbody>
        </table>
    </div>

    <!-- /.modal -->
@stop