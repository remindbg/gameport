@extends('admin.layouts.app')
@section('title','Създаване на нова Област')

@section('content')
    <form action="{{route('admin.region.update',$region->id)}}" method="POST" enctype="multipart/form-data">
        @csrf()
        @method('put')
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Области/ Региони - Редакция</h3>
                    </div>
                    <div class="card-body">

                        <div class="input-group mb-3">
                            <p class="mr-4">Име на Област</p>
                            <input type="text" class="form-control" value="{{$region->name}}" name="name">
                        </div>
                        <div class="input-group mb-3 ">
                            <p class="mr-4">SEO slug</p>
                            <input type="text" class="form-control" value="{{$region->slug}}" name="name">
                        </div>
                        <hr>

                        <div class="input-group mb-3">
                            <p class="mr-4">SEO Тайтъл </p>
                            <input type="text" class="form-control" value="" name="seo_title">
                        </div>
                        <div class="form-group">
                            <p class="text-sm">SEO Описание</p>
                            <textarea class="form-control" rows="3" name="seo_description"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-block btn-success">Редакция</button>
        </div>
    </form>

@stop


@section('scripts')


@endsection