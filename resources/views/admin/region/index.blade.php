@extends('admin.layouts.app')
@section('title','Области')

@section('content')
    <div class="card-body table-responsive p-0">
        <a href="{{route('admin.region.create')}}">
            <button type="button" class="btn btn-info btn-sm" >
                Нова Област
            </button></a>
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>Име</th>
                <th>slug</th>
                <th>Брой Обяви</th>
                <th>Брой Потребители</th>
                <th>Опции</th>
            </tr>

            @forelse($regions as $region)

                <tr>
                    <td>{{$region->id}}</td>
                    <td>{{$region->name}}</td>
                    <td>{{$region->slug}}</td>
                    <td>TODO</td>
                    <td>
                        @if($region->users)
                        {{$region->users->count()}}
                            @else няма
                            @endif
                    </td>
                    <td>
                        <a href="{{route('admin.region.edit', $region->id)}}"><button class="btn btn-primary btn-sm">Редакция/Преглед</button></a>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-danger-{{$region->id}}">
                            Изтриване
                        </button>
                    </td>
                </tr>
                <hr>
                <div class="modal fade" id="modal-danger-{{$region->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Изтриване на Област?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.region.destroy',$region->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Изтриване</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Отказ</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            @empty
                <hr>
               Няма добавени Области
            @endforelse








            </tbody>
        </table>
    </div>

    <!-- /.modal -->
@stop