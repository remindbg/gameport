@extends('admin.layouts.app')
@section('title','Редактиране на Рейтинг')

@section('content')

    @csrf()
    @method('post')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <h3 class="profile-username text-center">Рейтинга  е изпратен от :</h3>
                        <img class="profile-user-img img-fluid img-circle" src="{{asset('/uploads/'
                        .$rating->ratingauthor->id .
                         '/'.$rating->ratingauthor->avatar)}}"
                             alt="">
                    </div>

                    <h3 class="profile-username text-center"><a href="{{route('admin.users.edit',$rating->user->id)
                                    }}">{{$rating->ratingauthor->username}}</a></h3>

                    <p class="text-muted text-center">{{$rating->created_at->diffForHumans()}}</p>
                    <div class="row ">
                        <div class="col-lg-5">
                            <ul class="list-group list-group-unbordered text-center">
                                <li class="list-group-item">
                                    <b class="text-danger">Към Потребител:</b>
                                    <a class="float-right" href="{{route('admin.users.edit',$rating->user->id)
                                    }}">{{$rating->user->username}}</a>
                                </li>

                                <li class="list-group-item">
                                    {{$rating->body}}
                                </li>

                            </ul>

                        </div>
                    </div>


                </div>
                <form action="{{route('admin.ratings.update',$rating->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <button class="btn btn-block btn-info" type="submit">Одобряване на Рейтинга</button>

                </form>
                <!-- /.card-body -->
            </div>
        </div>



    </div>


@stop


@section('scripts')


@endsection
