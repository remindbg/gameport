@extends('admin.layouts.app')
@section('title','Преглед на Рейтинги')

@section('content')
    <div class="card-body table-responsive p-0">

        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>От Потребител</th>
                <th>Към Потребител</th>
                <th>Текст</th>
                <th>Рейтинг число</th>
                <th>Опции</th>
            </tr>

            @forelse($ratings->sortByDesc('updated_at') as $rating)

                <tr>
                    <td>{{$rating->id}}</td>
                    <td>{{$rating->ratingauthor->username}}</td>
                    <td>{{$rating->user->username}}</td>
                    <td>{{str_limit($rating->body,15)}}</td>
                    <td>{{$rating->is_approved ? 'Одобрен' : 'Не одобрен'}}</td>

                    <td>
                        {{$rating->rating_number}}
                    </td>

                    <td>
                        <a href="{{route('admin.ratings.edit', $rating->id)}}"><button class="btn btn-primary
                        btn-sm">Редакция/Преглед</button></a>
                        <button type="button" class="btn btn-danger btn-sm"
                                data-toggle="modal" data-target="#modal-danger-{{$rating->id}}">
                            Изтриване
                        </button>
                    </td>
                </tr>
                <hr>
                <div class="modal fade" id="modal-danger-{{$rating->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Изтриване на Рейтинга???</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.ratings.destroy',$rating->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Изтриване</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Отказ</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            @empty
                <hr>
                Няма добавени Рейтинги
            @endforelse

            </tbody>
        </table>
    </div>

    <!-- /.modal -->
@stop
