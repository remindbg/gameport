@extends('admin.layouts.app')
@section('title','Преглед на дискусия')

@section('content')


    <div class="card card-widget">
        <div class="card-footer card-comments">
            @foreach($conversation->messages->sortByDesc('created_at') as $message)

            <div class="card-comment">
                <!-- User image -->
                <div class="comment-text">
                    <span class="username">
                      {{$message->user->username}}
                      <span class="text-muted float-right">{{$message->created_at->diffForHumans()}}</span>
                    </span><!-- /.username -->
                    {{$message->message}}
                </div>
                <!-- /.comment-text -->
            </div>
            @endforeach
            <!-- /.card-comment -->

            <!-- /.card-comment -->
        </div>
        <!-- /.card-footer -->

        <!-- /.card-footer -->
    </div>
@stop