@extends('admin.layouts.app')
@section('title','Стартирани Дискусии')

@section('content')
    <div class="card-body table-responsive p-0">

        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>Стартирал Дискусията</th>
                <th>Участник 2</th>
                <th>Брой Съобщения</th>
                <th>Дата</th>
                <th>Опции</th>
            </tr>

            @forelse($conversations as $message)

                <tr>
                    <td>{{$message->id}}</td>
                    <td>{{$message->sender->username}}</td>
                    <td>{{$message->receiver->username}}</td>
                    <td>{{$message->messages()->count()}}</td>
                    <td>{{$message->created_at->diffForHumans()}}</td>

                    <td>
                        <a href="{{route('admin.messages.edit', $message->id)}}"><button class="btn btn-primary btn-sm">Редакция/Преглед</button></a>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-danger-{{$message->id}}">
                            Изтриване
                        </button>
                    </td>
                </tr>
                <hr>
                <div class="modal fade" id="modal-danger-{{$message->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Изтриване на Чат?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.messages.destroy',$message->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Изтриване</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Отказ</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            @empty
                <hr>
                Няма Съобщения
            @endforelse








            </tbody>
        </table>
    </div>

    <!-- /.modal -->
@stop