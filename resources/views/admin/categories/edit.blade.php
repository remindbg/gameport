@extends('admin.layouts.app')
@section('title','Редакция на Категория')

@section('content')
    <form action="{{route('admin.category.update',$category->id)}}" method="POST" enctype="multipart/form-data">
        @csrf()
        @method('put')
        <div class="row">
            <div class="col-lg-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Име</h3>
                    </div>
                    <div class="card-body">
                        <p class="text-sm">Име  ( slug-a се генерира автоматично, след това може да се редактира ако не е удачен</p>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" value="{{$category->name}}" name="name">
                        </div>
                    </div>
                    <div class="card-body">
                        <p class="text-sm">Иконка</p>
                        <div class="">
                            <span class="input-group-addon"><i class="{{$category->icon}}"></i></span>
                            <hr>

                            <input type="text" class="form-control icp icp-auto my"  name="icon" value="{{$category->icon}}">
                        </div>

                    </div>

                    <div class="card-body">
                        <p class="text-sm">slug</p>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" value="{{$category->slug}}" name="slug">
                        </div>
                    </div>
                    <div class="card-body">
                        <p class="text-sm">Родител</p>
                        <div class="form-group">
                            <select class="form-control" id="" name="parent_id">
                                <option value="0">-без Родител</option>
                                @forelse($categories as $categoryfor)
                                    @if($categoryfor->id == $category->id)
                                    @else
                                        <option value="{{$categoryfor->id}}
                                        {{$categoryfor->parent_id == $categoryfor->id ? 'selected' : ''}}">{{$categoryfor->name}}</option>
                                    @endif

                                @empty
                                    <p>Няма добавени</p>
                                @endforelse
                            </select>
                        </div>
                    </div>

            </div>
            </div>

            <div class="col-lg-6">

                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">SEO Информация</h3>
                    </div>
                    <div class="card-body">
                        <div class="col-lg-8">
                            <p class="text-sm">SEO Тайтъл (около 60 символа - може и ASCII икони/символи - визулизират се от гугъл в търсачката</p>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" value="{{$category->seo_title}}" name="seo_title">
                            </div>
                        </div>
                        <div class="form-group">
                            <p class="text-sm">SEO Описание - не повече от 160, но гугъл показва около 115 на мобилни</p>
                            <textarea class="form-control" rows="3" name="seo_description">{{$category->seo_description}}</textarea>
                        </div>
                    </div>

                </div>
            </div>
            <button type="submit" class="btn btn-block btn-success">Редакция</button>
        </div>
    </form>


@stop


@section('scripts')

    <script src="{{asset('admin/iconpicker/dist/js/fontawesome-iconpicker.min.js')}}"></script>
    <script>
        $('.my').iconpicker();

    </script>

@endsection


@section('css')
    <link rel="stylesheet" href="{{asset('admin/iconpicker/dist/css/fontawesome-iconpicker.css')}}" type="text/css">
    <style>
        .fade.in{
            opacity: 1;
        }
    </style>

@endsection