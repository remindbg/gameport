@extends('admin.layouts.app')
@section('title','Категории ( на обявите )')

@section('content')
    <div class="card-body table-responsive p-0">
        <a href="{{route('admin.category.create')}}">
            <button type="button" class="btn btn-info btn-sm">Създаване на Категория</button>
        </a>
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>Име</th>
                <th>slug</th>
                <th>родител</th>
                <th>иконка</th>
                <th>Опции</th>
            </tr>
            @forelse($categories as $category)
                <tr>
                    <td>{{$category->id}}</td>
                    <td>{{$category->name}}</td>
                    <td>{{$category->slug}}</td>
                    <td>
                       @if($category->parent)
                            {{$category->parent->name}}
                           @else
                           Няма
                        @endif
                    </td>
                    <td>
                        @if($category->icon)
                            <i class="{{$category->icon}}"></i>
                        @else
                            Няма
                        @endif
                    </td>


                    <td>
                        <a href="{{route('admin.category.edit', $category->id)}}"><button class="btn btn-primary btn-sm">Редакция/Преглед</button></a>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-danger-{{$category->id}}">
                            Изтриване
                        </button>
                    </td>
                </tr>
                <div class="modal fade" id="modal-danger-{{$category->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Изтриване на Тип?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.category.destroy',$category->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Изтриване</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Отказ</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            @empty
                <hr>
                Няма добавени Категории
            @endforelse
            </tbody>
        </table>
    </div>

    <!-- /.modal -->
@stop