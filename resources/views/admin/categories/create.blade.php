@extends('admin.layouts.app')
@section('title','Създаване на нова Категория')

@section('content')
    <form action="{{route('admin.category.store')}}" method="POST" enctype="multipart/form-data">
        @csrf()
        @method('post')
        <div class="row">
            <div class="col-lg-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Категория</h3>
                    </div>
                    <div class="card-body">
                        <p class="text-sm">Име</p>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" value="" name="name">
                        </div>
                    </div>
                    <div class="card-body">
                        <p class="text-sm">Иконка</p>
                        <div class="">
                            <span class="input-group-addon"><i class="fa fa-check"></i></span>
                            <hr>

                            <input type="text" class="form-control icp icp-auto my" value="" name="icon">
                        </div>

                    </div>

                    <div class="card-body">
                        <p class="text-sm">Родител</p>
                        <div class="form-group">
                            <select class="form-control" id="" name="parent_id">
                                <option value="0">-без Родител</option>
                                @forelse($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @empty
                                    <p>Няма добавени</p>
                                @endforelse
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">SEO Информация</h3>
                    </div>
                    <div class="card-body">
                        <div class="col-lg-8">
                            <p class="text-sm">SEO Тайтъл (около 60 символа - може и ASCII икони/символи - визулизират се от гугъл в търсачката</p>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" value="" name="seo_title">
                            </div>
                        </div>
                        <div class="form-group">
                            <p class="text-sm">SEO Описание - не повече от 160, но гугъл показва около 115 на мобилни</p>
                            <textarea class="form-control" rows="3" name="seo_description"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-block btn-success">Създаване</button>
        </div>
    </form>

@stop


@section('scripts')

    <script src="{{asset('admin/iconpicker/dist/js/fontawesome-iconpicker.min.js')}}"></script>
    <script>
        $('.my').iconpicker();

    </script>

@endsection


@section('css')
    <link rel="stylesheet" href="{{asset('admin/iconpicker/dist/css/fontawesome-iconpicker.css')}}" type="text/css">
    <style>
        .fade.in{
            opacity: 1;
        }
    </style>

@endsection