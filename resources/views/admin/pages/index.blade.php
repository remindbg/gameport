@extends('admin.layouts.app')
@section('title','Текстове по страници')

@section('content')
    <form action="{{route('admin.pages.update',1)}}" method="POST" enctype="multipart/form-data">
        @csrf()
        @method('put')
        <div class="row">

            <div class="col-lg-12">

                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Страница с Контакти</h3>
                    </div>
                    <div class="card-body">

                        <div class="form-group">

                            <textarea class="form-control" rows="10"
                                      name="contact">{{$page->contact}}</textarea>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-12">

                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Страница с Условия</h3>
                    </div>
                    <div class="card-body">

                        <div class="form-group">

                            <textarea class="form-control" rows="10"
                                      name="usloviq">{{$page->usloviq}}</textarea>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-lg-12">

                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Страница с GDPR</h3>
                    </div>
                    <div class="card-body">

                        <div class="form-group">

                            <textarea class="form-control" rows="10"
                                      name="gdpr">{{$page->gdpr}}</textarea>
                        </div>
                    </div>

                </div>
            </div>
            <button type="submit" class="btn btn-block btn-success">Запазване</button>
        </div>
    </form>

@stop


@section('scripts')
    <script src="{{asset('tinymce/tinymce.min.js')}}"></script>
    <script>tinymce.init({selector:'textarea'});</script>

@endsection