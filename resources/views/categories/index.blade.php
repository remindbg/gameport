@extends('layouts.appwithsearch')
@section('title','Всички Категории Обяви')

@section('meta')

@endsection


@section('content')

    <section class="content">
        <div class="container">
            <h2>Категории</h2>
            <ul class="categories-list clearfix">
                @forelse($categories as $category)
                   @if($category->parent_id == 0)
                        <li>
                            <i class="category-icon">
                                <i class="{{$category->icon}}"></i>
                            </i>
                            <h3><a href="{{route('category.single',['id' => $category->id, 'slug' => $category->slug])}}">{{$category->name}}</a></h3>

                            <div class="sub-categories">
                                @foreach($category->children as $parent)
                                    <a href="{{route('category.single',['id' => $parent->id, 'slug' => $parent->slug])}}">{{$parent->name}}</a>
                                @endforeach
                            </div>
                        </li>
                       @endif

                @empty
                    <h2>Няма Категории</h2>
                @endforelse
            </ul>

            <!--end categories-list-->
        </div>
    </section>
    <!--end content-->
@endsection
