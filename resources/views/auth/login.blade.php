@extends('layouts.app')
@section('title','Регистрация')


@section('content')

    <section class="block">

        <div class="container">

            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-4 col-md-10 col-sm-8">
                    <form class="form clearfix" method="POST" action="{{route('login')}}">
                        @csrf
                                <div class="form-group">
                                    <label for="email" class="col-form-label required">Потребителско име или Имейл</label>
                                    <input name="email" id="email" type="text"
                                           class="form-control" placeholder="потребителско име" required
                                           value="" >
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-form-label required">Парола</label>
                                    <input name="password" type="password" class="form-control" id="password" placeholder="Вашата парола" required>
                                </div>

                        <div class="d-flex justify-content-between align-items-baseline">
                            <label>
                                <input type="checkbox" name="remember" id="remember" value="">
                                Запомни Ме
                            </label>

                            <button type="submit" class="btn btn-primary">Вход</button>
                        </div>
                        @if (Route::has('password.request'))
                            <a class="btn text-info" href="{{ route('password.request') }}">
                                {{ __('Забравена Парола') }}
                            </a>
                        @endif

                    </form>
                    <hr>

                </div>
                <!--end col-md-6-->
            </div>
            <!--end row-->

@endsection
