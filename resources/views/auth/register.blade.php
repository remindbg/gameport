@extends('layouts.app')
@section('title','Регистрация')


@section('content')

    <section class="block">

        <div class="container">

            <div class="row justify-content-center">
                <div class="col-xl-10 col-lg-10 col-md-10 col-sm-8">
                    <form class="form clearfix" method="POST" action="{{route('register')}}">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="username" class="col-form-label required">Потребителско Име</label>
                                    <input name="username" type="text"
                                           class="form-control {{ $errors->has('username')? 'is-invalid' : '' }}" id="username" placeholder="потребителско име" required
                                            value="{{ old('username') }}" >
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="email" class="col-form-label required">Имейл</label>
                                    <input name="email" type="email"
                                           class="form-control  {{ $errors->has('email')? 'is-invalid' : '' }} " id="email" placeholder="Имейл Адрес" required
                                           value="{{ old('email') }}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="password" class="col-form-label required">Парола</label>
                                    <input name="password" type="password" class="form-control  {{ $errors->has('password')? 'is-invalid' : '' }}" id="password" placeholder="Вашата парола" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="password-confirm" class="col-form-label required">Повторете паролата</label>
                                    <input name="password_confirmation" type="password" class="form-control  {{ $errors->has('password')? 'is-invalid' : '' }}" id="password-confirm" placeholder="Вашата парола отново" required>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex justify-content-between align-items-baseline">

                            <button type="submit" class="btn btn-primary">Регистрация</button>
                        </div>
                    </form>
                    <hr>
                    <p>
                      Останалите полета можете да попълните след регистрацията от вашата профилна страница
                    </p>
                    <p>
                       С регистрацията се съгласявате с нашите  <a href="/pages/usloviq" class="link">условия</a>
                    </p>
                </div>
                <!--end col-md-6-->
            </div>
            <!--end row-->

@endsection
