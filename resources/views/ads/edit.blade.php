@extends('layouts.app')

@section('title','Редакция на Обява')

@section('content')

    <div class="row">
        <div class="col-md-3">
            @include('components.leftsidebar')
        </div>
        <!--end col-md-3-->
        <div class="col-md-9">

            <form class="form" method="POST" action="{{route('ad.update',$ad->id)}}" enctype="multipart/form-data">
                @csrf
                {{ csrf_field() }}
                {{--                @method('post')--}}
                <div class="row">
                    <div class="col-md-8">
                        <h2>Редакция на Обява:</h2>
                        <h4>{{$ad->title}}</h4>


                            @if (!$ad->is_active)
                            <div class="alert alert-danger alert-block">
                                <strong>Тази Обява не е активна</strong>
                             </div>
                            @endif



                        <div class="tab-content" id="myTabContent-pills">
                            <div class="tab-pane fade show active" id="obshti" role="tabpanel" aria-labelledby="obshti">
                                <section>
                                    <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="is_active" class="col-form-label required">Статус на Обявата</label>
                                                    <select name="is_active" id="is_active" data-placeholder="Да">
                                                        <option value="1" {{$ad->is_active ? 'selected' : ''}}>Активна</option>
                                                        <option value="0" {{$ad->is_active ? '' : 'selected'}}>Неактивна</option>
                                                    </select>
                                                </div>
                                            </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="title" class="col-form-label required">Заглавие</label>
                                                <input name="title"  type="text"
                                                       class="form-control" id="title" placeholder="" value="{{$ad->title}}" required>
                                            </div>

                                            <!--end form-group-->
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="price" class="col-form-label required">Цена</label>
                                                <input name="price" type="number" value="{{$ad->price}}" class="form-control" id="price">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="about" class="col-form-label">Описание на Обявата</label>
                                                <textarea name="description" id="about" class="form-control" rows="6">{{$ad->description}}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="adtype_id" class="col-form-label">Тип на Обявата</label>
                                                <select name="adtype_id" id="adtype_id" data-placeholder="Тип">
                                                    @foreach($adtypes as $type)
                                                    <option value="{{$type->id}}" {{$ad->adtype_id  == $type->id ?  'selected' : ''}}>{{$type->name}}</option>

                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="adcondition_id" class="col-form-label">Състояние</label>
                                                <select name="adcondition_id" id="adcondition_id" data-placeholder="Пол">
                                                    @foreach($adconditions as $condition)
                                                        <option value="{{$condition->id}}"
                                                            {{$ad->adcondition_id  == $condition->id ?  'selected' : ''}}>{{$condition->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="addeliveries_id" class="col-form-label">Тип на Доставка</label>
                                                <select name="addelivery_id" id="addeliveries_id" data-placeholder="Пол">
                                                    @foreach($addeliveries as $delivery)
                                                        <option value="{{$delivery->id}}"
                                                            {{$ad->addelivery_id  == $delivery->id ?  'selected' : ''}}>{{$delivery->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <!--end form-group-->

                                    </div>

                                    <div class="row">

                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label for="oblast" class="col-form-label">Област</label>
                                                <select name="region_id" id="oblast" data-placeholder="">

                                                    @foreach($regions as $region)

                                                        <option value="{{$region->id}}"
                                                            {{$ad->region_id == $region->id ? 'selected' : ''}}>
                                                            {{$region->name}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="accept_offer" class="col-form-label required">Приемате ли Оферти?</label>
                                                <select name="accept_offer" id="accept_offer" data-placeholder="Състояние">
                                                    <option value="1" {{$ad->accept_offers ? 'selected' : ''}}>Да</option>
                                                    <option value="0" {{$ad->accept_offers ? '' : 'selected'}}>Не</option>

                                                </select>
                                            </div>
                                        </div>



                                    </div>

                                </section>
                            </div>


                        </div>
                    </div>
                    <!--end col-md-8-->
                    <div class="col-md-4">
                        <hr>
                        <button type="submit" class="btn btn-success btn-block"><i class="fa fa-save fa-fw"></i>Запази</button>
                        <hr>


                    </div>

                </div>


                    <h2>Изображения</h2>
                <a href="{{route('ad.editimages',$ad->id)}}" class="btn btn-sm btn-info">Редакция на
                    Изображенията</a>






            </form>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('assets/plugins/typeahead.js')}}"></script>
    <script src="{{asset('assets/js/jQuery.MultiFile.min.js')}}"></script>


    <script>

    </script>


@endsection

@section('css')

@endsection











