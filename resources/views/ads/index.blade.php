@extends('layouts.appwithsearch')
@section('title')
   Всички Обяви
@endsection


@section('content')

    <section class="content">
        <section class="block">
            <div class="container">
                <div class="float-right d-xs-none thumbnail-toggle">
                    <a href="#" class="change-class active" data-change-from-class="list" data-change-to-class="grid" data-parent-class="items">
                        <i class="fa fa-th"></i>
                    </a>
                    <a href="#" class="change-class" data-change-from-class="grid" data-change-to-class="list" data-parent-class="items">
                        <i class="fa fa-th-list"></i>
                    </a>
                </div>
                <!--============ Section Title===================================================================-->
                <div class="section-title clearfix">
                    <div class="float-left float-xs-none">
                        <h2>Разгледайте Всички Обяви</h2>

                    </div>

                </div>
                <!--============ Items ==========================================================================-->
                <!--============ Recent Ads =============================================================================-->
                <section class="block">
                    <div class="container">

                        <div class="items grid compact grid-xl-4-items grid-lg-3-items grid-md-2-items">
                            @php
                                $randomad = rand(1,$ads->count())
                            @endphp
                            @forelse ($ads as $ad)
                                @if ($loop->iteration == $randomad)

                                    <a href="/ads/create" class="item call-to-action">
                                        <div class="wrapper">
                                            <div class="title">
                                                <i class="fa fa-plus-square-o"></i>
                                                Добави Обява
                                            </div>
                                        </div>
                                    </a>
                                    @include('components.adsingle')
                                @else
                                    @include('components.adsingle')
                                @endif
                            @empty
                                Няма намерени Обяви
                            @endforelse
                            <a href="/ads"><button type="button" class="btn btn-info icon float-right width-100">Виж Всички Обяви<i
                                        class="fa fa-chevron-right"></i></button></a>

                        </div>
                        <div class="page-pagination">
                            <nav aria-label="Pagination">
                                {{$ads->links()}}
                            </nav>
                        </div>


                    </div>

                </section>

            </div>

        </section>

    </section>



@endsection
