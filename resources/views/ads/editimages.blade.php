@extends('layouts.app')

@section('title','Редакция на Обява')

@section('content')

    <div class="row">
        <div class="col-md-3">
            @include('components.leftsidebar')
        </div>
        <!--end col-md-3-->
        <div class="col-md-9">
                @csrf
                {{ csrf_field() }}
                {{--                @method('post')--}}
                <div class="row">
                    <div class="col-md-8">
                        <h2>Редакция на Изображения на обява: {{$ad->title}}</h2>
                        <section class="block">
                            <div class="container">
                                <small>Качени изображения: {{$imagescount}}</small> <br>
                                @if($imagescount == 6)
                                    <small>Качи ли сте максимален брой изображения. Ако искате да качите други -
                                        изтрийте някои от старите
                                    </small>
                                    <hr>

                                @else
                                <section>
                                    <h2>Изображения</h2>
                                    <form action="{{route('adimage.update',$ad->id)}}" method="POST" enctype="multipart/form-data">
                                        @csrf

                                        <div class="file-upload-previews"></div>
                                        <div class="file-upload">
                                            @if((6 - $imagescount == 1))
                                                <input type="file" name="images[]" class="file-upload-input with-preview
                                       "
                                                       maxlength="1"
                                                       accept="gif|jpg|png"
                                                       value="">
                                                <span><i class="fa fa-plus-circle"></i>До 1
                                                Изображениe</span>
                                            @else
                                                <input type="file" name="images[]" class="file-upload-input with-preview
                                        multi max-{{6 - $imagescount}}"
                                                       multiple title="До 6 Изображения"   maxlength="{{6 - $imagescount}}"
                                                       accept="gif|jpg|png"
                                                       value="">
                                                <span><i class="fa fa-plus-circle"></i>До {{6 - $imagescount}} Изображения</span>
                                            @endif


                                        </div>
                                        <hr>

                                        <button class="btn btn-info" type="submit">Обновяване</button>

                                    </form>

                                </section>
                                @endif


                                <div class="items list  grid-xl-6-items grid-lg-6-items grid-md-6-items">

                                @forelse($ad->images as $image)
                                        <div class="item">
                                        {{--                <div class="ribbon-featured">{{$ad->created_at->diffforhumans()}}</div>--}}
                                        <!--end ribbon-->
                                            <div class="wrapper">
                                                <div class="image">
                                                    <h3>
                                                        <a href="" class="tag category">Изображение
                                                            {{$loop->iteration}}</a>
                                                    </h3>
                                                    <a href="{{asset($image->filepath)}}" target="_blank" class="image-wrapper
                                                    background-image">

                                                                    <img src="{!! asset($image->filepath) !!}" alt="">

                                                    </a>
                                                </div>
                                                <!--end image-->



                                                <!--end meta-->

                                                <!--end description-->
                                                <form action="{{route('adimage.delete',$image->id)}}" method="POST">
                                                    @csrf
                                                    <button class="detail text-caps underline pull-left
                                                " type="submit" onclick="confirm('Сигурни ли сте?')
                                                ">Изтриване</button>
                                                </form>

                                            </div>
                                        </div>
                                    @empty


                                    @endforelse

                                </div>



                            </div>

                        </section>








                    </div>
                    <!--end col-md-8-->






                <!--end col-md-3-->


        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('assets/plugins/typeahead.js')}}"></script>

    <script>

    </script>
            <script src="{{asset('assets/js/jQuery.MultiFile.min.js')}}"></script>


@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css
">
@endsection











