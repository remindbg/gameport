@extends('layouts.appwithsearch')
@section('title','Обяви и оферти по Области')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}" type="text/css">

@endsection

@section('content')

    <section class="content">
        <section class="block">

            <div class="container">
                <h1>{{$ad->title}}</h1>

                <div class=" box">

                    <!--============ Listing Detail =============================================================-->
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <!--Gallery Carousel-->
                                <section>

                                    <!--end section-title-->

                                    @if(!$ad->images->isempty())


                                        <div class="gallery-carousel owl-carousel">
                                        @foreach($ad->images as $image)

                                                    <img src="{!! asset($image->filepath) !!}"
                                                         alt="" data-hash="{{$loop->iteration}}">

                                        @endforeach
                                        </div>


                                    @else
                                        <div class="gallery-carousel owl-carousel">


                                                <img src="{!! asset('noimage-small.png') !!}"
                                                     alt="" data-hash="1">


                                        </div>

                                    @endif

                                    <div class="gallery-carousel-thumbs owl-carousel">
                                        @if ($ad->images)
                                            @foreach($ad->images as $image)
                                                <a href="#{{$loop->iteration}}" class="owl-thumb active-thumb background-image">
                                                    <img src="{!! asset($image->filepath) !!}" alt="">
                                                </a>
                                                @endforeach
                                        @else

                                                <a href="#1" class="owl-thumb active-thumb background-image">
                                                    <img src="{!! asset('noimage-small.png') !!}" alt="">
                                                </a>

                                        @endif

                                    </div>
                                </section>
                                @if ($ad->is_active)
                                    <div class="row">
                                        <div class="col-md-6">
                                            @auth

                                                @if($ad->user->id != Auth::user()->id && !$ad->accept_offers)
                                                    <a href="#" class="btn btn-success btn-block button-ads-info"
                                                       data-toggle="tooltip" data-placement="top" title="Потребителят не желае да получава оферти">
                                                        <i class="fa fa-mail-forward"></i>Направи Оферта</a>



                                                @elseif ($ad->user->id == Auth::user()->id || !$ad->accept_offers)
                                                    <a href="#" class="btn btn-success btn-block button-ads-info"
                                                       data-toggle="tooltip" data-placement="top" title="Не можете да пращате оферти до ваши обяви">
                                                        <i class="fa fa-mail-forward"></i>Направи Оферта</a>
                                                @else
                                                    <a href="{{route('offer.create',$ad->id)}}" class="btn btn-success btn-block button-ads-info">
                                                        <i class="fa fa-mail-forward"></i>Направи Оферта</a>
                                                @endif

                                            @endauth

                                            @guest
                                                <a href="#" class="btn btn-success btn-block button-ads-info"
                                                   data-toggle="tooltip" data-placement="top" title="Нуждаете се от регистрация">
                                                    <i class="fa fa-mail-forward"></i>Направи Оферта</a>

                                            @endguest

                                        </div>
                                        <div class="col-md-6">
                                            @auth
                                                @if ($ad->user->id == Auth::user()->id)
                                                    <a href="#" class="btn btn-info btn-block button-ads-info"
                                                       data-toggle="tooltip" data-placement="top" title="Не можете да изпращате съобщение до себе си">
                                                        <i class="fa fa-mail-bulk"></i>
                                                        Съобщение
                                                    </a>
                                                @else
                                                    <a href="{{route('messages.compose',['adid' => $ad->id,'senderid'
                                                     => Auth::user()->id, 'receiver' => $ad->user->id])}}" class="btn
                                                     btn-info
                                                     btn-block
                                                     button-ads-info">
                                                        <i class="fa fa-mail-bulk"></i>
                                                        Съобщение
                                                    </a>

                                                @endif
                                            @endauth
                                            @guest
                                                <a href="#" class="btn btn-info btn-block button-ads-info"
                                                   data-toggle="tooltip" data-placement="top" title="Нуждаете се от регистрация">
                                                    <i class="fa fa-mail-bulk"></i>
                                                    Съобщение
                                                </a>

                                            @endguest

                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-6">
                                            @auth
                                                @if($isfavorite)
                                                    @if ($favoriteads)
                                                        <form action="{{route('favorite.destroy',$favoriteads[0]->id)}}"
                                                              method="POST">
                                                            {{csrf_field()}}

                                                            <button type="submit" class="btn button-favorite btn-block btn-green button-ads-info">
                                                                <i class="fa fa-close"></i>
                                                                Премахване от Любими
                                                            </button>

                                                        </form>
                                                    @endif

                                                @else
                                                    <form action="{{route('favorite.store',['ad_id' => $ad->id])}}" method="POST">
                                                        {{csrf_field()}}
                                                        <button type="submit" class="btn button-favorite btn-block btn-green button-ads-info">
                                                            <i class="fa fa-heart"></i>
                                                            Добави в Любими
                                                        </button>

                                                    </form>
                                                @endif

                                            @endauth
                                            @guest
                                                <button type="submit" class="btn btn-block btn-green button-favorite
                                                button-ads-info pink"
                                                        data-toggle="tooltip" data-placement="top"
                                                        title="Нуждаете се от регистрация">
                                                    <i class="fa fa-heart"></i>
                                                    Добави в Любими
                                                </button>

                                            @endguest

                                        </div>
                                        <div class="col-md-6 mb-10">
                                            @auth
                                                @if ($ad->user->id == Auth::user()->id)
                                                    <a href="#" class="btn btn-danger btn-block button-ads-info"
                                                       data-toggle="tooltip" data-placement="top" title="Не можете да докладвате собствената си обява">
                                                        <i class="fa fa-warning"></i>Докладвай</a>
                                                @else
                                                    <a href="{{route('report.create',['id' => $ad->id])}}" class="btn btn-danger btn-block button-ads-info">
                                                        <i class="fa fa-warning"></i>Докладвай</a>
                                                @endif
                                            @endauth
                                            @guest
                                                <a href="#" class="btn btn-danger btn-block button-ads-info"
                                                   data-toggle="tooltip" data-placement="top" title="Нуждаете се от регистрация">
                                                    <i class="fa fa-warning"></i>Докладвай</a>

                                            @endguest


                                        </div>
                                    </div>
                                @else
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a  class="btn btn-danger btn-block button-ads-info"
                                               data-toggle="tooltip" data-placement="top" title="Обявата не е активна">
                                                <i class="fa fa-mail-bulk"></i>
                                               Тази Обява не е активна
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-md-12 ">
                                    <hr>
                                    <h2>Подробности</h2>
                                    <ul class="nav nav-pills" id="myTab-pills" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active show" id="one-tab-pills" data-toggle="tab" href="#one-pills"
                                               role="tab" aria-controls="one-pills" aria-expanded="true"
                                               aria-selected="true">Описание</a>
                                        </li>


                                    </ul>
                                    <div class="tab-content" id="myTabContent-pills">
                                        <div class="tab-pane fade active show" id="one-pills" role="tabpanel" aria-labelledby="one-tab-pills">
                                            {{$ad->description}}
                                        </div>

                                    </div>
                                </div>

                                </div>
                                <!--end Gallery Carousel-->
                            <div class="col-lg-4">
                                <p>Цена</p>
                                @auth
                                    @if (Auth::user()->is_admin)
                                        <a href="{{route('ad.edit',$ad->id)}}" class="btn-info btn btn-block">Admin Edit</a>

                                    @endif
                                @endauth

                                <h1 class="blue-txt">{{$ad->price}}лв</h1>
                                <hr>
                                <p>Публикувано: </p>
                                <h3>{{$ad->created_at->diffForHumans()}}</h3>
                                <p>Категория:</p>
                                <h3>{{$ad->category->name}}</h3>
                                <p>Тип Доставка:</p>
                                <h3>{{$ad->delivery->name}}</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Състояние:</p>
                                        <h3>{{$ad->condition->name}}</h3>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Оферти/Размяна?</p>
                                        <h3>
                                            @if($ad->accept_offers)
                                                Да
                                                @else
                                                Не
                                                @endif
                                        </h3>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Област:</p>
                                        <h3>{{$ad->region->name}}</h3>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Прегледа:</p>
                                        <h3>{{$ad->views}}</h3>
                                    </div>
                                </div>

                                <hr>
{{--                                author box--}}
                                <div class="box author-info">

                                    <div class="row">
                                        <div class=" col-md-4">
                                            <img class="background-image img-circle"
                                                 @if ($ad->user->avatar)
                                                 src="{{asset('/uploads/'.$ad->user->id . '/'.$ad->user->avatar)}}" alt="">
                                            @else
                                                src="{{asset('avatar.jpg')}}" alt="">
                                                 @endif

                                        </div>
                                        <div class="col-md-8">
                                            <a href="{{route('users.single',$ad->user->id)}}"><h2 class="text-center">{{$ad->user->username}}</h2></a>
                                        </div>

                                        </div>
                                    <hr>
                                    <p class="text-center"><a href="{{route('users.publishedads',$ad->user->id)
                                    }}">Вижте всички Обяви
                                            ({{$ad->user->ads->count()
                                    }})</p></a>
                                    <a href="{{route('users.single',$ad->user->id)}}"><p class="text-center">Вижте целия профил</p></a>

                                    </div>
                                </div>

                            </div>


                        </div>

                        </div>


                            <hr>






                    </div>
                    <!--============ End Listing Detail =========================================================-->
                    <!--============ Sidebar ====================================================================-->

                    <!--============ End Sidebar ================================================================-->
                </div>
            </div>
            <!--end container-->
        </section>
        <!--end block-->
    </section>
    <!--end content-->




@endsection

@section('scripts')
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
@endsection
