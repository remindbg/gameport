@extends('layouts.appwithsearch')
@section('title','Публикуване на обява')


@section('content')


    <!--*********************************************************************************************************-->
    <section class="content">
        <section class="block">
            <div class="container">

                <form class="form form-submit" method="POST" enctype="multipart/form-data" action="{{route('ad.store')}}">
                    @csrf

                    <section>
                        <h2>Основна Информация</h2>
                        <div class="form-group" id="type">
                            <label for="type" class="required">Тип На Обявата</label>
                            <figure>
                                @forelse($adtypes as $type)
                                    <label class="framed {{old('adtype_id') == $type->id ?
                                    ' active hover ' :''}}">
                                        <input type="radio" name="adtype_id" {{old('adtype_id') == $type->id ?
                                    ' class="radio-checked checked="checked" ' :''}}
                                               value="{{$type->id}}"
                                                {{old('adtype_id' == $type->id ? 'checked' : '')}}
                                               >
                                    {{$type->name}}
                                    </label>
                                    @empty

                                    @endforelse


                            </figure>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="title" class="col-form-label required">Заглавие</label>
                                    <input name="title"
                                           type="text"
                                           class="form-control"
                                           id="title"
                                           placeholder="Заглавие"
                                            value="{{old('title')}}"
                                    >
                                </div>
                                <!--end form-group-->
                            </div>
                            <!--end col-md-8-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="price" class="col-form-label required">Цена</label>
                                    <input name="price" type="number" value="{{old('price')}}" class="form-control"
                                           id="price">

                                </div>
                                <!--end form-group-->
                            </div>
                        </div>
                        <div class="row">


                        </div>
                    </section>
                    <!--end basic information-->
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Главна Категория</h2>
                                <div class="form-group">
                                    <label for="submit-category" class="col-form-label">Категория</label>
                                    <select class="change-tab" data-change-tab-target="category-tabs"
                                            name="main_category" id="main_category" data-placeholder="Главна Категория">
                                        @forelse ($categories as $category)
                                            @if($category->parent_id == 0)

                                            <option value="{{$category->id}}" {{old('main_category') == $category->id
                                            ? 'selected' : ''
                                            }}>{{$category->name}}</option>

                                            @endif

                                        @empty

                                        @endforelse

                                    </select>
                                </div>
                                <!--end form-group-->
                            </div>
                            <!--end col-md-4-->
                            <div class="col-md-12">
                                <h2>Под Категория</h2>
                                <div class="form-slides" id="category-tabs">
                                    <div class="form-slide default pt-10">
                                        <h3>Изберете Под Категория</h3>
                                    </div>

                                    @forelse($categories as $category)


                                            <div class="form-slide pt-10" id="{{$category->id}}">
                                                <h3>{{$category->name}}</h3>
                                                <figure class="category-icon">
                                                   <i class="{{$category->icon}}"></i>
                                                </figure>
                                                <!--end category-icon-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="computers___processor" class="col-form-label">Под Категория</label>
                                                            <select name="category_id" id="category_id" data-placeholder="Изберете Категория">
                                                                @forelse($category->children as $subcat)
                                                                <option value="{{$subcat->id}}
                                                                {{old('category_id') == $subcat->id
                                        ? 'selected' : ''
                                        }}">{{$subcat->name}}</option>
                                                                    @empty
                                                                @endforelse
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                    @empty
                                        @endforelse
                                </div>
                                <!--end form-slides-->
                            </div>
                            <!--end col-md-8-->
                        </div>
                        <!--end row-->
                    </section>

                    <section>
                        <h2>Описание</h2>
                        <div class="form-group">
                            <label for="details" class="col-form-label">Описание на обявата</label>
                            <textarea name="description" id="details" class="form-control" rows="4">{{old('description')
                            }}</textarea>
                        </div>
                        <!--end form-group-->
                    </section>

                    <section>
                        <h2>Област</h2>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="city" class="col-form-label required">Област</label>
                                    <select name="region_id" id="city" data-placeholder="Град">

                                        @forelse ($regions as $region)
                                            <option value="{{$region->id}}" {{old('region_id') == $region->id ?
                                            'selected' : ''
                                            }}>{{$region->name}}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                                <!--end form-group-->
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="addelivery_id" class="col-form-label required">Тип на Доставка/ Предаване</label>
                                    <select name="addelivery_id" id="addelivery_id" data-placeholder="Предаване">

                                        @forelse ($addeliveries as $delivery)
                                            <option value="{{$delivery->id}}"
                                            {{old('addelivery_id') == $delivery->id ?
                                            'selected' : ''
                                            }}>{{$delivery->name}}</option>
                                        @empty

                                        @endforelse


                                    </select>
                                </div>
                                <!--end form-group-->
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="city" class="col-form-label required">Състояние</label>
                                    <select name="adcondition_id" id="adcondition_id" data-placeholder="Състояние">
                                        @forelse ($adconditions as $condition)
                                            <option value="{{$condition->id}}" {{old('adcondition_id') ==
                                            $condition->id ?
                                            'selected' : ''
                                            }}>{{$condition->name}}</option>
                                        @empty

                                        @endforelse


                                    </select>
                                </div>
                                <!--end form-group-->
                            </div>
                            <!--end col-md-6-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="accept_offer" class="col-form-label required">Приемате ли Оферти за Обявата?<small>* Ако желаете други потребители да ви изпращат оферти с доплащане или размяна с други обяви</small></label>
                                    <select name="accept_offer" id="accept_offer" data-placeholder="Състояние">
                                        <option value="1" {{old('accept_offer') == 1 ? 'selected' : ''}}>Да</option>
                                        <option value="0" {{old('accept_offer') == 0 ? 'selected' : ''}}>Не</option>
                                    </select>
                                </div>
                                <!--end form-group-->
                                <section>
                                    <h2>Изображения</h2>
                                    <div class="file-upload-previews"></div>
                                    <div class="file-upload">
                                        <input type="file" name="images[]" class="file-upload-input with-preview"
                                               multiple title="До 6 Изображения"  accept="gif|jpg|png" value="{{old
                                               ('images')}}">
                                        <span><i class="fa fa-plus-circle"></i>До 6 Изображения</span>
                                    </div>
                                </section>
                            </div>


                        </div>
                        <!--end row-->

                        <!--end form-group-->

                    </section>

{{--                    <section>--}}
{{--                        <h2>Gallery</h2>--}}
{{--                        <div class="file-upload-previews"></div>--}}
{{--                        <div class="file-upload">--}}
{{--                            <input type="file" name="files[]" class="file-upload-input with-preview" multiple title="Click to add files" maxlength="10" accept="gif|jpg|png">--}}
{{--                            <span><i class="fa fa-plus-circle"></i>Click or drag images here</span>--}}
{{--                        </div>--}}
{{--                    </section>--}}



                    <section class="clearfix">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary large icon btn-block">Напред<i class="fa fa-chevron-right"></i></button>
                        </div>
                    </section>
                </form>
                <!--end form-submit-->
            </div>
            <!--end container-->
        </section>
        <!--end block-->
    </section>
    <!--end content-->



@endsection


@section('scripts')
    <script src="{{asset('assets/js/jquery-validate.bootstrap-tooltip.min.js')}}"></script>
    <script src="{{asset('assets/js/jQuery.MultiFile.min.js')}}"></script>

    <script>

    </script>

@endsection
