@extends('layouts.appwithsearch')
@section('title','Добавяне на изображения')


@section('content')


    <!--*********************************************************************************************************-->
    <section class="content">
        <section class="block">
            <div class="container">
                <form class="form form-submit" method="POST" enctype="multipart/form-data" action="">
                    @csrf

                    <section>
                        <h4>Сега е време да добавите главно изображение</h4>

                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <section>
                                        <hr>
                                        <button type="submit" class="btn btn-primary btn-block">
                                            <i class="fa fa-save fa-fw"></i>Напред</button>
                                        <hr>

                                        <div class="">

                                            <div class="image background-image">
                                                <img src="" alt="">
                                            </div>
                                            <div class="single-file-input">
                                                <input type="file" id="avatar" name="main_image">
                                                <div class="btn btn-framed btn-primary small">Качете Главно Изображение</div>
                                            </div>

                                        </div>

                                    <!--end form-group-->
                                </section>
                                <!--end form-group-->
                            </div>
                            <!--end col-md-8-->

                        </div>
                        <div class="row">


                        </div>

                        <section class="clearfix">
                            <div class="form-group">
                                <button type="submit" onclick="return confirm('Сигурни ли сте?')"
                                        class="btn btn-primary large icon btn-block">Изпращане на доклада
                                    <i class="fa fa-chevron-right"></i>
                                </button>
                            </div>
                        </section>
                </form>
                <!--end form-submit-->
            </div>
            <!--end container-->
        </section>
        <!--end block-->
    </section>
    <!--end content-->



@endsection
