@extends('layouts.app')

@section('title','Активна Оферта')

@section('content')

    <div class="row">
        <div class="col-md-3">
            @include('components.leftsidebar')
        </div>
        <!--end col-md-3-->
        <div class="col-md-9">

            {{--                @method('post')--}}
            <div class="row">
                <div class="col-md-12">
                    <h2>Оферта относно обява: {{$offer->ad->title}} </h2>
                    <hr>
                    @if($offer->sender->id == Auth::user()->id)
                        <p class="text-md-center">Към потребител: {{$offer->receiver->username}}</p>

                    @else
                        <p class="text-md-center">от потребител: {{$offer->sender->username}}</p>

                    @endif

                    <p>
                        {{$offer->body}}
                    </p>
                    <small>{{$offer->updated_at->diffForHumans()}}</small>
                    <hr>

                            <div class="row">


                                <div class="col-lg-4">
                                    <form action="{{route('offer.finishoffer',$offer->id)}}" method="POST">
                                        @csrf
                                        <p>Само собственика на обявата може да приключи сделка</p>
                                        <button type="submit" class="btn btn-block btn-success" onclick="return
                                        confirm('Сигурни ли сте че завършите сделката като успешна?')">Сделката е
                                            приключена</button>
                                    </form>
                                </div>


                                <div class="col-lg-4">
                                    <form action="{{route('offer.reject',$offer->id)}}" method="POST">
                                        @csrf
                                        <p>С натискането на бутона ще изтриете цялата оферта</p>
                                        <button type="submit" class="btn btn-block btn-danger" onclick="return
                                        confirm('Сигурни ли сте че искате да откажете и изтриете офертата?')">Отказ на
                                            Офертата</button>

                                    </form>
                                </div>
                            </div>

                </div>
                <!--end col-md-8-->
            </div>
            <!--end col-md-3-->


        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('assets/plugins/typeahead.js')}}"></script>
    <script>

    </script>


@endsection










