@extends('layouts.app')

@section('title','Оферти')

@section('content')

    <div class="row">
        <div class="col-md-3">
            @include('components.leftsidebar')
        </div>
        <!--end col-md-3-->
        <div class="col-md-9">
            <form class="form" method="POST" action="" enctype="multipart/form-data">
                @csrf
                {{ csrf_field() }}
                {{--                @method('post')--}}
                <div class="row">
                    <div class="col-md-12">
                        <h2>Оферти и запитвания</h2>
                        <ul class="nav nav-pills" id="myTab-pills" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="received-button" data-toggle="tab" href="#received" role="tab" aria-controls="received" aria-expanded="true">Получени Оферти</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="sent-button" data-toggle="tab" href="#sent" role="tab" aria-controls="sent">Изпратени Оферти</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="active-button" data-toggle="tab" href="#active" role="tab"
                                   aria-controls="sent">Приети Оферти</a>
                            </li>

                        </ul>
                        <div class="tab-content" id="myTabContent-pills">
                            <div class="tab-pane fade show active" id="received" role="tabpanel" aria-labelledby="received">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th class="w-50">Относно Обява</th>
                                        <th class="w-50">Размяна За:</th>
                                        <th>Цена</th>
                                        <th>Доплащане?</th>
                                        <th>Дата</th>
                                        <th>Статус</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($useroffers->sortByDesc('created_at') as $offer)

                                        @if($offer->is_ended || $offer->is_accepted)


                                        @else
                                            <tr class="{{$offer->is_seen ? 'text-muted' : 'text-success'}}">
                                                <td>
                                                    <img src="{!! asset($offer->ad->images()->first()->filepath) !!}"
                                                         style="max-width: 60px;max-height: 70px">
                                                    <a href="{{route('offer.single',$offer->id)}}">
                                                        {{ str_limit($offer->ad->title, $limit = 12, $end = '...') }}


                                                    </a>

                                                </td>
                                                <td>
                                                    @if(isset($offer->ad->images()->first()->filepath))
                                                        <img src="{!! asset($offer->for_which_ad->images()->first()
                                                        ->filepath) !!}"
                                                             style="max-width: 60px;max-height: 70px">
                                                    @else
                                                        <img src="{!! asset('noimage-small.png') !!}"
                                                             style="max-width: 60px;max-height: 70px">
                                                    @endif
                                                    <a href="{{route('offer.single',$offer->id)}}">
                                                        {{ str_limit($offer->for_which_ad->title, $limit = 12, $end = '...') }}


                                                    </a>

                                                </td>
                                                <td>{{$offer->price}}лв</td>
                                                <td>
                                                    @if ($offer->is_doplashtane)
                                                        Да
                                                    @else
                                                        Не
                                                    @endif
                                                </td>


                                                <td>{{$offer->created_at->diffForHumans()}}</td>
                                                <td>
                                                    @if($offer->is_rejected)
                                                        Отхвърлена {{$offer->updated_at->diffForHumans()}}
                                                    @else
                                                        Активна
                                                    @endif
                                                </td>
                                            </tr>
                                            @endif


                                    @empty
                                        Нямате Получени Оферти
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="sent" role="tabpanel" aria-labelledby="sent">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th class="w-50">Относно Обява</th>
                                        <th class="w-50">Размяна За:</th>
                                        <th>Цена</th>
                                        <th>Доплащане?</th>
                                        <th>Дата</th>
                                        <th>Статус</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($usersentoffers->sortByDesc('created_at') as $offer)

                                        @if($offer->is_ended || $offer->is_accepted)


                                        @else
                                            <tr class="{{$offer->is_seen ? 'text-muted' : 'text-success'}}">
                                                <td>
                                                    <img src="{!! asset($offer->ad->images()->first()->filepath) !!}"
                                                         style="max-width: 60px;max-height: 70px">
                                                    <a href="{{route('offer.single',$offer->id)}}">
                                                        {{ str_limit($offer->ad->title, $limit = 12, $end = '...') }}


                                                    </a>

                                                </td>
                                                <td>
                                                    @if(isset($offer->ad->images()->first()->filepath))
                                                        <img src="{!! asset($offer->for_which_ad->images()->first()
                                                        ->filepath) !!}"
                                                             style="max-width: 60px;max-height: 70px">
                                                    @else
                                                        <img src="{!! asset('noimage-small.png') !!}"
                                                             style="max-width: 60px;max-height: 70px">
                                                    @endif
                                                    <a href="{{route('offer.single',$offer->id)}}">
                                                        {{ str_limit($offer->for_which_ad->title, $limit = 12, $end = '...') }}


                                                    </a>

                                                </td>
                                                <td>{{$offer->price}}лв</td>
                                                <td>
                                                    @if ($offer->is_doplashtane)
                                                        Да
                                                    @else
                                                        Не
                                                    @endif
                                                </td>


                                                <td>{{$offer->created_at->diffForHumans()}}</td>
                                                <td>
                                                    @if($offer->is_rejected)
                                                        Отхвърлена {{$offer->updated_at->diffForHumans()}}
                                                    @else
                                                        Активна
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif


                                    @empty
                                        Нямате Изпратени Оферти
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="active" role="tabpanel" aria-labelledby="active-button">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th class="w-50">Относно Обява</th>
                                        <th class="w-50">Размяна За:</th>
                                        <th>Цена</th>
                                        <th>Доплащане?</th>
                                        <th>Дата</th>
                                        <th>Статус</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($activeoffers->sortByDesc('created_at') as $offer)

                                        @if($offer->is_ended)


                                        @else
                                            <tr class="{{$offer->is_seen ? 'text-muted' : 'text-success'}}">
                                                <td>
                                                    @if(isset($offer->ad->images()->first()->filepath))
                                                        <img src="{!! asset($offer->ad->images()->first()->filepath) !!}"
                                                             style="max-width: 60px;max-height: 70px">
                                                    @else
                                                        <img src="{!! asset('noimage-small.png') !!}"
                                                             style="max-width: 60px;max-height: 70px">
                                                    @endif


                                                    <a href="{{route('offer.single',$offer->id)}}">
                                                        {{ str_limit($offer->ad->title, $limit = 12, $end = '...') }}


                                                    </a>

                                                </td>
                                                <td>
                                                    @if(isset($offer->ad->images()->first()->filepath))
                                                        <img src="{!! asset($offer->for_which_ad->images()->first()
                                                        ->filepath) !!}"
                                                             style="max-width: 60px;max-height: 70px">
                                                    @else
                                                        <img src="{!! asset('noimage-small.png') !!}"
                                                             style="max-width: 60px;max-height: 70px">
                                                    @endif
                                                    <a href="{{route('offer.single',$offer->id)}}">
                                                        {{ str_limit($offer->for_which_ad->title, $limit = 12, $end = '...') }}


                                                    </a>

                                                </td>
                                                <td>{{$offer->price}}лв</td>
                                                <td>
                                                    @if ($offer->is_doplashtane)
                                                        Да
                                                    @else
                                                        Не
                                                    @endif
                                                </td>


                                                <td>{{$offer->created_at->diffForHumans()}}</td>
                                                <td>
                                                    @if($offer->is_rejected)
                                                        Отхвърлена {{$offer->updated_at->diffForHumans()}}
                                                    @else
                                                        Активна
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif


                                    @empty
                                        Нямате Получени Оферти
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>
                    <!--end col-md-8-->

                </div>
                <!--end col-md-3-->

            </form>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('assets/plugins/typeahead.js')}}"></script>
    <script>

    </script>


@endsection










