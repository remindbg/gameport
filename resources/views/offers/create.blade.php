@extends('layouts.appwithsearch')
@section('title','Оферта')


@section('content')

    <section class="content">
        <section class="block">
            <div class="container">
                <form class="form form-submit" method="POST" enctype="multipart/form-data" action="{{route('offer.store',$ad->id)}}">
                    @csrf

                    <section>
                        <h3>Създаване на оферта към обява: </h3>
                        <div class="box">
                            <p>{{$ad->title}}</p>

                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="offer_ad_id" class="col-form-label required">Изберете от вашите Обяви:</label>
                                        <select name="offer_ad_id" id="offer_ad_id"
                                                data-placeholder="Нуждаете се от обява!">

                                                @foreach($userads as $ad)
                                                <option value="{{$ad->id}}">{{$ad->title}} | Цена: {{$ad->price}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                    <!--end form-group-->

                                <section>

                                    <div class="form-group">
                                        <label for="details" class="col-form-label">Съобщение към Офертата</label>
                                        <textarea name="body" id="details" class="form-control" rows="4"></textarea>
                                    </div>
                                    <!--end form-group-->
                                </section>
                                <!--end form-group-->
                            </div>
                            <!--end col-md-8-->

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                    <div class="form-group">
                                            <label for="doplashtane"
                                            class="col-form-label required">Желаете ли да доплатите към вашата оферта?

                                            </label>
                                            <select name="is_doplashtane" id="doplashtane">
                                <option value="1" selected>Да</option>
                                                    <option value="0">Не<option>

                                            </select>
                                        </div>
                                <!--end form-group-->
                            </div>

                        </div>

                        <section class="clearfix">
                            <div class="form-group">
                                <button type="submit" onclick="return confirm('Сигурни ли сте?')"
                                        class="btn btn-info large icon btn-block">Изпращане на Оферта
                                    <i class="fa fa-chevron-right"></i>
                                </button>
                            </div>
                        </section>
                </form>
                <!--end form-submit-->
            </div>
            <!--end container-->
        </section>
        <!--end block-->
    </section>
    <!--end content-->
@endsection
