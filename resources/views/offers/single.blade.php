@extends('layouts.app')

@section('title','Оферта относно Обява')

@section('content')

    <div class="row">
        <div class="col-md-3">
            @include('components.leftsidebar')
        </div>
        <!--end col-md-3-->
        <div class="col-md-9">

                {{--                @method('post')--}}
                <div class="">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-lg-5 box">
                                <h4 class="text-center">Към обява:</h4>

                                <img src="{!! asset($offer->ad->images()->first()->filepath) !!}"
                                     style="max-width: 60px;max-height: 70px">
                                <a href="{{$offer->ad->adslug()}}">
                                    {{ str_limit($offer->ad->title, $limit = 12, $end = '...') }}


                                </a>
                            </div>
                            <div class="col-lg-2 box "><h4 class="text-center">Оферта от:</h4>
                                <h5 class="text-center">{{$offer->sender->username}}</h5>

                            </div>
                            <div class="col-lg-5 box ">
                                <h4 class="text-center">За Обява:</h4>
                                <img src="{!! asset($offer->for_which_ad->images()->first()->filepath) !!}"
                                     style="max-width: 60px;max-height: 70px">
                                <a href="{{$offer->for_which_ad->adslug()}}">
                                    {{ str_limit($offer->for_which_ad->title, $limit = 12, $end = '...') }}


                                </a>
                            </div>

                        </div>


                        <hr>
                        <div class="box">
                            @if($offer->sender->id == Auth::user()->id)
                                <p class="text-md-center">Към потребител: {{$offer->receiver->username}}</p>

                            @else
                                <p class="text-md-center">От потребител: {{$offer->sender->username}}</p>

                            @endif
                        </div>

                        <hr>
                        <h4>Съобщение към Офертата</h4>
                        <div class="box">
                            <p>
                                {{$offer->body}}
                            </p>
                        </div>




                    <small>{{$offer->created_at->diffForHumans()}}</small>
                    <hr>

                    @if ($offer->is_rejected)
                    <div class="row">
                            <button class="btn btn-block btn-info disabled">
                                    Офертата е отхвърлена {{$offer->updated_at->diffForHumans()}}
                                </button>
                    </div>

                        @else
                            @if($offer->sender->id == Auth::user()->id)
                                <button class="btn btn-block btn-info disabled">
                                    Потребителят все още не е отговорил на офертата
                                </button>
                                <form action="{{route('offer.reject',$offer->id)}}" method="POST">
                                    @csrf
                                    <p>С натискането на бутона ще изтриете цялата оферта</p>
                                    <button type="submit" class="btn btn-block btn-danger" onclick="return
                                        confirm('Сигурни ли сте че искате да откажете и изтриете офертата?')">Отказ на
                                        Офертата</button>

                                </form>

                            @else
                                <div class="row">
                                        <div class="col-lg-4">
                                            <form action="{{route('offer.accept',$offer->id)}}" method="POST">
                                                @csrf
                                                <button type="submit" class="btn btn-block btn-success">Приемане на
                                                    Офертата</button>
                                            </form>
                                        </div>


                                    <div class="col-lg-4">
                                        <form action="{{route('offer.reject',$offer->id)}}" method="POST">
                                            @csrf
                                            <button type="submit" class="btn btn-block btn-danger" onclick="return
                                            confirm('Сигурни ли сте?')">Изтриване на Офертата</button>

                                        </form>
                                    </div>
                                </div>

                            @endif

                    @endif

                    </div>
                    <!--end col-md-8-->
                </div>
                <!--end col-md-3-->


        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('assets/plugins/typeahead.js')}}"></script>
    <script>

    </script>


@endsection










