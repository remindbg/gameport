<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        DB::table('users')->insert([

            'username'                        => 'administrator',
            'email_verified_at'          => now(),
            'password'                   => bcrypt('adminpassword'), // password
            'remember_token'             => Str::random(10),
            'is_admin'                   => 1,
            'email' =>  'admin@admin.com',


            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

        ]);

        $regions = [
           'София','Благоевград','Бургас','Добрич','Габрово','Хасково','Кържали','Кюстендил','Ловеч','Монтана','Пазарджик',
            'Перник','Плевен','Пловдив','Разград','Русе','Шумен','Силистра','Сливен','Смолян','Стара Загора','Търговище',
            'Варна','Велико Търново','Видин','Враца','Ямбол'
        ];

            foreach ($regions as $region){
                DB::table('regions')->insert([

                    'name'                        => $region,
                    'slug'          => Str::slug($region),
                    'seo_title'                   => $region,
                    'seo_description'             => 'Обяви от област : ' . $region,



                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

                ]);

            }


            $adconditions = ['Ново','Употребявано'];

        foreach ($adconditions as $region) {
            DB::table('ad_conditions')->insert([

                'name'                        => $region,
                'slug'                        => Str::slug($region),
                'seo_title'                   => $region,
                'seo_description'             => 'Обяви от състояние : ' . $region,



                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

            ]);
        }
        $addeliveries = ['На Ръка','С Куриер','Няма Значение'];

        foreach ($addeliveries as $region) {
            DB::table('ad_deliveries')->insert([

                'name'                        => $region,

                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

            ]);
        }
        $adtypes = ['Продажба','Подарявам','Размяна'];

        foreach ($adtypes as $region) {
            DB::table('ad_types')->insert([

                'name'                        => $region,
                'slug'                        => Str::slug($region),
                'seo_title'                   => $region,
                'seo_description'             => 'Обяви от състояние : ' . $region,



                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

            ]);
        }
        $categories = ['Главна Категория 1','Главна Категория 2','Категория Главна 3'];

        foreach ($categories as $region) {
            DB::table('categories')->insert([

                'name'                        => $region,
                'slug'                        => Str::slug($region),
                'seo_title'                   => $region,
                'seo_description'             => 'Обяви от Категория : ' . $region,
                'parent_id' => 0,



                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

            ]);
        }

            DB::table('settings')->insert([

                'title'                        => 'Някакво Заглавие',
                'description'             =>       'Някакво Описание',




                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

            ]);

        DB::table('pages')->insert([

            'contact'                        => "<p><strong>ads</strong>Страница Контактиadfadfadsfasdfa</p>",
            'usloviq'             =>       "<p>Страница Условияafdsfadfasdfas</p>",
            'gdpr' => "<p>Страница ГДПРfadsfadsfadsfasdfa</p>",



            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

        ]);
        DB::table('static_texts')->insert([

            'homepage_lead'                        => "<h1 class=\"opacity-40 center\"> <a href=\"#\">проба</a>, <a href=\"#\">Продай</a> Или <a href=\"#\">Размени</a>",
            'homepage_normal'             =>       "ТЕСТ ЕДНО ДВЕ ТРИ",
            'footer_left' => "ТЕСТ ЕДНО ДВЕ ТРИТЕСТ ЕДНО ДВЕ ТРИТЕСТ ЕДНО ДВЕ ТРИТЕСТ ЕДНО ДВЕ ТРИ",
            'footer_right' => "ТЕСТ ЕДНО ДВЕ ТРИТЕСТ ЕДНО ДВЕ ТРИТЕСТ ЕДНО ДВЕ ТРИ",



            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

        ]);







    }
}
