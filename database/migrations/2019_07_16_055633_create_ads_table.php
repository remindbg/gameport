<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedInteger('adcondition_id')->nullable();
            $table->unsignedInteger('adtype_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('region_id')->nullable();
            $table->unsignedInteger('addelivery_id')->nullable();
            $table->string('uid')->nullable();
            $table->boolean('is_active')->default(true);

            $table->string('title');
            $table->text('description');
            $table->unsignedInteger('views')->default(1);

            $table->boolean('is_tradable')->default(false);
            $table->boolean('is_sold')->default(false);
            $table->integer('price')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
