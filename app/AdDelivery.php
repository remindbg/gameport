<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdDelivery extends Model
{
    public function ads() {
        return $this->hasMany('App\Ad');

    }
}
