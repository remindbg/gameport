<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{

    protected $guarded = [];


    public function  users() {
        return $this->hasMany('App\User');
    }

    public function ads() {
        return $this->hasMany('App\Ad');

    }
}
