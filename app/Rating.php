<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $guarded = [];

    public function  user() {
        return $this->belongsTo('App\User');

    }

    public function  ratingauthor() {
        return $this->belongsTo('App\User','rating_author_id');

    }
}
