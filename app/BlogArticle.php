<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogArticle extends Model
{
    protected $guarded = [];


    public function category() {
        return $this->belongsTo('App\BlogCategory','blog_category_id');
    }


    public function user() {
        return $this->belongsTo('App\User');
    }
}
