<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'names', 'email', 'password','last_login_at',
        'last_login_ip', 'city','age','avatar',
        'gender','region_id','phone','facebook',
        'about','is_phone_visible','is_active', 'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_login_at' => 'datetime',

    ];


    public function notifications()
    {
        return $this->morphMany(NewNotifications::class, 'notifiable')
            ->orderBy('created_at', 'desc');
    }

    public function receivedrating() {
        return $this->hasMany('App\Rating','user_id');
    }


    public  function hideemail() {
        $email = $this->email;
        $emailSplit = explode('@', $email);
        $email = $emailSplit[0];
        $len = strlen($email)-1;
        for($i = 1; $i < $len; $i++) {
            $email[$i] = '*';
        }

        $hiddenemail = $email . '@' . $emailSplit[1];

        return $hiddenemail;
    }


    public function getCreatedAtAttribute($date)
    {


        return Carbon::createFromFormat('Y-m-d H:i:s', $date);
    }




    public function region() {
        return $this->belongsTo('App\Region');

    }


    public function  ads() {
        return $this->hasMany('App\Ad');
    }






    public function received()
    {
        return $this->hasMany('App\Conversation', 'receiver_id');
    }

    public function messages() {
        return $this->hasMany('App\Message');
    }


    public function sent()
    {
        return $this->hasMany('App\Conversation', 'sender_id');
    }

    public function favoriteads() {
        return $this->hasMany('App\FavoriteAds','user_id');
    }


    public function receivedOffers() {
        return $this->hasMany('App\Offer','to_user');
    }


    public function sentOffers() {
        return $this->hasMany('App\Offer','user_id');

    }





}
