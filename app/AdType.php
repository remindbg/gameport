<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdType extends Model
{
    public function ads() {
        return $this->hasMany('App\Ad','adtype_id');

    }

}
