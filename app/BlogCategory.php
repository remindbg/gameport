<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    protected $guarded = [];


    public function articles() {
        return $this->hasMany('App\BlogArticle');
    }
}
