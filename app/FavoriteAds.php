<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavoriteAds extends Model
{
    protected $guarded = [];


    public function user() {
        return $this->belongsTo('App\User');
    }

    public function ads() {
        return $this->BelongsTo('App\Ad','ad_id');
    }



}
