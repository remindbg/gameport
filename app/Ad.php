<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $guarded = [];



    public function adslug() {
        $id = $this->id;
        $uid = $this->uid;
        $category_slug = $this->category->slug;

        $adslug = url('/ads/'.$id . '/' . $category_slug . '/' . $uid);
//        Route::get('/ads/{id}/{catslug}/{uid}','AdController@single')->name('ad.single');


        return $adslug;



    }

    public function condition() {
        return $this->belongsTo('App\AdCondition','adcondition_id');
    }


    public function conversations(){
        return $this->hasMany('App\Conversation');
    }

    public function type() {
        return $this->belongsTo('App\AdType','adtype_id');
    }


    public function delivery() {
        return $this->belongsTo('App\AdDelivery','addelivery_id');
    }

    public function region() {
        return $this->belongsTo('App\Region');
    }

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }


    public function category() {
        return $this->belongsTo('App\Category');
    }

    public function images() {
        return $this->hasMany('App\AdImage');
    }

    public function comments() {
        return $this->hasMany('App\AdComment');
    }




}
