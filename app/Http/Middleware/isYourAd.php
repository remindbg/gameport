<?php

namespace App\Http\Middleware;

use Closure;
use App\Ad;
use App\User;
use Illuminate\Support\Facades\Auth;
class isYourAd
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ad = Ad::find($request->route()->parameter('id'));

        if($ad) {
            if (Auth::user()->id == $ad->user->id || Auth::user()->is_admin) {

                return $next($request);
            }
            else {
                return redirect()->route('dashboard.myads')->with('error','Не можете да редактирате дадената обява');

            }
        }
        else {
            return redirect()->route('dashboard.myads')->with('error','Такава обява не съществува');

        }


    }
}
