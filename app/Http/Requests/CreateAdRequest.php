<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
   {
       return [
           'adtype_id' => 'required',
           'title' => 'required|min:5|max:50',
           'price' => 'required',
           'description' => 'required|min:10|max:1000',
           'category_id' => 'nullable',
           'adcondition_id' => 'nullable',
           'addelivery_id' => 'nullable',
           'region_id'    => 'nullable',
           'main_category' => 'nullable',
           'accept_offer' => 'nullable',
           'image_file.*' => 'nullable|mimes:jpg,jpeg,png,bmp|max:10000',
           'images.*' => 'nullable'


       ];
   }
   /**
 * Get the error messages for the defined validation rules.
 *
 * @return array
 */

public function messages()
{
    return [
        'adtype_id.required' => 'Изберете Типа на обявата',
        'title.required' => 'Заглавието е задължително',
        'title.min' => 'Заглавието е прекалено късо',
        'title.max' => 'Заглавието е прекалено дълго',
        'price.required'  => 'Цената е задължителна',
        'description.required' => 'Описанието е задължително',
        'description.min' => 'Описанието е прекалено кратко',
        'description.max' => 'Описанието е прекалено дълго'
    ];
}
}
