<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|same:password|min:5',
            'password-confirm' => 'required|same:password',
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'password.required' => 'Полето е задължително',
            'password-confirm.required' => 'Задължително е да повторите паролата',
            'password-confirm' => 'Паролите не съвпадат',
            'password.same' => 'Паролите не съвпадат',
            'password-confirm.same' => 'Паролите не съвпадат',
            'password.min' => 'Паролата е прекалено кратка, използвайте повече от 5 символа',
            'password-confirm.min' => 'Паролата е прекалено кратка, използвайте повече от 5 символа',
        ];
    }
}
