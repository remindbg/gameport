<?php

namespace App\Http\Controllers;

use App\Region;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regions = Region::with('ads')->paginate(8);
        return view('region.index',compact('regions'));

    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function single($id,$slug)
    {

     $region = Region::find($id);
     $regionads = $region->ads()->paginate(16);



        return view('region.single',compact('region','regionads'));

    }


}
