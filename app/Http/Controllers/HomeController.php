<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Category;
use Illuminate\Http\Request;
use App\Settings;
use stdClass;
use App\StaticText;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $settings = Settings::find(1);
        $homepagetext = StaticText::find(1);

        $categories = Category::with('parent','children')->get();
        $ads = Ad::
        with('user','type','category','region','condition','delivery')->where('is_active',true)
        ->orderBy('created_at','desc')->paginate(7);
//        dd($ads);
        return view('home',compact('categories','ads','settings','homepagetext'));
    }
}
