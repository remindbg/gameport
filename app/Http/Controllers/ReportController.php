<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Report;
use Illuminate\Http\Request;
use Auth;
class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $ad = Ad::find($id);
        return view('reports.create',compact('ad'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,Request $request)
    {
        $report = Report::FirstOrCreate(
            ['user_id' => Auth::user()->id,
                'body' => $request['body'],
                'ad_id' => $id

            ]

        );
        $report->save();
        if($report->wasRecentlyCreated) {
//           dd($favorite->id);
//            dd($adid);
//            dd($favorite->id);
            return back()->with('info','Успешно Изпращане на Доклада');

        } else {
            return back()->with('info','Вече сте докладвали Обявата');

        }





    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        //
    }
}
