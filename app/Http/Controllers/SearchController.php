<?php

namespace App\Http\Controllers;

use App\Search;
use Illuminate\Http\Request;
use App\Region;
use App\Category;
use App\Ad;
class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request['keyword'];
        $category_id = (int)$request['category_id'];

        $type_id = (int)$request['type_id'];
        $region_id = (int)$request['region_id'];
        $min_price = (int)$request['min_price'];

        $max_price = (int)$request['max_price'];


//       $result = Ad::when($keyword, function ($q) use ($keyword) {
//            return $q->where('title', 'like', '%' . $keyword . '%')
//                    ->orWhere('description', 'like', '%' . $keyword . '%');
//       })
//           ->whereHas('category', function ($q) use ($category_id) {
//               return $q->where('category_id', $category_id);
//             })
//           ->whereHas('region', function ($q) use ($region_id) {
//               return $q->where('region_id', $region_id);
//                   })
//                   ->whereHas('type', function ($q) use ($type_id) {
//                       return $q->where('adtype_id', $type_id);
//                   })
//                   ->where('price', '>=', $min_price)
//                   ->where('price', '<=', $max_price)
//                   ->paginate(3);
//
//
//
//               return view('search.index', compact('result'));

        $result = Ad::when($keyword, function ($q) use ($keyword) {
            return $q->where(function($q) use ($keyword) {
                $q->where('title', 'like', '%' . $keyword . '%')->orWhere('description', 'like', '%' . $keyword . '%');
            });
        })
            ->when($category_id, function ($q) use ($category_id) {
                return $q->where('category_id', $category_id);
            })
            ->when($region_id, function ($q) use ($region_id) {
                return $q->where('region_id', '=', $region_id);
            })
            ->when($type_id, function ($q) use ($type_id) {
                return $q->where('adtype_id', '=', $type_id);
            })
            ->when($min_price, function ($q) use ($min_price) {
                return $q->where('price', '>=', $min_price);
            })
            ->when($max_price, function ($q) use ($max_price) {
                return $q->where('price', '<=', $max_price);
            })
            ->paginate(3);
//        dd($result);
        session()->flashInput($request->input());
        return view('search.index', compact('result'));

           }







    public function results(Request $request) {




        return view('search.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Search  $search
     * @return \Illuminate\Http\Response
     */
    public function show(Search $search)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Search  $search
     * @return \Illuminate\Http\Response
     */
    public function edit(Search $search)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Search  $search
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Search $search)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Search  $search
     * @return \Illuminate\Http\Response
     */
    public function destroy(Search $search)
    {
        //
    }
}
