<?php

namespace App\Http\Controllers;

use App\Offer;
use App\User;
use Illuminate\Http\Request;
use App\Ad;
use Auth;
class OfferController extends Controller
{


    public function activesingle($id) {

        $offer = Offer::find($id);

        return view('offers.activesingle',compact('offer'));
    }



    public function index()
    {
        $userid = Auth()->user()->id;
        $user = User::find($userid);


        $useroffers = $user->receivedoffers;

        $usersentoffers = $user->sentoffers;

        $activeoffers2 = Offer::with('ad','sender','receiver','for_which_ad')
            ->where('user_id',$userid)
            ->orWhere('to_user',$userid)->get()
           ;


        $activeoffers = $activeoffers2->where('is_accepted',true);




        return view('offers.index',compact('useroffers','usersentoffers','activeoffers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($ad_id)
    {
        $userads = User::find(Auth()->user()->id);
        $userads = $userads->ads()->where('is_active',true)->get();

        $ad = Ad::find($ad_id);

        return view('offers.create',compact('ad','userads'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($ad_id,Request $request)
    {

        if(!$request['offer_ad_id']) {
            return back()->with('error','За да изпратите оферта е нужно да имате активна Обява');
        }

        $ad = Ad::find($ad_id);
        $adprice = $ad->price;
        $to_user = $ad->user->id;



        $offer = new Offer();
        $offer->ad_id = $ad_id;
        $offer->user_id = Auth()->user()->id;
        $offer->body = $request['body'];
        $offer->is_accepted = false;
        $offer->offer_ad_id = $request['offer_ad_id'];
        $offer->price = $adprice;
        $offer->to_user = $to_user;
        $offer->is_doplashtane = $request['is_doplashtane'];
        $offer->save();

        return redirect()->route('offer.index')->with('info','Офертата е изпратена към потребителя');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function single($id)
    {
        $offer = Offer::find($id);

        return view('offers.single',compact('offer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function edit(Offer $offer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function reject($id)
    {
        $offer = Offer::find($id);
        $offer->is_rejected = true;
        $offer->is_accepted = false;
        $offer->is_seen = true;

        $offer->delete();

        return redirect()->route('offer.index')->with('success','Офертата е отхвърлена');
    }


    public function accept($id,Request $request) {

        $offer = Offer::find($id);
        $offer->is_accepted = true;
        $offer->is_seen = true;
        $offer->save();

        // lets change some properties to the AD now

        $ad_id = $offer->ad->id;
        $ad = Ad::find($ad_id);
        $ad->adstatus = 2;

                // adstatus = 2 means that the offer is processing , but not finished , 3 means the offer is finished
       $ad->save();
       $adaccepttext = true;

        $receiver = $offer->for_which_ad->user()->get()->first();
//        dd($receiver);



       return redirect()->route('messages.compose',
           ['adid' => $offer->for_which_ad->id,
               'sender_id' => Auth::user()->id,
               'accept' => $adaccepttext,
               'receiver' => $receiver,

           ]);



    }

    public function deleteoffer($id) {
        $offer = Offer::find($id);
        $offer->delete();

        return redirect()->route('offers.index');

    }


   public function finishoffer($id) {
        $offer = Offer::find($id);

        if(!$offer || $offer->is_ended) {
            return back()->with('success','Такава Оферта не съществува или е приключила');

        }
        else {
            $offer->is_ended = true;
            $offer->save();

            $ad1 = $offer->ad;
            $ad2 = $offer->for_which_ad;

            $ad1->is_active = false;
            $ad2->is_active = false;

            $ad2->adstatus = 3;
            $ad1->adstatus = 3;

            $ad1->is_sold = true;
            $ad2->is_sold = true;

            $ad1->swapped_for_ad_id = $ad2->id;
            $ad2->swapped_for_ad_id = $ad1->id;


            $ad1->save();
            $ad2->save();

            $potentialuser = $ad1->user->id;
            $potentialuser2 = $ad2->user->id;

            $offer->delete();

            if (Auth::user()->id == $potentialuser) {
                return redirect()->route('users.single',$potentialuser2)
                    ->with('success','Поздравление за приключилата сделка. Ако желаете можете да оставите рейтинг върху профила на потребителя');

            }
            if (Auth::user()->id == $potentialuser2) {
                return redirect()->route('users.single',$potentialuser1)
                    ->with('success','Поздравление за приключилата сделка. Ако желаете можете да оставите рейтинг върху профила на потребителя');

            }



        }




   }


}
