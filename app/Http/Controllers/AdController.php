<?php

namespace App\Http\Controllers;

use App\Ad;
use App\AdCondition;
use App\AdDelivery;
use App\AdImage;
use App\AdType;
use App\Category;
use App\Conversation;
use App\FavoriteAds;
use App\Region;
use App\User;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\CreateAdRequest;
use App\Http\Requests\UpdateImageRequest;

use Illuminate\Support\Facades\Auth;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads = Ad::with('type','category','user','condition','delivery'
        )->where('is_active',true)
        ->orderBy('created_at','desc')
        ->paginate(8);

        return view('ads.index',compact('ads'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $adtypes = AdType::all();
        $adconditions = AdCondition::all();
        $addeliveries = AdDelivery::all();
        $regions = Region::all();
        $categories = Category::with('parent','children')->get();

        return view('ads.create',
            compact('adtypes','adconditions','addeliveries','regions','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAdRequest $request)
    {


        $data = $request->validated();
//        dd($data);
        $ad = new Ad;
        $ad->adtype_id = $data['adtype_id'];
        $ad->title = $data['title'];
        $ad->description = $data['description'];
        $ad->price = $data['price'];
        if($data['category_id'] == null) {
            $ad->category_id = $data['main_category'];
        }
        else {
            $ad->category_id = $data['category_id'];
        }
        $ad->adcondition_id = $data['adcondition_id'];
        $ad->addelivery_id = $data['addelivery_id'];
        $ad->region_id = $data['region_id'];
        $string = strtolower(str_random(7));
       $ad->uid = $string;
       $ad->views = 1;
       $ad->user_id =  Auth::id();
        $ad->accept_offers = $data['accept_offer'];

       $ad->save();
        if($ad->wasRecentlyCreated) {

            if(array_key_exists('images',$data)) {
                    $counter = 1;
                    foreach ($data['images'] as $file) {
                        $extension = $file->getClientOriginalExtension();
                        $fileName = str_random(5)."-".date('his')."-".str_random(3).".".$extension;
                        $folderpath  = 'uploads/obqvi/'.$ad->uid . '/';
                         $test =  $file->move($folderpath , $fileName);
                         $filepath =  $folderpath . $fileName;

                         $dbimages = new AdImage();
                         $dbimages->ad_id = $ad->id;
                         $dbimages->filepath = $filepath;
                         if($counter == 1) {
                             $dbimages->is_first = true;
                         }
                         $dbimages->save();
                         $counter++;

                    }

            }
            else {
                $dbimages = new AdImage();
                $dbimages->ad_id = $ad->id;
                $dbimages->is_first = true;
                $dbimages->filepath = 'noimage-small.png';

                $dbimages->save();

            }

        }

        return redirect()->route('ad.single',['id' => $ad->id,'catslug'=> $ad->category->slug,'uid' => $ad->uid])->with
    ('info','Обявата е успешно публикувана');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
public function  updateimage($id, UpdateImageRequest $request) {
    $data = $request->validated();

    $ad = Ad::find($id);
    if($ad->images->count() >= 6) {
        return back()->with('message','Вече има качени 6 изображения');
    }

    if($data['images']) {

        $counter = 1;
        foreach ($data['images'] as $file) {
            if($counter == 6) {
                break;
            }

            $extension = $file->getClientOriginalExtension();
            $fileName = str_random(5)."-".date('his')."-".str_random(3).".".$extension;
            $folderpath  = 'uploads/obqvi/'.$ad->uid . '/';
            $test =  $file->move($folderpath , $fileName);
            $filepath =  $folderpath . $fileName;

            $dbimages = new AdImage();
            $dbimages->ad_id = $ad->id;
            $dbimages->filepath = $filepath;

            $dbimages->save();


            $counter++;



        }
        return  back()->with('success','Успешно Обновяване');

    }
    else {
        return back()->with('info','Не сте въвели Данни');
    }

}

    public function editimages($id) {
        $ad = Ad::find($id);

        $imagescount = $ad->images->count();

        return view('ads.editimages',compact('ad','imagescount'));
    }



    public function single($id)
    {
        $ad = Ad::find($id);
//        dd($ad);
        $ad->views += 1;
        $ad->save();

        $isfavorite = false;
        $favoriteads = [];
        if($user = Auth::user())
        {
            $user = Auth::user();
            // do what you need to do

            $favoriteads = FavoriteAds::with('user')
                ->where('ad_id','=',$ad->id)
                ->where('user_id','=',$user->id)->get();



            if($favoriteads->isEmpty()) {

            }
            else {
                $isfavorite = true;
            }
        }

        return view('ads.single',compact('ad','isfavorite','favoriteads'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $regions = Region::all();
        $adtypes = AdType::all();
        $adconditions = AdCondition::all();
        $addeliveries = AdDelivery::all();
        $ad = Ad::find($id);
        return view('ads.edit',compact('ad','adtypes','regions','adconditions','addeliveries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {

        $ad = Ad::find($id);
        $ad->adtype_id = $request['adtype_id'];
        $ad->title = $request['title'];
        $ad->description = $request['description'];
        $ad->price = $request['price'];

        $ad->adcondition_id = $request['adcondition_id'];
        $ad->addelivery_id = $request['addelivery_id'];
        $ad->region_id = $request['region_id'];

        $ad->accept_offers = $request['accept_offer'];
        $ad->is_active = $request['is_active'];

        $ad->save();

        return back()->with('success','Успешна Редакция');



    }

    public  function deleteimage($id) {
        $image = AdImage::find($id);
        $path = $image->filepath;
        unlink($path);
        $image->delete();

        return back()->with('message','Изображението е успешно изтрито');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ad $ad)
    {
        //
    }


}
