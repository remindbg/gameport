<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
   public function contact() {
       $page = Page::find(1);
       return view('pages.contact',compact('page'));

   }
    public function usloviq() {
        $page = Page::find(1);
        return view('pages.usloviq',compact('page'));

    }
    public function gdpr() {
        $page = Page::find(1);
        return view('pages.gdpr',compact('page'));

    }
}
