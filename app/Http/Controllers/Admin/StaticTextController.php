<?php

namespace App\Http\Controllers\Admin;

use App\StaticText;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StaticTextController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (StaticText::find(1)) {

            $statictext = StaticText::find(1);
            return view('admin.settings.statictexts.index',compact('statictext'));
        }
        else {
            $statictext = new StaticText();
            $statictext->homepage_lead = '<h1 class="opacity-40 center">
            <a href="#">Купи</a>, <a href="#">Продай</a> Или  <a href="#">Размени</a>
        </h1>';
            $statictext->homepage_normal = 'Няма Зададен Текст все още';
            $statictext->footer_left = 'Няма Зададен Текст все още';
            $statictext->footer_right = 'Няма Зададен Текст все още';
            $statictext->save();
            return view('admin.settings.statictexts.index',compact('statictext'));
        }
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $settings = StaticText::find($id);

        $settings->homepage_lead = $request['homepage_lead'];
        $settings->homepage_normal = $request['homepage_normal'];
        $settings->footer_left = $request['footer_left'];
        $settings->footer_right = $request['footer_right'];

        $settings->save();
        return back()->with('success','Запазени Настройки');


    }


}
