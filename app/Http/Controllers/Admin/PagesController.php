<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;
class PagesController extends Controller
{
    public function index() {

        if (Page::find(1)) {

            $page = Page::find(1);
            return view('admin.pages.index',compact('page'));
        }
        else {
            $page = new Page();
            $page->contact = 'Страница Контакти';
            $page->usloviq = 'Страница Условия';
            $page->gdpr = 'Страница ГДПР';

            $page->save();
            return view('admin.pages.index',compact('page'));

        }


    }

    public function update(Request $request,$id) {
        $page = Page::find($id);
        $page->contact = $request['contact'];
        $page->usloviq = $request['usloviq'];
        $page->gdpr = $request['gdpr'];
        $page->save();
        return back()->with('success','Запазени Настройки');


    }

    public function show() {

    }
    public function destroy() {

    }

    public function edit() {

    }
}
