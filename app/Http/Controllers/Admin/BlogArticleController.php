<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BlogArticle;
use App\BlogCategory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class BlogArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {

        $articles = BlogArticle::with('category','user')->orderBy('created_at','DESC')->get();

        return view('admin.blogarticles.index',compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = BlogCategory::all();

        return view('admin.blogarticles.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        $article = new BlogArticle();

        $article->title = $request['title'];
        $article->blog_category_id = $request['category_id'];
        $article->slug = Str::slug($request['title'],'-');
        $article->is_active = $request['is_active'];
        $article->user_id = Auth::user()->id;

        $article->body = $request['body'];

        $article->save();

        if($article->isRecentlyCreated) {
            if ($request->hasFile('image')) {

                $file = $request->file('image');
                $picName = $file->getClientOriginalName();
                $imagePath = '/uploads/articles/' . $article->id . '/';
                $file->move(public_path($imagePath), $picName);
                $article->image = $imagePath . $picName;
                $article->save();

                return redirect()->route('admin.blogarticles.index')->with('success','Статията е успешно Добавена');

            }
        }
        else {
            return redirect()->route('admin.blogarticles.index')->with('danger','Възникна Някакъв Проблем');

        }



//        $createarticle = Article::Create([
//            'title' => $request['title'],
//            'body' => $request['body'],
//
//        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = BlogArticle::find($id);
        $categories = BlogCategory::all();

        return view('admin.blogarticles.edit',compact('article','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article = BlogArticle::find($id);

        $article->title = $request['title'];
        $article->blog_category_id = $request['category_id'];
        $article->slug = Str::slug($request['title'],'-');
        $article->is_active = $request['is_active'];
        $article->user_id = Auth::user()->id;
        $article->seo_title = $request['seo_title'];
        $article->seo_description = $request['seo_description'];

        $article->body = $request['body'];

        $article->save();


            if ($request->hasFile('image')) {

                if($article->image) {
                    $oldimage = $article->image;
                    Storage::disk('public_uploads')->delete($article->image);


                }

                $file = $request->file('image');
                $picName = $file->getClientOriginalName();
                $imagePath = '/uploads/articles/' . $article->id . '/';
                $file->move(public_path($imagePath), $picName);
                $article->image = $imagePath . $picName;
                $article->save();


            }
        return redirect()->route('admin.blogarticles.index')->with('success','Статията е успешно Редактирана');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
