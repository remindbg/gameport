<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ad;
use App\AdDelivery;
use App\AdType;
use App\AdCondition;
use App\Region;
use App\AdImage;
use Illuminate\Support\Facades\File;
class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deliveries = AdDelivery::all();
        $conditions = AdCondition::all();
        $types = AdType::all();
        $regions = Region::all();
        $ads = Ad::with('type','user','delivery','condition','region','category','images')
            ->orderBy('created_at','desc')
            ->get();


        return view('admin.ads.index',compact('ads','deliveries','conditions','types','regions'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $deliveries = AdDelivery::all();
        $conditions = AdCondition::all();
        $types = AdType::all();
        return view('admin.ads.create',compact('deliveries','conditions','types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ad = new Ad();

       return redirect()->route('admin.ad.index')->with('message','Успешно Добавяне');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $conditions = AdCondition::all();
        $types = AdType::all();
        $ad = Ad::find($id);



        $regions = Region::all();
        $deliveries = AdDelivery::all();
        $categories = Category::all();
//        dd($ad->delivery);
        return view('admin.ads.edit',
            compact('ad','conditions','types','regions','deliveries','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ad = Ad::find($id);
       $ad->update($request->all());
        return redirect()->route('admin.ad.index')->with('success','Успешна Редакция');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ad  = Ad::find($id);
        $adimages = $ad->images;
        if($adimages) {
            $imagecount = 0;
            foreach ($adimages as $image) {

                $path = $image->filepath;

                $image->delete();
                if(file_exists($path)) {
                    if($path == 'noimage-small.png') {

                    }
                    else {
                        @unlink($path);
                        $imagecount++;
                    }


                }
            }
        }
        $ad->delete();

        return redirect()->route('admin.ad.index')
            ->with('success','Изтрита обява  + ' . $imagecount . ' Изображения от базата и файловете');

    }
}
