<?php

namespace App\Http\Controllers\Admin;

use App\Settings;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
  public function index() {
      if (Settings::find(1)) {

          $settings = Settings::find(1);
          return view('admin.settings.index',compact('settings'));
      }
      else {
          $settings = new Settings();
          $settings->title = 'Примерно Заглавие';
          $settings->description = 'Примерно описание за гугъл';
          $settings->save();
          return view('admin.settings.index',compact('settings'));

      }


  }

  public function update(Request $request,$id) {
      $settings = Settings::find($id);
      $settings->title = $request['title'];
      $settings->description = $request['description'];
      $settings->save();
      return back()->with('success','Запазени Настройки');


  }
}
