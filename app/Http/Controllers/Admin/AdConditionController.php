<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AdCondition;
use App\Ad;
use Illuminate\Support\Str;
class AdConditionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $adconditions = AdCondition::with('ads');
        return view('admin.ads.adcondition.index',compact('adconditions'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ads.adcondition.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $condition = new AdCondition();
        $condition->name = $request['name'];
        $condition->slug = Str::slug($request['name'],'-');
        $condition->seo_title = $request['seo_title'];
        $condition->seo_description = $request['seo_description'];
        $condition->save();
        return redirect()->route('admin.adcondition.index')->with('success','Успешно добавяне на състояние');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $adcondition = AdCondition::find($id);
        return view('admin.ads.adcondition.edit',compact('adcondition'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $condition = AdCondition::find($id);
        $condition->name = $request['name'];
        $condition->slug = $request['slug'];
        $condition->seo_title = $request['seo_title'];
        $condition->seo_description = $request['seo_description'];
        $condition->save();
        return redirect()->route('admin.adcondition.index')
            ->with('success','Успешно редактиране на състояние');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $condition = AdCondition::find($id);
        $condition->delete();
        return redirect()->route('admin.adcondition.index')
            ->with('success','Успешно Изтриване на Състояние');
    }
}
