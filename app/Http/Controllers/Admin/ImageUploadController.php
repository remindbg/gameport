<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;
class ImageUploadController extends Controller
{
    public function uploadImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        if ($validator->passes()) {


            $input = $request->all();
            $input['image'] = time().'.'.$request->image->getClientOriginalExtension();
            $id = \Auth::user()->id;
            $request->image->move(public_path('uploads/avatars/'.$id.'/avatar/'), $input['image']);


           $user = User::find($id);
           $user->avatar = $input['image'];
           $user->save();


            return 1;
        }


        return 0;
    }
}
