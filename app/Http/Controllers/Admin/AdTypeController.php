<?php

namespace App\Http\Controllers\Admin;

use App\AdType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
class AdTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adtypes = AdType::all();
        return view('admin.ads.adtypes.index',compact('adtypes'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ads.adtypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $condition = new AdType();
        $condition->name = $request['name'];
        $condition->slug = Str::slug($request['name'],'-');
        $condition->seo_title = $request['seo_title'];
        $condition->seo_description = $request['seo_description'];
        $condition->save();
        return redirect()->route('admin.adtype.index')->with('success','Успешно добавяне на Тип Обява');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $adtype = AdType::find($id);
        return view('admin.ads.adtypes.edit',compact('adtype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adtype = Adtype::find($id);
        $adtype->name = $request['name'];
        $adtype->slug = $request['slug'];
        $adtype->seo_title = $request['seo_title'];
        $adtype->seo_description = $request['seo_description'];
        $adtype->save();
        return redirect()->route('admin.adtype.index')->with('success','Успешно редактиране на Тип Обява');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $adtype = AdType::find($id);
        $adtype->delete();
        return redirect()->route('admin.adtype.index')->with('success','Успешно Изтриване на Тип Обява');
    }
}
