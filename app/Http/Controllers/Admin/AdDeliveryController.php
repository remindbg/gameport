<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AdDelivery;
use App\Ad;
use Illuminate\Support\Str;
class AdDeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $addeliveries = AdDelivery::all();
        return view('admin.ads.addelivery.index',compact('addeliveries'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ads.addelivery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $condition = new AdDelivery();
        $condition->name = $request['name'];

        $condition->save();
        return redirect()->route('admin.addelivery.index')->with('success','Успешно добавяне на Тип Доставка');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $addelivery = AdDelivery::find($id);
        return view('admin.ads.addelivery.edit',compact('addelivery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $addelivery = AdDelivery::find($id);
        $addelivery->name = $request['name'];

        $addelivery->save();
        return redirect()->route('admin.addelivery.index')->with('success','Успешно редактиране на Тип Доставка');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $addelivery = AdDelivery::find($id);
        $addelivery->delete();
        return redirect()->route('admin.addelivery.index')->with('success','Успешно Изтриване на Тип Доставка');
    }
}
