<?php

namespace App\Http\Controllers;

use App\AdComments;
use Illuminate\Http\Request;

class AdCommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdComments  $adComments
     * @return \Illuminate\Http\Response
     */
    public function show(AdComments $adComments)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdComments  $adComments
     * @return \Illuminate\Http\Response
     */
    public function edit(AdComments $adComments)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdComments  $adComments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdComments $adComments)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdComments  $adComments
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdComments $adComments)
    {
        //
    }
}
