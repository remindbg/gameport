<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('ads','region')->get();
        return view('users.index',compact('users'));



    }
    public function publishedads($id)
    {
       $user = User::find($id);
       $userads = $user->ads()->paginate(8);

        return view('users.publishedads',compact('user','userads'));



    }

    public function changepassword()
    {
       $user_id = Auth::user()->id;
       $user = User::find($user_id);


        return view('dashborad.changepassword',compact('user'));



    }

    public function updatepassword(ChangePasswordRequest $request)
    {
        if(Auth::Check())
        {

            $user = User::find(Auth::user()->id);


            if(Hash::check($request->password,$user->password)) {
                return back()->with('warning','Паролата не може да е същата като старата ви парола');
            }
            else {
                $user = User::find(Auth::user()->id)->update(["password"=> bcrypt($request->password)]);

            }
            return  redirect()->route('dashboard.editprofile')->with('success','Успешно променихте паролата');

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function single($id)
    {
        $user = User::find($id);
        $userrating = $user->receivedrating()->where('is_approved','=',true)->limit(5)
            ->get();


//            dd($user->receivedrating()->first()->rating_author()->first()->username);


        return view('users.single',compact('user','userrating'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




}
