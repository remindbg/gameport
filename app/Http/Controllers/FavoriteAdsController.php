<?php

namespace App\Http\Controllers;

use App\FavoriteAds;
use Illuminate\Http\Request;
use App\User;
class FavoriteAdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = Auth()->user()->id;


        $user = User::find($userid);
        $ads = $user->favoriteads;

//      foreach ($ads as $test)
//      {
//          dd($test->ads->title);
//      }
        return view('dashborad.favorites',compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$ad_id)
    {
        $userid = Auth()->user()->id;
        $adid = $ad_id;
        $favorite = FavoriteAds::FirstOrCreate(
            ['user_id' => $userid,
                'ad_id' => $ad_id]

        );
        $favorite->save();
       if($favorite->wasRecentlyCreated) {
//           dd($favorite->id);
//            dd($adid);
//            dd($favorite->id);
           return back()->with('info','Успешно Добавяне на обявата в Любими');

        } else {
           return back()->with('success','Обявата Вече е добавена в Любими');


       }






    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FavoriteAds  $favoriteAds
     * @return \Illuminate\Http\Response
     */
    public function show(FavoriteAds $favoriteAds)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FavoriteAds  $favoriteAds
     * @return \Illuminate\Http\Response
     */
    public function edit(FavoriteAds $favoriteAds)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FavoriteAds  $favoriteAds
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FavoriteAds $favoriteAds)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FavoriteAds  $favoriteAds
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $favad = FavoriteAds::find($id);
        $favad->delete();

        return back()->with('info','Успешно премахнахте обявата от Любими');
    }

}
