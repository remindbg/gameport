<?php

namespace App\Http\Controllers;

use App\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,Request $request)
    {
        $data = $request->all();
        $receiver = User::find($id);
        $sender = Auth::user();

        $rating = new Rating();
        $ratingbody = $request['body'];

        if ($receiver->id === $sender->id) {
            return back()->with('error','Не можете да поставяте рейтинг на собственият си профил :)');

        }


        if(strlen($ratingbody) <= 3) {
            return back()->with('error','Рейтинга е прекалено кратък - напишете повече от 3 символа');
        }
        if(is_null($request['rating_number'])) {
            return back()->with('error','Не сте избрали Рейтинг Число');
        }

        $rating->rating_number = $request['rating_number'];
        $rating->body = $request['body'];
        $rating->user_id = $receiver->id;
        $rating->rating_author_id = $sender->id;
        $rating->is_negative = $request['is_negative'];

        $rating->save();
        return back()->with('success','Рейтинга е успешно Добавен. След като бъде одобрен от администратор, ще бъде публичен');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function single($id)

    {
        $user = User::find($id);
        if(!$user) {
            return back()->with('error','Няма Такъв Потребител');
        }
        else {
            $userrating = $user->receivedrating()
                ->where('is_approved','=',true)
                ->get();

            return  view('ratings.single',compact('userrating','user'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function edit(Rating $rating)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rating $rating)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rating $rating)
    {
        //
    }
}
