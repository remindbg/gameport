<?php

namespace App\Http\Controllers;

use App\BlogArticle;
use App\BlogCategory;
use Illuminate\Http\Request;

class BlogArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = BlogArticle::with('category')->orderBy('created_at','DESC')->get();
        $latestarticles = BlogArticle::with('category')->limit(6)->get();

        $bcategories = BlogCategory::all();
        return view('articles.index',compact('articles','bcategories','latestarticles'));
    }

    public function single($article_id,$category_slug,$article_slug) {

        $latestarticles = BlogArticle::with('category')->limit(6)->get();

        $bcategories = BlogCategory::all();
        $article = BlogArticle::find($article_id);

        return view('articles.single',compact('article','bcategories','latestarticles'));

    }

}
