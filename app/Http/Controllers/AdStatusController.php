<?php

namespace App\Http\Controllers;

use App\AdStatus;
use Illuminate\Http\Request;

class AdStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdStatus  $adStatus
     * @return \Illuminate\Http\Response
     */
    public function show(AdStatus $adStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdStatus  $adStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(AdStatus $adStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdStatus  $adStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdStatus $adStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdStatus  $adStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdStatus $adStatus)
    {
        //
    }
}
