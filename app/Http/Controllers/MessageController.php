<?php

namespace App\Http\Controllers;

use App\Conversation;
use App\Message;
use Illuminate\Http\Request;
use App\User;
use App\Ad;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = Auth()->user()->id;
        $user = User::find($userid);
        $receivedconvos = $user->received;
        $sentconvos = $user->sent;

        $allunread = 0;
        $unreadreceived = $user->received()->whereHas('messages', function ($query) {
            $query->where('is_seen', false);
        })->get();
        $unreadnumreseived = $unreadreceived->count();
//       dd($unreadreceived);



        return view('messages.index',compact('receivedconvos','sentconvos'));
    }

    public function single($id)
    {
        $conversation = Conversation::find($id);


        $unread = $conversation->messages()->where('is_seen',0)->get();



        foreach ($unread as $unreadmessages) {




            if($unreadmessages->user_id == Auth::user()->id) {
                $unreadmessages->is_seen = false;
                $unreadmessages->save();
            }
            else {
                $unreadmessages->is_seen = true;

                $conversation->is_seen = true;
                $conversation->save();
                $unreadmessages->save();
            }

        }


        return view('messages.single',compact('conversation'));
    }


    public function insert($id,Request $request)
    {

       $message = $request['message'];
       if(strlen($message) <= 5) {
           return back()->with('error','Съобщението е прекалено кратко');
       }
       else {

          $convo = Conversation::find($id);



          $message = Message::create([
              'message' => $message,
              'user_id' => Auth()->user()->id,
              'conversation_id' => $id

          ]);
//              $convo->messages()->save([
//                  'message' => $message,
//                  'user_id' => Auth()->user()->id,
//                  'conversation_id' => $id
//
//                  ]);

           return back()->with('success','Съобщението е Изпратено');

       }

       }

       public function compose($adid,$senderid,$receiver) {

        $ad = Ad::find($adid);
        $sender = Auth()->user()->id;
        $receiver = User::find($receiver);


           return view('messages.create',compact('ad','sender','receiver'));

       }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($adid,$sender_id, Request $request)
    {
        $ad = Ad::find($adid);

        $sender = (int)$sender_id;
        $receiver = $ad->user->id;
        $userIds = array($sender,$receiver);


        $message = $request['message'];
        if(strlen($message) <= 5) {
            return back()->with('error','Съобщението е прекалено кратко');
        }
        else {

            $existingConversation = Conversation::where([
                [ 'sender_id', '=', $userIds[0] ],
                [ 'receiver_id', '=', $userIds[1] ]
            ])->orWhere([
                [ 'sender_id', '=', $userIds[1] ],
                [ 'receiver_id', '=', $userIds[0] ]
            ])->first();


            if($existingConversation) {
                    Message::Create([
                    'message' => $request['message'],
                    'user_id' => Auth()->user()->id,
                    'conversation_id' => $existingConversation->id
                ]);
                return redirect()
                    ->route('messages.single',$existingConversation->id)
                    ->with('info','Вече Съществува дискусия между теб и потребителя');

            }
            else {  // create a new conversation

                $convo = new Conversation();
                $convo->sender_id = Auth()->user()->id;
                $convo->receiver_id = $ad->user->id;
                $convo->ad_id = $ad->id;
                $convo->save();

                if($convo->save()) {
                    $messagecreated = Message::Create([
                        'message' => $request['message'],
                        'user_id' => Auth()->user()->id,
                        'conversation_id' => $convo->id
                    ]);
                    return redirect()
                        ->route('messages.single',$convo->id)
                        ->with('success','Успешно Изпратено Съобщение');
                }

            }
        }

    }

}
