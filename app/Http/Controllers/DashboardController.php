<?php

namespace App\Http\Controllers;

use App\Region;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class DashboardController extends Controller
{


    public function  editprofile() {
        $tryuser = Auth::user()->id;

        $user = User::find($tryuser);

        $regions = Region::all();

        return view('dashborad.editprofile',compact('user','regions'));

    }

    public function storeprofile(Request $request) {
        $user_id = Auth::user()->id;

        $user = User::find($user_id);

        if($request->hasFile('avatar')) {
            $oldavatar = $user->avatar;
            $file = $request->file('avatar');
            $picName = $file->getClientOriginalName();
            $imagePath = '/uploads/' . $user->id  . '/';
            $file->move(public_path($imagePath), $picName);
           $user->avatar = $picName;


            Storage::disk('public_uploads')->delete('' .$user->id . '/' . $oldavatar);
        }


        $user->names = $request['names'];
        $user->city = $request['city'];
        $user->age = $request['age'];
        $user->region_id  = $request['region_id'];
        $user->gender = $request['gender'];
        $user->about = $request['about'];
        $user->phone = $request['phone'];
        $user->facebook = $request['facebook'];




        $user->save();
        return redirect()->back()->with('success','Успешна Редакция на данните');

    }

    public function myads() {
        $userid = Auth()->user()->id;
        $user = User::find($userid);
        return view('dashborad.myads',compact('user'));

    }
}
