<?php

namespace App\Http\Controllers;

use App\StaticText;
use Illuminate\Http\Request;

class StaticTextController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StaticText  $staticText
     * @return \Illuminate\Http\Response
     */
    public function show(StaticText $staticText)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StaticText  $staticText
     * @return \Illuminate\Http\Response
     */
    public function edit(StaticText $staticText)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StaticText  $staticText
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StaticText $staticText)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StaticText  $staticText
     * @return \Illuminate\Http\Response
     */
    public function destroy(StaticText $staticText)
    {
        //
    }
}
