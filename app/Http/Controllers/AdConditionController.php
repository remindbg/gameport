<?php

namespace App\Http\Controllers;

use App\Ad;
use App\AdCondition;
use Illuminate\Http\Request;

class AdConditionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdCondition  $adCondition
     * @return \Illuminate\Http\Response
     */
    public function show(AdCondition $adCondition)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdCondition  $adCondition
     * @return \Illuminate\Http\Response
     */
    public function edit(AdCondition $adCondition)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdCondition  $adCondition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdCondition $adCondition)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdCondition  $adCondition
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdCondition $adCondition)
    {
        //
    }
}
