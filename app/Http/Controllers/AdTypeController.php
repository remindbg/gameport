<?php

namespace App\Http\Controllers;

use App\AdType;
use Illuminate\Http\Request;
use App\Ad;
class AdTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id,$slug)
    {
        $type = AdType::with('ads')->find($id);
        $adtypes = $type->ads()->paginate(15);
        return view('adtypes.index',compact('type','adtypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdType  $adType
     * @return \Illuminate\Http\Response
     */
    public function show(AdType $adType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdType  $adType
     * @return \Illuminate\Http\Response
     */
    public function edit(AdType $adType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdType  $adType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdType $adType)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdType  $adType
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdType $adType)
    {
        //
    }
}
