<?php

namespace App\Providers;

use App\Ad;
use App\Category;
use App\Message;
use App\StaticText;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Ads;
use App\User;
use App\Categories;
use App\AdCondition;
use App\AdDelivery;
use App\AdType;
use App\Region;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // admin sidebar
        view()->composer('admin._static.nav', function($view) {
            $catcount = Category::count();
            $adcount = Ad::count();
            $usercount = User::count();
            $regioncount = Region::count();
            $messagesnew = Message::with('users');



            $view->with('usercount',$usercount)
                ->with('catcount',$catcount)
                ->with('adcount',$adcount)
                ->with('regioncount',$regioncount)
            ;
        });


        /*
         * front end sidebars/menus
         */
        view()->composer('_static.header', function($view) {
             $adtypes = AdType::all();




            $view->with('adtypes',$adtypes);
        });

        /*
         * search values
         */
        view()->composer('components.search', function($view) {
            $adtypes = AdType::all();
           $categoriessearch = Category::all()
               ->where('parent_id','=',0)
               ;
           $regionssearch = Region::all();





            $view->with('categoriessearch',$categoriessearch)
                 ->with('regionssearch', $regionssearch)->with('adtypes',$adtypes);
        });
        view()->composer('search.index', function($view) {
            $adtypes = AdType::all();
            $categoriessearch = Category::all()
                ->where('parent_id','=',0)
            ;
            $regionssearch = Region::all();





            $view->with('categoriessearch',$categoriessearch)
                ->with('regionssearch', $regionssearch)->with('adtypes',$adtypes);
        });

        view()->composer('_static.footer', function($view) {
           $footertext = StaticText::find(1);





            $view->with('footertext',$footertext);
        });


        view()->composer('layouts.homelayout', function($view) {
            $homepagetext = StaticText::find(1);





            $view->with('homepagetext',$homepagetext);
        });



        Carbon::setLocale('bg');
        Schema::defaultStringLength(191);
    }
}
