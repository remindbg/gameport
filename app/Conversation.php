<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $guarded = [];
    public function messages()
    {
        return $this->hasMany('App\Message')->orderBy('created_at', 'desc');;
    }

    public function sender()
    {
        return $this->belongsTo('App\User');
    }

    public function receiver()
    {
        return $this->belongsTo('App\User');
    }

    public function ad() {
        return $this->belongsTo('App\Ad');
    }



}
