<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Category extends Model
{
    protected $guarded = [];


    /**
     * Get the Categories associated with the parent's `id`
     */
    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id', 'id');
    }

    /**
     * Get the parent associated with the Category`s parent_id`
     */
    public function parent()
    {
        return $this->belongsTo('App\Category','parent_id');
    }


    public function ads() {
        return $this->hasMany('App\Ad');
    }

}
