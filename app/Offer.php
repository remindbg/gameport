<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{

    public function receiver() {
        return $this->belongsTo('App\User','to_user');
    }

    public function sender() {
        return $this->belongsTo('App\User','user_id');
    }



    public function ad() {
        return $this->belongsTo('App\Ad');
    }


    public function  for_which_ad() {
        return $this->belongsTo('App\Ad','offer_ad_id');

    }
}
